﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Threading;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;

namespace Minecraft_Launcher
{
    class FileDownloader
    {

        static System.Net.WebClient webConnect;// Our WebClient that will be doing the downloading for us
        static Stopwatch sw = new Stopwatch();// The stopwatch which we will be using to calculate the download speed
        static bool downloadBusy = false;
        static bool cancelDownload = false;
        static double totalBytes = 0;
        static double previousByteCount = 0;
        static double bytesRecievedTotal = 0;
        static double bytesRecieved = 0;
        static string downloadingFile = "";
        static string userAgentVer = "AtomLauncher/AtomicElectronics/1.0";


        /// <summary>
        /// Input multiple file to download. Does not stop until error or completed.
        /// </summary>
        /// <param name="urlFilePath">A Dictonary of files that contain the list of files.
        /// [Doesnt matter number, { conatining file url, file name, folder if any}]</param>
        /// <param name="filePATH"></param>
        internal static void Multi(Dictionary<int, string[]> urlFilePath, string filePATH)
        {
            cancelDownload = false;
            totalBytes = 0;
            bytesRecievedTotal = 0;
            bytesRecieved = 0;
            downloadingFile = "";


            Downloader.atomLaunch.formText("Downloader_LabelStatus", "Checking Remote Files"); 
            //Downloader_LabelStatus.Text = "Checking Remote Files";
            //Downloader_LabelDLFile.Text = "";
            //Downloader_LabelDLSpeed.Text = "";
            //Downloader_LabelFileMB.Text = "";
            //Downloader_LabelTotalMB.Text = "";

            //atomLauncher.atomLaunch.barValues(0, 0);
            foreach (KeyValuePair<int, string[]> entry in urlFilePath)
            {
                if (cancelDownload) throw new System.Exception("Checking Remote Files");

                //  Downloader_LabelFileMB.Text = (Convert.ToInt32(entry.Key) + 1) + " / " + urlFilePath.Count();

                //atomLauncher.atomLaunch.barValues((Convert.ToInt32(entry.Key) + 1) * 100 / urlFilePath.Count(), 0);

                string barValues = ((Convert.ToInt32(entry.Key) + 1) * 100 / urlFilePath.Count()).ToString();
                Console.Write("barValues" + barValues);

                int contentLength = 0;
                // Downloader_LabelDLFile.Text = entry.Value[0];
                try
                {
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(entry.Value[0]);
                    req.UserAgent = userAgentVer;
                    req.Method = "HEAD";
                    using (System.Net.WebResponse resp = req.GetResponse())
                    {
                        int.TryParse(resp.Headers.Get("Content-Length"), out contentLength);
                    }
                }
                catch (Exception ex)
                {
                    cancelDownload = true;
                    throw new System.Exception("Error: " + ex.Message);
                }
                totalBytes = totalBytes + contentLength;
            }
            // Downloader_LabelStatus.Text = "Downloading Files";
            sw.Restart();
            foreach (KeyValuePair<int, string[]> entry in urlFilePath)
            {
                if (cancelDownload) throw new System.Exception("Downloading Files");
                downloadBusy = true;
                try
                {
                    download(entry.Value[0], filePATH + @"\" + entry.Value[1]); // Start downloading the file
                }
                catch (Exception ex)
                {
                    cancelDownload = true;
                    throw new System.Exception("Error: " + ex.Message);
                }
                while (downloadBusy) // Wait for Complete File
                {
                    Thread.Sleep(100);
                    if (cancelDownload)
                    {
                        break;
                    }
                }
            }
            sw.Reset();

            //Downloader_LabelDLFile.Text = "";
            //Downloader_LabelDLSpeed.Text = "";
            //Downloader_LabelFileMB.Text = "";
            //Downloader_LabelTotalMB.Text = "";

            //atomLauncher.atomLaunch.barValues(100, 100);
            if (cancelDownload) throw new System.Exception("Downloading Files");
        }



        /// <summary>
        /// Input a single file to download. Does not stop until error or completed.
        /// </summary>
        /// <param name="urlFilePATH">Example: "http://www.webaddress.com/file.zip" </param>
        /// <param name="filePATH">Example: "C:\LOCATION\file.zip" </param>
        internal static void Single(string urlFilePATH, string filePATH)
        {
            cancelDownload = false;
            totalBytes = 0;
            bytesRecievedTotal = 0;
            bytesRecieved = 0;
            downloadingFile = "";

            //Downloader_LabelStatus.Text = "Checking Remote Files";
            //Downloader_LabelDLFile.Text = urlFilePATH;
            //Downloader_LabelDLSpeed.Text = "";
            //Downloader_LabelFileMB.Text = "";
            //Downloader_LabelTotalMB.Text = "";

            //atomLauncher.atomLaunch.barValues(0, 0);
            /////////////////////
            if (cancelDownload) throw new System.Exception("Checking Remote File");
            /////////////////////
            int contentLength = 0;
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(urlFilePATH);
                req.UserAgent = userAgentVer;
                req.Method = "HEAD";
                using (System.Net.WebResponse resp = req.GetResponse())
                {
                    int.TryParse(resp.Headers.Get("Content-Length"), out contentLength);
                }
                totalBytes = contentLength;
            }
            catch (Exception ex)
            {
                cancelDownload = true;
                throw new System.Exception(ex.Message);
            }
            /////////////////////
            if (cancelDownload) throw new System.Exception("Downloading File");
            /////////////////////
            //  Downloader_LabelStatus.Text = "Downloading File";
            sw.Reset();
            sw.Start();
            downloadBusy = true;
            try
            {
                download(urlFilePATH, filePATH); // Start downloading the file
            }
            catch (Exception ex)
            {
                cancelDownload = true;
                throw new System.Exception(ex.Message);
            }
            /////////////////////
            if (cancelDownload) throw new System.Exception("Downloading File");
            /////////////////////
            while (downloadBusy) // Wait for Complete File
            {
                Thread.Sleep(100);
                if (cancelDownload)
                {
                    break;
                }
            }
            sw.Reset();


            //Downloader_LabelDLFile.Text = "";
            //Downloader_LabelDLSpeed.Text = "";
            //Downloader_LabelFileMB.Text = "";
            //Downloader_LabelTotalMB.Text = "";
            // atomLauncher.atomLaunch.barValues(100, 100);
        }

        /// <summary>
        /// The base method used to start individual downloads.
        /// </summary>
        /// <param name="urlFilePATH">Example: "http://www.webaddress.com/file.zip" </param>
        /// <param name="filePATH">Example: "C:\LOCATION\file.zip" </param>
        internal static void download(string urlFilePATH, string filePATH)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(filePATH));
            downloadingFile = filePATH;
            if (cancelDownload) return;
            using (webConnect = new WebClient())
            {
                webConnect.Headers[HttpRequestHeader.UserAgent] = userAgentVer;
                webConnect.DownloadProgressChanged += new DownloadProgressChangedEventHandler(progress);
                webConnect.DownloadFileCompleted += new AsyncCompletedEventHandler(completed);
                try
                {
                    Uri URL = new Uri(urlFilePATH);
                    webConnect.DownloadFileAsync(URL, downloadingFile);
                }
                catch (Exception ex)
                {
                    throw new System.Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// The method used by the AsyncDownload to show progress.
        /// </summary>
        /// <param name="sender">Object Sender</param>
        /// <param name="e">Download Progress Changed Event Args - e</param>
        internal static void progress(object sender, DownloadProgressChangedEventArgs e)
        {
            try
            {
                if (cancelDownload)
                {
                    webConnect.CancelAsync();
                }
                else
                {
                    if (totalBytes == 0)
                    {
                        totalBytes = 1;
                    }
                    bytesRecieved = e.BytesReceived;
                    double compileReceivedBytes = bytesRecievedTotal + bytesRecieved;
                    if (e.ProgressPercentage != 100)
                    {
                        //  Downloader_LabelDLFile.Text = downloadingFile;
                        if (sw.Elapsed.TotalSeconds >= 5)
                        {

                            //    Downloader_LabelDLSpeed.Text = ((compileReceivedBytes - previousByteCount) / 1024 / sw.Elapsed.TotalSeconds).ToString("0.00") + " kB/s";
                            sw.Restart();
                            previousByteCount = compileReceivedBytes;
                        }
                        else if (previousByteCount == 0)
                        {
                            //      Downloader_LabelDLSpeed.Text = (compileReceivedBytes / 1024 / sw.Elapsed.TotalSeconds).ToString("0.00") + " kB/s";
                        }
                        //if ((566 * atomLauncher.atomLaunch.formBarTop.Width) / 100 != e.ProgressPercentage)
                        //{
                        //    atomLauncher.atomLaunch.barValues(e.ProgressPercentage, Convert.ToInt32(compileReceivedBytes * 100 / totalBytes));
                        //    atomLauncher.atomLaunch.formText("formLabelFileMB", (Convert.ToDouble(bytesRecieved) / 1024 / 1024).ToString("0.00") + " MB " + "/ " + (Convert.ToDouble(e.TotalBytesToReceive) / 1024 / 1024).ToString("0.00") + " MB");
                        //    atomLauncher.atomLaunch.formText("formLabelTotalMB", (Convert.ToDouble(compileReceivedBytes) / 1024 / 1024).ToString("0.00") + " MB " + "/ " + (Convert.ToDouble(totalBytes) / 1024 / 1024).ToString("0.00") + " MB");
                        //}
                    }
                    else
                    {
                        //atomLauncher.atomLaunch.barValues(100, Convert.ToInt32(compileReceivedBytes * 100 / totalBytes));
                        //    Downloader_LabelFileMB.Text = (Convert.ToDouble(bytesRecieved) / 1024 / 1024).ToString("0.00") + " MB " + "/ " + (Convert.ToDouble(e.TotalBytesToReceive) / 1024 / 1024).ToString("0.00") + " MB";
                    }
                }
            }
            catch (Exception ex)
            {
                cancelDownload = true;
                MessageBox.Show("Error: " + ex.Message + " : Please Report this. (Error - atomDownloading.progress)");
            }
        }
        /// <summary>
        /// The method used by the AsyncDownload when completed.
        /// </summary>
        /// <param name="sender">Object Sender</param>
        /// <param name="e">Async Completed Event Args - e</param>
        internal static void completed(object sender, AsyncCompletedEventArgs e)
        {
            bytesRecievedTotal = bytesRecievedTotal + bytesRecieved;
            if (e.Cancelled == true)
            {
                queueDelete(downloadingFile);
            }
            else if (e.Error != null)
            {
                cancelDownload = true;
                queueDelete(downloadingFile);
                MessageBox.Show("Error: " + e.Error.Message);
            }
            else
            {
                downloadingFile = "";
                downloadBusy = false;
            }
        }

        public static void queueDelete(string pathFILE)
        {
            Thread delQuT = new Thread(() => deleteLoop(pathFILE, true));
            delQuT.Start();
        }
        public static string deleteLoop(string pathFILE, bool displayMessageBox = false, int attemtps = 10)
        {
            string status = "";
            int x = 0;
            while (true)
            {
                bool tempBool = tryToDelete(pathFILE);
                if (tempBool)
                {
                    break;
                }
                Thread.Sleep(500);
                if (x > attemtps)
                {
                    try
                    {
                        File.Delete(pathFILE);
                    }
                    catch (Exception ex)
                    {
                        status = ex.Message;
                        if (displayMessageBox) MessageBox.Show("File: " + pathFILE + "\n\nError: " + ex.Message + "\n\nMake sure Your have proper file permissons to delete these files, or, make sure a program isnt exicuting them.");
                    }
                    break;
                }
                x++;
            }
            return status;
        }
        public static bool tryToDelete(string pathFILE)
        {
            if (File.Exists(pathFILE))
            {
                try
                {
                    File.Delete(pathFILE);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        public static string deleteFolLoop(string pathFOLDER, bool recursive = true, bool displayMessageBox = false, int attemtps = 10)
        {
            string status = "";
            int x = 0;
            while (true)
            {
                bool tempBool = tryToDeleteFol(pathFOLDER, recursive);
                if (tempBool)
                {
                    break;
                }
                Thread.Sleep(500);
                if (x > attemtps)
                {
                    try
                    {
                        Directory.Delete(pathFOLDER, recursive);
                    }
                    catch (Exception ex)
                    {
                        status = ex.Message;
                        if (displayMessageBox) MessageBox.Show("File: " + pathFOLDER + "\n\nError: " + ex.Message + "\n\nMake sure Your have proper file permissons to delete these files, or, make sure a program isnt exicuting them.");
                    }
                    break;
                }
                x++;
            }
            return status;
        }
        public static bool tryToDeleteFol(string pathFOLDER, bool recursive)
        {
            if (Directory.Exists(pathFOLDER))
            {
                try
                {
                    Directory.Delete(pathFOLDER, recursive);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

    }
}
