﻿namespace Minecraft_Launcher
{
    partial class Update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Update));
            this.groupBox_Progress = new System.Windows.Forms.GroupBox();
            this.progressBar_Update = new System.Windows.Forms.ProgressBar();
            this.labelSpeed = new System.Windows.Forms.Label();
            this.labelPerc = new System.Windows.Forms.Label();
            this.labelDownloaded = new System.Windows.Forms.Label();
            this.button_Download = new System.Windows.Forms.Button();
            this.reason_label = new System.Windows.Forms.Label();
            this.groupBox_Progress.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_Progress
            // 
            this.groupBox_Progress.Controls.Add(this.progressBar_Update);
            this.groupBox_Progress.Controls.Add(this.labelSpeed);
            this.groupBox_Progress.Controls.Add(this.labelPerc);
            this.groupBox_Progress.Controls.Add(this.labelDownloaded);
            this.groupBox_Progress.Location = new System.Drawing.Point(12, 12);
            this.groupBox_Progress.Name = "groupBox_Progress";
            this.groupBox_Progress.Size = new System.Drawing.Size(237, 65);
            this.groupBox_Progress.TabIndex = 25;
            this.groupBox_Progress.TabStop = false;
            this.groupBox_Progress.Text = "Download progress";
            // 
            // progressBar_Update
            // 
            this.progressBar_Update.Location = new System.Drawing.Point(6, 19);
            this.progressBar_Update.Name = "progressBar_Update";
            this.progressBar_Update.Size = new System.Drawing.Size(188, 23);
            this.progressBar_Update.TabIndex = 13;
            // 
            // labelSpeed
            // 
            this.labelSpeed.AutoSize = true;
            this.labelSpeed.Location = new System.Drawing.Point(147, 45);
            this.labelSpeed.Name = "labelSpeed";
            this.labelSpeed.Size = new System.Drawing.Size(30, 13);
            this.labelSpeed.TabIndex = 20;
            this.labelSpeed.Text = "0kbs";
            this.labelSpeed.Visible = false;
            // 
            // labelPerc
            // 
            this.labelPerc.AutoSize = true;
            this.labelPerc.BackColor = System.Drawing.Color.Transparent;
            this.labelPerc.Location = new System.Drawing.Point(200, 24);
            this.labelPerc.Name = "labelPerc";
            this.labelPerc.Size = new System.Drawing.Size(21, 13);
            this.labelPerc.TabIndex = 14;
            this.labelPerc.Text = "0%";
            // 
            // labelDownloaded
            // 
            this.labelDownloaded.AutoSize = true;
            this.labelDownloaded.BackColor = System.Drawing.Color.Transparent;
            this.labelDownloaded.Location = new System.Drawing.Point(6, 45);
            this.labelDownloaded.Name = "labelDownloaded";
            this.labelDownloaded.Size = new System.Drawing.Size(10, 13);
            this.labelDownloaded.TabIndex = 19;
            this.labelDownloaded.Text = "-";
            this.labelDownloaded.Visible = false;
            // 
            // button_Download
            // 
            this.button_Download.Location = new System.Drawing.Point(174, 85);
            this.button_Download.Name = "button_Download";
            this.button_Download.Size = new System.Drawing.Size(75, 23);
            this.button_Download.TabIndex = 24;
            this.button_Download.Text = "Download";
            this.button_Download.UseVisualStyleBackColor = true;
            this.button_Download.Click += new System.EventHandler(this.button_Update_Click);
            // 
            // reason_label
            // 
            this.reason_label.AutoSize = true;
            this.reason_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reason_label.Location = new System.Drawing.Point(13, 89);
            this.reason_label.Name = "reason_label";
            this.reason_label.Size = new System.Drawing.Size(156, 13);
            this.reason_label.TabIndex = 26;
            this.reason_label.Text = "Get files required to launch mc?";
            // 
            // Update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 113);
            this.Controls.Add(this.reason_label);
            this.Controls.Add(this.groupBox_Progress);
            this.Controls.Add(this.button_Download);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Update";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Update";
            this.TopMost = true;
            this.groupBox_Progress.ResumeLayout(false);
            this.groupBox_Progress.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_Progress;
        private System.Windows.Forms.ProgressBar progressBar_Update;
        private System.Windows.Forms.Label labelSpeed;
        private System.Windows.Forms.Label labelPerc;
        private System.Windows.Forms.Label labelDownloaded;
        private System.Windows.Forms.Button button_Download;
        private System.Windows.Forms.Label reason_label;
    }
}