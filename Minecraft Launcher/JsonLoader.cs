﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Windows.Forms;

namespace Minecraft_Launcher
{
    class JsonLoader
    {
        /// <summary>
        /// Parses the minecraft json related to a specific version.
        /// </summary>
        /// <param name="jsonFile">The json file for a sepcific version. Example: "C:\LOCATION\1.6.2.json"</param>
        /// <returns>Returns a dictonary with all of the elements.</returns>
        /// 

        public static string[] libraries = { "" };
        public static string[] urlLibraries = { "" };
        public static string[] natives = { "" };

                //public static List<string> libraries_List = new List<string>();
              //  public static List<string> natives_List = new List<string>();
               // public static List<string> urlLibraries_List = new List<string>();

        public static int l = 0;

        public static int n = 0;

        internal static Dictionary<string, string[]> getVersionData(string jsonFile)
        {

            //libraries_List.Clear(); 
            //natives_List.Clear(); 
            //urlLibraries_List.Clear();

            Array.Clear(libraries, 0, libraries.Length);
            Array.Clear(urlLibraries, 0, urlLibraries.Length);
            Array.Clear(natives, 0, natives.Length);
            l = 0;
            n = 0;
            /*
            string archType = "32";
            bool is64Bit = Environment.Is64BitProcess;
            if (is64Bit) {
                archType = "64";
            }
            natives = null;
            libraries = null;
            urlLibraries = null;
            */



            var json = System.IO.File.ReadAllText(jsonFile);
            dynamic version = JsonConvert.DeserializeObject(json);
            //string[] libraries = { "" };
            //string[] urlLibraries = { "" };
            //int l = 0;
            //int n = 0;

            if (version.inheritsFrom != null)
            {
                string inheritVersion = version.inheritsFrom;
                string tmp = Main.exePath + "\\versions\\" + inheritVersion + "\\" + inheritVersion + ".json";
                if (File.Exists(tmp))
                {
                    //MessageBox.Show(inheritVersion + ".json exists " + tmp);
                    var jsonInherit = System.IO.File.ReadAllText(tmp);
                    dynamic versionInherit = JsonConvert.DeserializeObject(jsonInherit);

                    ///===========================================
                    ///
                    libraryAdd(versionInherit);
             
                    //==========================================
                }
                else
                {
                    //MessageBox.Show("This version requires you to have minecraft '" + inheritVersion + "' installed. You can probably find this to download at [Settings]>[Minecraft Releases]");


                    string versionMC = inheritVersion;
                    string versionJar = "http://s3.amazonaws.com/Minecraft.Download/versions/" + inheritVersion + "/" + inheritVersion + ".jar";
                    string versionJson = "http://s3.amazonaws.com/Minecraft.Download/versions/" + inheritVersion + "/" + inheritVersion + ".json";
                    string versionJarPath = @"\versions\" + inheritVersion + @"\" + Path.GetFileName(versionJar);
                    string versionJsonPath = @"\versions\" + inheritVersion + @"\" + Path.GetFileName(versionJson);

                    string VersionDirectory = Main.exePath + @"\versions\" + versionMC;
                    string GameVersionDirectory = Main.exePath + @"\" + versionMC;

                    DialogResult yesnoDialog = MessageBox.Show("This version requires you to have Minecraft '" + inheritVersion + "' installed. \n\n You can probably find that version to download at [Settings]>[Minecraft Releases], or simply press the 'Yes' button to try download now", "Missing \\versions\\" + inheritVersion + "\\" + inheritVersion + ".json", MessageBoxButtons.YesNo);
                    if (yesnoDialog == DialogResult.Yes)
                    {
                        Dictionary<int, string[]> VersionDownload = new Dictionary<int, string[]>{
                 {0, new string[] { versionJar, versionJarPath }},
                 {1, new string[] { versionJson, versionJsonPath }}
                  };

                        //   Dictionary<int, string[]> demoDownloadDictonary = new Dictionary<int, string[]>{
                        //{0, new string[] { versionJar, path }},
                        //{1, new string[] { versionJson, path }},
                        //{2, new string[] { "http://kraftzone.net/downloads/MinecraftLauncher_v1.10.zip", @"Folder\MinecraftLauncher_v1.10.zip" }},
                        //{3, new string[] { "http://kraftzone.net/downloads/LiteLoader1.7.2.jar", @"Folder\LiteLoader1.7.2.jar" }}
                        // };

                        Downloader MultifileDownload = new Downloader(VersionDownload, Main.exePath, Main.exePath + versionJsonPath);
                        MultifileDownload.ShowDialog();

                         tmp = Main.exePath + "\\versions\\" + inheritVersion + "\\" + inheritVersion + ".json";
                        if (File.Exists(tmp))
                        {
                            var jsonInherit = System.IO.File.ReadAllText(tmp);
                            dynamic versionInherit = JsonConvert.DeserializeObject(jsonInherit);

                            ///===========================================
                            ///
                            libraryAdd(versionInherit);

                            //==========================================
                        }

                    }
                }
            }

            libraryAdd(version);
            
            string tmpAssets = "legacy";
            if (version.assets != null)
            {
                tmpAssets = version.assets;
            }

            //string[] libraries = libraries_List.ToArray();
            //string[] natives = natives_List.ToArray();
            //string[] urlLibraries = urlLibraries_List.ToArray();

            Dictionary<string, string[]> dict = new Dictionary<string, string[]>{
                {"id" , new string[] { version.id }},
                {"time" , new string[] { version.time }},
                {"releaseTime" , new string[] { version.releaseTime }},
                {"type" , new string[] { version.type }},
                {"minecraftArguments" , new string[] { version.minecraftArguments }},
                {"mainClass" , new string[] { version.mainClass }},
                {"assets" , new string[] { tmpAssets }},
              //{"libraries" , new string[] { "net/sf/jopt-simple/jopt-simple/4.5/jopt-simple-4.5.jar" "etc" "etc" }},
                {"libraries" , libraries },
                {"urlLibraries" , urlLibraries },
              //{"natives" , new string[] { "net/sf/jopt-simple/jopt-simple/4.5/jopt-simple-4.5.jar" "etc" "etc" }},
                {"natives" , natives }
            };
            return dict;
        }

        /// <summary>
        /// Parses the minecraft verion list.
        /// </summary>
        /// <param name="jsonFile">The json file for the list of versions. Example: "C:\LOCATION\verions.json"</param>
        /// <returns>Returns a dictonary with all of the elements.</returns>
        internal static Dictionary<string, string[]> getVersionList(string jsonFile)
        {
            var json = System.IO.File.ReadAllText(jsonFile);
            dynamic version = JsonConvert.DeserializeObject(json);
            Dictionary<string, string[]> dict = new Dictionary<string, string[]>{
                {"AL_LatestID", new string[] { version.latest.release, version.latest.snapshot }},
              //{"1.6.4", new string[] { "time", "releaseTime", "type" }}
            };
            foreach (var entry in version.versions)
            {
                string keyString = entry.id;
                string[] arrString = { entry.time, entry.releaseTime, entry.type };
                dict.Add(keyString, arrString); //Odd errors if put in directly.
            }
            return dict;
        }


        internal static void libraryAdd(dynamic jsonfile)
        {
            string archType = "32";
            bool is64Bit = Environment.Is64BitProcess;
            if (is64Bit)
            {
                archType = "64";
            }

               foreach (var param in jsonfile.libraries)
                    {

                        bool isNative = false;
                        bool addFile = true;
                        bool modFile = false;

                        if (param.rules != null)
                        {
                            foreach (var entry in param.rules)
                            {
                                if (entry.action == "allow")
                                {
                                    if (entry.os != null)
                                    {
                                        if (entry.os.name != "windows")
                                        {
                                            addFile = false;
                                        }
                                    }
                                }
                                else
                                {
                                    if (entry.os != null)
                                    {
                                        if (entry.os.name == "windows")
                                        {
                                            addFile = false;
                                        }
                                    }
                                    else
                                    {
                                        addFile = false;
                                    }
                                }
                            }
                        }
                        if (param.url != null)
                        {
                            modFile = true;
                        }

                        //removed, removes native files from libraries but also natives and does not get extracted.
                        /*
                        //Added exception for 1.7.2, shadermod does not work if they are loaded
                        if (param.name == "net.java.jinput:jinput-platform:2.0.5")
                        {
                            //  System.Windows.Forms.MessageBox.Show(param.ToString());
                            addFile = false;
                        }
                        //Added exception for 1.7.2, shadermod does not work if they are loaded
                        if (param.name == "org.lwjgl.lwjgl:lwjgl-platform:2.9.0")
                        {
                            // System.Windows.Forms.MessageBox.Show(param.ToString());
                            addFile = false;
                        }*/


                        if (addFile)
                        {
                            string fileName = param.name;
                            string[] colonSplit = fileName.Split(new char[] { ':' }, 3);
                            string[] folderSplit = colonSplit[0].Split(new char[] { '.' });
                            string compileFolder = "";
                            foreach (string entry in folderSplit)
                            {
                                compileFolder += entry + @"\";
                            }
                            string compileSplit = "";
                            if (param.natives != null)
                            {
                                isNative = true;
                                compileSplit = compileFolder + colonSplit[1] + @"\" + colonSplit[2] + @"\" + colonSplit[1] + "-" + colonSplit[2] + "-" + param.natives.windows + ".jar";
                            }
                            else
                            {
                                compileSplit = compileFolder + colonSplit[1] + @"\" + colonSplit[2] + @"\" + colonSplit[1] + "-" + colonSplit[2] + ".jar";
                            }
                            string compileComplete = compileSplit.Replace("${arch}", archType);

                            //if check added, so native file does not get added into libraries
                            if (!isNative)//
                            {
                          
                                //libraries_List.Add(compileComplete);
                                
                                if (l > libraries.Length - 1)
                                {
                                    Array.Resize(ref libraries, libraries.Length + 1);
                                    Array.Resize(ref urlLibraries, libraries.Length + 1);
                                }
                                //Added into if check
                                libraries[l] = compileComplete;//
                                 
                            }//
                            //libraries[l] = compileComplete;

                            if (modFile)
                            {


                                urlLibraries[l] = param.url;
                            }
                            else
                            {
                                urlLibraries[l] = "";
                            }

                            //if check added, so native file does not get added into libraries
                            if (!isNative)
                            {
                                l++;
                            }
                            if (isNative)
                            {
                                if (n > natives.Length - 1)
                                {
                                    Array.Resize(ref natives, natives.Length + 1);
                                }
                                natives[n] = compileComplete;
                                n++;
                            }
                        }
                    }
        }
        /// <summary>
        /// Parses the minecraft index file for minecraft assets.
        /// </summary>
        /// <param name="jsonFile">The json file for the list of assets. Example: "C:\LOCATION\verions.json"</param>
        /// <returns>Returns a dictonary with all of the elements.</returns>
        internal static Dictionary<string, string[]> getAssetsList(string jsonFile)
        {
            var json = System.IO.File.ReadAllText(jsonFile);
            dynamic assets = JsonConvert.DeserializeObject(json);
            Dictionary<string, string[]> dict = new Dictionary<string, string[]>
            {
                //{"objectFileLocation", new string[] { "hash", "size" }},
            };
            var values = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(Convert.ToString(assets.objects));
            foreach (KeyValuePair<string, dynamic> entry in values)
            {
                dict.Add(entry.Key, new string[] { entry.Value.hash, entry.Value.size });
            }
            return dict;
        }
    }
}