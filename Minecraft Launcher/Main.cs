﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
//remove below
using System.IO;
using Microsoft.Win32;
//used to run stuff with Process
using System.Diagnostics;
using System.Xml;
//for shortfile
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Ionic.Zip;

namespace Minecraft_Launcher
{
    public partial class Main : Form
    {


        /// <summary>
        /// ///// Code to allow log text scrollbar to autoscroll unless user moves scrollbar ////////
        /// </summary>


        //// Constants for extern calls to various scrollbar functions
        //private const int SB_VERT = 0x1;
        //private const int WM_VSCROLL = 0x115;
        //private const int SB_THUMBPOSITION = 4;
        //private const int SIF_TRACKPOS = 10;

        //// Structure to hold scroll bar info
        //[StructLayout(LayoutKind.Sequential)]
        //public struct ScrollInfo
        //{
        //    public int cbSize;
        //    public int fMask;
        //    public int nMin;
        //    public int nMax;
        //    public int nPage;
        //    public int nPos;
        //    public int nTrackPos;
        //}

        //// The SetScrollPos function sets the position of the scroll box (thumb) in the specified scroll bar and, if requested, 
        //// redraws the scroll bar to reflect the new position of the scroll box
        //[DllImport("user32.dll")]
        //static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

        //// The GetScrollPos function retrieves the current position of the scroll box (thumb) in the specified scroll bar
        //[DllImport("user32.dll", CharSet = CharSet.Auto)]
        //private static extern int GetScrollPos(IntPtr hWnd, int nBar);

        //// Retrieves the parameters of a scroll bar, including the minimum and maximum scrolling positions, the page 
        //// size, and the position of the scroll box (thumb)
        //[DllImport("user32.dll", CharSet = CharSet.Auto)]
        //private static extern int GetScrollInfo(IntPtr hWnd, int fnBar, ref ScrollInfo lpScrollInfo);

        //// Places (posts) a message in the message queue associated with the thread that created the specified window 
        //// and returns without waiting for the thread to process the message
        //[DllImport("user32.dll")]
        //private static extern bool PostMessageA(IntPtr hWnd, int nBar, int wParam, int lParam);

        //// Retrieves the current minimum and maximum scroll box (thumb) positions for the specified scroll bar
        //[DllImport("user32.dll")]
        //static extern bool GetScrollRange(IntPtr hWnd, int nBar, out int lpMinPos, out int lpMaxPos);
        

        /// <summary>
        /// /////////////////END SCROLLBAR///////////////////
        /// </summary>

        internal static Dictionary<string, string[]> versionData = new Dictionary<string, string[]>
        {
            //{"id"                  , new string[] { "1.6.4" }},
            //{"time"                , new string[] { "2013-09-19T10:52:37-05:00" }},
            //{"releaseTime"         , new string[] { "2013-09-19T10:52:37-05:00" }},
            //{"Type"                , new string[] { "release" }},
            //{"minecraftArguments"  , new string[] { "--username ${auth_player_name} --session ${auth_session} --version ${version_name} --gameDir ${game_directory} --assetsDir ${game_assets} --uuid ${auth_uuid} --accessToken ${auth_access_token}" }},
            //{"mainClass"           , new string[] { "net.minecraft.client.main.Main" }},
            //{"libraries"           , new string[] { "net\sf\jopt-simple\jopt-simple\4.5\jopt-simple-4.5.jar" "etc" "etc" }},
            //{"natives"             , new string[] { "net\sf\jopt-simple\jopt-simple\4.5\jopt-simple-4.5.jar" "etc" "etc" }},
        };

        internal static Dictionary<string, string[]> assetsList = new Dictionary<string, string[]>
        {
            //{"objectFileLocation", new string[] { "hash", "size" }},
            //{"objectFileLocation", new string[] { "hash", "size" }},
            //{"objectFileLocation", new string[] { "hash", "size" }},
            //{"etc", new string[] { "etc", "etc" }},
            //{"etc", new string[] { "etc", "etc" }},
            //{"etc", new string[] { "etc", "etc" }},
        };

        internal static Dictionary<int, string[]> DownloadList = new Dictionary<int, string[]>();


        internal static int DownloadListTotalFiles = 0;

        //check 64bit
        [DllImport("kernel32", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        public extern static IntPtr LoadLibrary(string libraryName);

        [DllImport("kernel32", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        public extern static IntPtr GetProcAddress(IntPtr hwnd, string procedureName);

        private delegate bool IsWow64ProcessDelegate([In] IntPtr handle, [Out] out bool isWow64Process);

        public static bool IsOS64Bit()
        {
            if (IntPtr.Size == 8 || (IntPtr.Size == 4 && Is32BitProcessOn64BitProcessor()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static IsWow64ProcessDelegate GetIsWow64ProcessDelegate()
        {
            IntPtr handle = LoadLibrary("kernel32");

            if (handle != IntPtr.Zero)
            {
                IntPtr fnPtr = GetProcAddress(handle, "IsWow64Process");

                if (fnPtr != IntPtr.Zero)
                {
                    return (IsWow64ProcessDelegate)Marshal.GetDelegateForFunctionPointer((IntPtr)fnPtr, typeof(IsWow64ProcessDelegate));
                }
            }

            return null;
        }

        public static bool IsNullOrEmpty(string testString)
        {
            return string.IsNullOrEmpty(testString);
        }
        private static bool Is32BitProcessOn64BitProcessor()
        {
            IsWow64ProcessDelegate fnDelegate = GetIsWow64ProcessDelegate();

            if (fnDelegate == null)
            {
                return false;
            }

            bool isWow64;
            bool retVal = fnDelegate.Invoke(Process.GetCurrentProcess().Handle, out isWow64);

            if (retVal == false)
            {
                return false;
            }

            return isWow64;
        }

        //endcheck 64bit


        [DllImport("kernel32")]
        public static extern int GetShortPathName(
            string lpszLongPath, StringBuilder lpszShortPath,
            int bufSize);


        public string Getshorty(string longFileName)
        {
            StringBuilder sb = new StringBuilder(1024);
            GetShortPathName(longFileName, sb, sb.Capacity);

            string shortFileName = sb.ToString();

            
            return shortFileName;
        }

        //Gets folder location of the running exe "C:\Launcher"  
        internal static string exePath = System.Windows.Forms.Application.StartupPath;


        private List<Profile> _profiles = new List<Profile>();


        //string exePath = @"C:\Games\MC1.6.1";
        private string javaLoc = "";
        private string autoconnect = "";
        private string screenmode = "";
        private string userName = "";
        private string AuthCheckbox = "no";

        public string kzLauncherURL = "http://kraftzone.net/KzLauncher/";

        //  public string mc172filename = "MC_1.7.2_Files.zip";
        // public int mc172bytesize = 100431113;

        public string mc172filename = "MC_1.7.2_Files.zip";
        public int mc172bytesize = 111001245;

        public string mc162filename = "MC_1.6.2_Liteloader.zip";
        public int mc162bytesize = 51081971;

        public string jre32filename_java7 = "jre1.7.0_25_32bit.zip";
        public int jre32bytesize_java7 = 40880391;
        public string jre64filename_java7 = "jre1.7.0_25_64bit.zip";
        public int jre64bytesize_java7 = 42470950;

        public string jre32filename_java8 = "jre1.8.0_05_32bit.zip";
        public int jre32bytesize_java8 = 46453500;
        public string jre64filename_java8 = "jre1.8.0_05_64bit.zip";
        public int jre64bytesize_java8 = 49409722;


        private System.IO.FileSystemWatcher m_Watcher;
        private bool WatchLog = true;

        public bool foundversion = false;

        public string Xmx = "-Xmx1024m ";
        public decimal CurrentXMX = 512;
        public string JavaFinalArguements = "";
        public string JavaVersionArgs = "";
        public string JavaVersionXmX = "1024";
        public string JavaCustomXmX = "1024";
        public string JavaCustomArgs = "";
        public string SettingsAfterLaunch = "";
        public string SelectedProfile = "";

        public string LauncherUpdatedFile = null;
        public string LauncherFileName = "Minecraft Launcher.exe";
        public decimal LauncherVersion = 1.25M;

        public string fake = "";
        public string LaunchErrors = "";


        internal static List<string> JavaPortableFound = new List<string>();
        public bool uninstallversion = false;

        //this stuff causes issues if someones computer does not have perfmon working properly
        //http://answers.microsoft.com/en-us/windows/forum/windows_7-performance/perfmon-problems-unable-to-add-counters/e90f231d-0014-457d-8b1f-5f342971597a
        //http://stackoverflow.com/questions/3121688/performance-counter-input-string-was-not-in-a-correct-format-c-sharp




        Dictionary<string, int> missingLibraries = new Dictionary<string, int>();


        Dictionary<int, string[]> GetMissingFiles = new Dictionary<int, string[]>();

        const string LOG_NAME = "Test";
        private LogString myLogger = LogString.GetLogString(LOG_NAME);

      //  protected System.Diagnostics.PerformanceCounter cpuCounter;
       // protected System.Diagnostics.PerformanceCounter ramCounter;

        
        public Main()
        {
            InitializeComponent();
            //Load username settings
            textBox_Username.Text = "#Delete#";



            //Required to fool forge installer, as it checks for the officla minecraft launcher file.
            if (!File.Exists("launcher_profiles.json"))
            {
                WriteResourceToFile("Minecraft_Launcher.Resources.launcher_profiles.json", "launcher_profiles.json");

                //ExtractEmbeddedResource(exePath, "Minecraft_Launcher.Resources.launcher_profiles.json", "launcher_profiles.json");
            }
            
            //System.Reflection.Assembly hostAssembly = this.GetType().Assembly;
           // System.IO.Stream resourceStream = hostAssembly.GetManifestResourceStream("EmbeddedResourceFileDemo.EmbeddedResource.txt");

          // System.Resources.ResourceManager rm = new System.Resources.ResourceManager("Minecraft_Launcher.Resources.launcher_profiles.json", System.Reflection.Assembly.GetExecutingAssembly());

            //fake = rm.GetString("Minecraft_Launcher.Resources.launcher_profiles.json");//Some Text attributes
            //MessageBox.Show(fake);


            //Get the full filename or prefix of a resource file.. debug code
            //ADded file "launcher_profiles.json"
            //Is actually when compiled...
            //"Minecraft_Launcher.Resources.launcher_profiles.json"
//string[] names = this.GetType().Assembly.GetManifestResourceNames();
            //foreach (string s in names){
            //    MessageBox.Show(s);
          //  }


            //get the exe file name, should be "Minecraft Launcher.exe" but check incase it was renamed.
            LauncherFileName = Path.GetFileName(System.Reflection.Assembly.GetEntryAssembly().Location);

            //get exe version
            FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(LauncherFileName);
            LauncherVersion = decimal.Parse(myFileVersionInfo.FileVersion);



          
            Log_richTextBox.MaxLength = myLogger.MaxChars + 100;

            // Add update callback delegate
            myLogger.OnLogUpdate += new LogString.LogUpdateDelegate(this.LogUpdate);

            button_Addprofile.Enabled = false;
            button_Save.Enabled = false;



            label_mcversionnumber.Text = "v" + LauncherVersion.ToString();


            //JAVA PORTABLE CHECK
            //MessageBox.Show(exePath);
            radioButton_Java7_32.Enabled = true;
            radioButton_Java7_64.Enabled = true;
            radioButton_Java8_32.Enabled = true;
            radioButton_Java8_64.Enabled = true;
            radioButton_Minecraft172.Enabled = true;

            // radioButton_Minecraft164.Enabled = true;
            label_tickjava32_java7.Text = " x";
            label_tickjava32_java7.ForeColor = Color.Red;
            label_tickjava64_java7.Text = " x";
            label_tickjava64_java7.ForeColor = Color.Red;
            label_tickmc172.Text = " x";
            label_tickmc172.ForeColor = Color.Red;





            JavaPortableCheck();

           

            //VERSIONS CHECK
            comboBox_MCVersion.Enabled = false;
            try
            {
                String[] VersionFolders = Directory.GetDirectories(exePath + @"\versions");

                foreach (string s in VersionFolders)
                {
                    string removeBaseDir = s.Replace(exePath + @"\versions\", "");
                    //MessageBox.Show(removeBaseDir);

                    if (removeBaseDir == "1.7.2")
                    {
                        foundversion = true;
                        radioButton_Minecraft172.Enabled = true;
                        label_tickmc172.Text = "✓";
                        label_tickmc172.ForeColor = Color.LimeGreen;
                    }
                    comboBox_MCVersion.Items.Add(removeBaseDir);
                    comboBox_MCVersion.Enabled = true;
                    comboBox_MCVersion.Text = removeBaseDir;
                    string myPath = exePath + "\\" + removeBaseDir;
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(@"'versions' directory not found. " + ex.Message);
                radioButton_Minecraft172.Enabled = File.Exists(mc172filename);
                //tabControl_Main.SelectTab(tabPage_Updates);

                // Update up = new Update("http://kraftzone.tk/KzLauncher/mc1.6.2Files.zip", "Get files required to launch mc?");
                // Update up = new Update("http://kraftzone.tk/KzLauncher/jre1.7.0_25_64bit.zip","Get files required to launch mc?");
                //Update up = new Update(kzLauncherURL + mc172filename, "Get files required to launch mc?", mc172bytesize, false);
                //  up.ShowDialog();

                //  Update up = new Update("http://178.32.72.146/MC_1.6.2_Files.zip", "Get files required to launch mc?"); 
                //cannot minimize

                // up.Show();

            }
            finally
            {
                //  radioButton_Minecraft172.Enabled = File.Exists(mc172filename);
                // radioButton_Minecraft162.Enabled = false;

                //    label_tickmc172.Text = "✓";
                //   label_tickmc172.ForeColor = Color.LimeGreen;
            }
            //if (!foundversion)
            // {
            //     label_tickmc164.Text = "✓";
            //     label_tickmc164.ForeColor = Color.LimeGreen;

            //    Update up = new Update(kzLauncherURL + mc171filename, "Download files for mc 1.7.1?", mc171bytesize, false);
            //     up.ShowDialog();
            //     }
            if (!File.Exists(mc172filename))
            {
                //MessageBox.Show("test");
                radioButton_Minecraft172.Enabled = true;
                label_tickmc172.Text = "x";
                label_tickmc172.ForeColor = Color.Red;
            }
        }

        private void JavaPortableCheck()
        {
            JavaPortableFound.Clear();
            comboBox_JavaVersion.Items.Clear();
            try
            {
                String[] MainDirFolders = Directory.GetDirectories(exePath);
                radioButton_JavaPortable.Enabled = false;
                foreach (string s in MainDirFolders)
                {
                    //string fixed3 = Regex.Replace(s, @"(C:\\Games\\MC1.6.1)", "");
                    string removeBaseDir = s.Replace(exePath + "\\", "");
                    string javaFound = "";
                    if (removeBaseDir == "jre1.7.0_25_32bit")
                    {
                        //string java32 = "jre1.7.0_25_32bit";
                        javaLoc = "\\jre1.7.0_25_32bit\\bin\\javaw.exe";
                        comboBox_JavaVersion.Items.Add("Java 7 (32bit)");
                        javaFound = "32";
                        radioButton_JavaPortable.Enabled = true;

                        radioButton_Java7_32.Enabled = File.Exists(jre32filename_java7);

                        label_tickjava32_java7.Text = "✓";
                        label_tickjava32_java7.ForeColor = Color.LimeGreen;
                        JavaPortableFound.Add(removeBaseDir);
                    }
                    if (removeBaseDir == "jre1.7.0_25_64bit")
                    {
                        //string java64 = "jre1.7.0_25_64bit";

                        comboBox_JavaVersion.Items.Add("Java 7 (64bit)");
                        javaFound = "64";
                        radioButton_JavaPortable.Enabled = true;
                        radioButton_Java7_64.Enabled = File.Exists(jre64filename_java7);
                        label_tickjava64_java7.Text = "✓";
                        label_tickjava64_java7.ForeColor = Color.LimeGreen;
                        JavaPortableFound.Add(removeBaseDir);
                    }
                    if (removeBaseDir == "jre1.8.0_05_32bit")
                    {
                        //string java32 = "jre1.7.0_25_32bit";
                        javaLoc = "\\jre1.8.0_05_32bit\\bin\\javaw.exe";
                        comboBox_JavaVersion.Items.Add("Java 8 (32bit)");
                        javaFound = "32";
                        radioButton_JavaPortable.Enabled = true;

                        radioButton_Java8_32.Enabled = File.Exists(jre32filename_java8);

                        label_tickjava32_java8.Text = "✓";
                        label_tickjava32_java8.ForeColor = Color.LimeGreen;
                        JavaPortableFound.Add(removeBaseDir);
                    }
                    if (removeBaseDir == "jre1.8.0_05_64bit")
                    {
                        //string java64 = "jre1.7.0_25_64bit";

                        comboBox_JavaVersion.Items.Add("Java 8 (64bit)");
                        javaFound = "64";
                        radioButton_JavaPortable.Enabled = true;
                        radioButton_Java8_64.Enabled = File.Exists(jre64filename_java8);
                        label_tickjava64_java8.Text = "✓";
                        label_tickjava64_java8.ForeColor = Color.LimeGreen;
                        JavaPortableFound.Add(removeBaseDir);
                    }
                    if (IsOS64Bit() && javaFound == "64")
                    {
                        comboBox_JavaVersion.Text = "Java 7 (64bit)";
                        radioButton_JavaPortable.Checked = true;
                    }
                    else if (IsOS64Bit() && javaFound == "32")
                    {
                        comboBox_JavaVersion.Text = "Java 7 (32bit)";
                        radioButton_JavaPortable.Checked = true;
                    }

                    if (!IsOS64Bit() && javaFound == "32")
                    {
                        comboBox_JavaVersion.Text = "Java 7 (32bit)";
                        radioButton_JavaPortable.Checked = true;
                    }
                    if (!IsOS64Bit() && javaFound == "64")
                    {
                        comboBox_JavaVersion.Text = "Java 7 (64bit)";
                        //radioButton_JavaPortable.Enabled = false;
                        //radioButton_JavaDefault.Checked = true;
                    }
                    //MessageBox.Show(removeBaseDir);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(@"Directies not found. " + ex.Message);
                comboBox_JavaVersion.Enabled = false;
                radioButton_JavaPortable.Enabled = false;
                radioButton_JavaDefault.Checked = true;
            }
        }

        // Updates that come from a different thread can not directly change the
        // TextBox component. This must be done through Invoke().
        private delegate void UpdateDelegate();
        private void LogUpdate()
        {
            Invoke(new UpdateDelegate(
                delegate
                {
                   
                    Log_richTextBox.Text = myLogger.Log;

                    if (checkBoxAutoScroll.Checked) {
                        Log_richTextBox.SelectionStart = Log_richTextBox.Text.Length;
                        Log_richTextBox.ScrollToCaret();
                    }


                })
            );
        }


        #region MainLoad
        private void Main_Load(object sender, EventArgs e)
        {


            //      Dictionary<string, int> Files = new Dictionary<string, int>
            // {
            //   { "1.7.2", 9001 },
            //  { "1.6.2Liteloader", 3474 },
            //  { "1.7.2Liteloader", 11926 }
            // };
            //    string json = JsonConvert.SerializeObject(Files, Newtonsoft.Json.Formatting.Indented);

            // Console.WriteLine(json);
            // MessageBox.Show(json);
            
            Log_richTextBox.Text = myLogger.Log;

            if (!File.Exists(exePath + "\\Minecraft Launcher.xml"))
            {
                XmlTextWriter xW = new XmlTextWriter(exePath + "\\Minecraft Launcher.xml", Encoding.UTF8);
                xW.WriteStartElement("Profiles");
                xW.WriteEndElement();
                xW.Close();
            }
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(exePath + "\\Minecraft Launcher.xml");
            foreach (XmlNode xNode in xDoc.SelectNodes("Profiles/Profile"))
            {
                Profile p = new Profile();
                p.ProProfile = xNode.SelectSingleNode("ProProfile").InnerText;
                p.ProUsername = xNode.SelectSingleNode("ProUsername").InnerText;
                p.ProMcVersion = xNode.SelectSingleNode("ProMcVersion").InnerText;
                p.ProJavaVersion = xNode.SelectSingleNode("ProJavaVersion").InnerText;
                p.ProScreenconfig = xNode.SelectSingleNode("ProScreenconfig").InnerText;
                p.ProServerconfig = xNode.SelectSingleNode("ProServerconfig").InnerText;

                p.ProAuthOn = xNode.SelectSingleNode("ProAuthOn").InnerText;
                p.ProAuthUser = xNode.SelectSingleNode("ProAuthUser").InnerText;
                p.ProAuthPass = xNode.SelectSingleNode("ProAuthPass").InnerText;

                p.ProAfterLaunch = xNode.SelectSingleNode("ProAfterLaunch").InnerText;
                p.ProCustomJavaXmx = xNode.SelectSingleNode("ProCustomJavaXmx").InnerText;
                p.ProCustomJava = xNode.SelectSingleNode("ProCustomJava").InnerText;

                p.ProSelected = xNode.SelectSingleNode("ProSelected").InnerText;

                if (string.IsNullOrEmpty(p.ProSelected) == false)
                {
                    SelectedProfile = p.ProSelected;
                    //MessageBox.Show("--"+SelectedProfile+"--");
                }
                _profiles.Add(p);
                listView_Profiles.Items.Add(p.ProProfile);

                comboBox_Profile.Items.Add(p.ProProfile);
                comboBox_Profile.Text = p.ProProfile;
            }

            if (listView_Profiles.Items.Count > 0)
            {
                listView_Profiles.Items[0].Selected = true;
                listView_Profiles.Select();
            }
            comboBox_Profile.Text = SelectedProfile;
            comboBox_Profile.Select();
            //Enable Launcher button
            textBox_Username.Select();

            string myPath = exePath + "\\" + comboBox_MCVersion.Text;
        }

        #endregion



        private void textcheck()
        {
            string regexmc = "^[a-zA-Z0-9_]+$";

            Regex reg_exp = new Regex(regexmc);
            if (reg_exp.IsMatch(textBox_Username.Text))
            {
                textBox_Username.BackColor = Color.LimeGreen;
                label_Error.Visible = false;
                button_Launch.Enabled = true;

                if (button_Addprofile.Enabled == true)
                {
                    button_Save.Enabled = true;
                }
                button_Save.Enabled = true;
            }
            else
            {
                textBox_Username.BackColor = Color.Yellow;
                label_Error.Visible = true;
                button_Launch.Enabled = false;
                button_Save.Enabled = false;
            }
        }

        private void textBox_Username_TextChanged(object sender, EventArgs e)
        {
            textcheck();
        }

        private void labelcheck()
        {
            if (label_Error.Visible == false)
            {
                button_Save.Enabled = true;
            }
        }

        //-----------------server group--------------------
        private void radioButton_NoAuto_CheckedChanged(object sender, EventArgs e)
        {
            textBox_CustomServer.ReadOnly = true;
            autoconnect = "";
            labelcheck();
        }

        private void radioButton_Kraftzone_CheckedChanged(object sender, EventArgs e)
        {
            textBox_CustomServer.ReadOnly = true;
            autoconnect = "mc.kraftzone.net";
            labelcheck();
        }

        private void radioButton_CustomServer_CheckedChanged(object sender, EventArgs e)
        {
            textBox_CustomServer.ReadOnly = false;
            labelcheck();
        }

        private void textBox_CustomServer_TextChanged(object sender, EventArgs e)
        {
            if (radioButton_CustomServer.Checked == false)
            {
                radioButton_CustomServer.Checked = true;
                autoconnect = textBox_CustomServer.Text;
            }
            autoconnect = textBox_CustomServer.Text;

            labelcheck();
        }

        //-----------------screen group--------------------
        private void radioButton_ScreenDefault_CheckedChanged(object sender, EventArgs e)
        {
            screenmode = "";

            labelcheck();
        }

        private void radioButton_Windowed720p_CheckedChanged(object sender, EventArgs e)
        {
            screenmode = " --width 1280 --height 720";
            labelcheck();
        }

        private void radioButton_Fullscreen_CheckedChanged(object sender, EventArgs e)
        {
            screenmode = " --fullscreen";

            labelcheck();
        }

        // BUTTONS------------------------------

        private void button_mcpatcher_Click(object sender, EventArgs e)
        {
            //string launcher = javaLoc + " -Xmx128m -jar " + fakePath + "mcpatcher-4.1.0_01.jar";

            string mcpatcher = "-Xmx128m -jar " + exePath + "\\mcpatcher-4.1.0_01.jar";

            //var start = Process.Start(exePath + javaLoc, mcpatcher);
            //javaLoc = "\\jre1.7.0_25_64bit\\bin\\javaw.exe";
            //ProcessStartInfo p = new ProcessStartInfo(exePath + javaLoc, mcpatcher);
            // p.UseShellExecute = false; // apparently required when adding environment variables
            //p.EnvironmentVariables.Add(@"APPDATA", @"C:\Games\MC1.6.1");

            //Process.Start(p);

            Process myProcess = new Process();

            try
            {
                myProcess.StartInfo.UseShellExecute = false;
                // You can start any process, HelloWorld is a do-nothing example.
                myProcess.StartInfo.FileName = exePath + "\\mcpatcher.bat";
                //MessageBox.Show(exePath);
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.Arguments = mcpatcher;

                myProcess.Start();
                // This code assumes the process you are starting will terminate itself.  
                // Given that is is started without a window so you cannot terminate it  
                // on the desktop, it must terminate itself or you can do it programmatically 
                // from this application using the Kill method.
            }
            catch (Exception exx)
            {
                MessageBox.Show(exx.Message);
            }
        }

        private static string myPassword = "password";
        //string mySalt = BCrypt.GenerateSalt();
        private static string mySalt = "$2a$10$aPL1FKhQONOy34MCohYLdu";

        private static string myHash = BCrypt.HashPassword(myPassword, mySalt);

        private bool doesPasswordMatch = BCrypt.CheckPassword(myPassword, myHash);
        //bool doesPasswordMatch = BCrypt.CheckPassword(myPassword, myHash);

        private string authsession = "${auth_session}";
        private string startupcheck()
        {

            string exePathShort = Getshorty(exePath);
            if (radioButton_JavaDefault.Checked == true)
            {
                return "javaw";
            }
            else if (radioButton_JavaPortable.Checked == true)
            {
                if (comboBox_JavaVersion.Text == "Java 7 (64bit)")
                {
                    javaLoc = "\\jre1.7.0_25_64bit\\bin\\javaw.exe";
                    return "\"" + exePathShort + javaLoc + "\"";
                }
                if (comboBox_JavaVersion.Text == "Java 7 (32bit)")
                {
                    javaLoc = "\\jre1.7.0_25_32bit\\bin\\javaw.exe";
                    return "\"" + exePathShort + javaLoc + "\"";
                }
                if (comboBox_JavaVersion.Text == "Java 8 (64bit)")
                {
                    javaLoc = "\\jre1.8.0_05_64bit\\bin\\javaw.exe";
                    return "\"" + exePathShort + javaLoc + "\"";
                }
                if (comboBox_JavaVersion.Text == "Java 8 (32bit)")
                {
                    javaLoc = "\\jre1.8.0_05_32bit\\bin\\javaw.exe";
                    return "\"" + exePathShort + javaLoc + "\"";
                }
            }
            
            radioButton_JavaDefault.Checked = true;
            return "javaw";

        }
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        void timer_Tick(object sender, System.EventArgs e)
        {
            button_Launch.Enabled = true;
            timer.Stop();
        }

        #region LaunchClink
        private void button_Launch_Click(object sender, EventArgs e)
        {
            //disable launch button for 3 seconds, prevent miss click launches
            button_Launch.Enabled = false;
            timer.Interval = 3000; // here time in milliseconds 3000=3seconds
            timer.Tick += timer_Tick;
            timer.Start();
            userName = textBox_Username.Text;
            
            string Uuid = "Uuid";
            string token = "authsessionToken";
            string twitch_token = "IGNORE";

            if (Auth_checkBox.Checked == true)
            {
                try
                {
                    var session = Session.DoLogin(Premium_user_textBox.Text, Premium_pass_textBox.Text);
                    // authsession = "token:" + session.SessionId + ":" + session.ClientToken;
                    authsession = session.SessionId;
                    Uuid = session.SelectedProfile.Id;
                    token = session.AccessToken;
                    userName = session.SelectedProfile.Name;

                    if (session.User.Properties != null && session.User.Properties[0x00000000].Value != null) {
                       twitch_token = session.User.Properties[0x00000000].Value;
                    }
                }
                catch(WebException ee)
                {

                    var stream = ee.Response.GetResponseStream();
                    var json = new StreamReader(stream).ReadToEnd();
                    //stream.Close();

                    MessageBox.Show(ee.Message);

                    button_Launch.Enabled = true;
                    MessageBox.Show("-Username/Passsword incorrect.\n-Mojang Login servers derping\n-Your internet is broken \n-Firewall blocking?\n\nTry again or untick 'Enable Authentication'\nThen you can launch offline mode");
                    return;
                }
            }

            string authsessionUuid = " --uuid " + Uuid;
            string authsessionToken = " --accessToken " + token;

            //MessageBox.Show(session.Username + " token:" + session.SessionId + ":" + session.UserId);

            //    string sourceDirectory = @"C:\source";
            //    string destinationDirectory = @"C:\destination";

            //   try
            //   {
            //       Directory.Move(sourceDirectory, destinationDirectory);
            //   }
            //   catch (Exception exx)
            //    {
            //        Console.WriteLine(exx.Message);
            //    }

            String serverconnect = "";

            if (radioButton_NoAuto.Checked == true)
            {
                serverconnect = "";
            }
            else if (radioButton_Kraftzone.Checked == true && Auth_checkBox.Checked == true)
            {
                serverconnect = " --server mc.kraftzone.net";
                //  serverconnect = " --server 188.165.251.178:25565";
            }
            else if (radioButton_Kraftzone.Checked == true && Auth_checkBox.Checked == false)
            {
                serverconnect = " --server mc.kraftzone.net:25575";
                //  serverconnect = " --server 188.165.251.178:25575";
            }
            else if (radioButton_CustomServer.Checked == true)
            {
                if (textBox_CustomServer.Text == "")
                {
                    serverconnect = "";
                }
                else
                {
                    serverconnect = " --server " + textBox_CustomServer.Text;
                }
            }
            string version = comboBox_MCVersion.Text;
            string startup = "";
            string jarfile = "\\versions\\" + version + "\\" + version + ".jar";
            string assets = "\\versions\\" + version + "\\" + version + "-natives";

            // string exePathShort = "\""+exePath;
            string exePathShort = Getshorty(exePath);
            string gameDir = "\\" + version;


            //fix for forge, it loads before minecraft, and minecraft creates the gamedir profile directory,
            //if forge loads first it doesn't create it, so this will do it instead.
            if (!Directory.Exists(exePathShort + gameDir))
            {
                Directory.CreateDirectory(exePathShort + gameDir);
            }
          
            //JavaVersionXmX = "1500";
            JVMCustom();

            JavaVersionArgs = "-XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump ";
            JavaVersionArgs += "-Dfml.ignoreInvalidMinecraftCertificates=true -Dfml.ignorePatchDiscrepancies=true ";
            JavaFinalArguements = JavaVersionArgs + "-Xmx" + JavaVersionXmX + "m " + JavaCustomArgs+" ";


            //AtomLauncher Help
            string liteloaderjson = exePathShort + "\\versions\\" + version + "\\" + version + ".json";
            if (File.Exists(liteloaderjson))
            {
                versionData = JsonLoader.getVersionData(liteloaderjson);
            }


            string mcLibraries = " -cp ";
            string mcClass = "";
            string mcArgs = "";
            int missingCounter = 0;
            try
            {
                foreach (string entry in versionData["libraries"])
                {
                    if (entry != null) { 
                   // MessageBox.Show(entry.ToString());
                   

                    string libraryfile = exePathShort + @"\libraries\" + entry;
                    mcLibraries = mcLibraries + "" + libraryfile + ";";

                    //check file size is no 0bytes
                    long filesize = 0;
                    try
                    {
                        FileInfo filecheck = new FileInfo(libraryfile);
                        filesize = filecheck.Length;
                    }
                    catch (Exception ex2)
                    {
                        
                        MessageBox.Show("Could not find file" + ex2);
                    }
                    if (!File.Exists(libraryfile) || filesize <= 300)
                    // if (!File.Exists(libraryfile))
                    {


                        missingLibraries.Add(entry, missingCounter);

                        //example https://libraries.minecraft.net/org/lwjgl-platform//lwjgl-platform-2.9.1-nightly.jar

                        string libraryJar = "https://libraries.minecraft.net/" + entry.Replace("\\", "/");

                        string[] folderSplit = entry.Split(new char[] { '\\' });


                        string libraryJarPath = @"\libraries\";

                        foreach (string subfolder in folderSplit)
                        {
                            libraryJarPath += subfolder + @"\";
                        }
                        //remove the trailing \ from end of the string
                        libraryJarPath = libraryJarPath.Remove(libraryJarPath.Length - 1);



                        Console.WriteLine("Missing Library " + libraryJarPath + "###" + libraryJar);
                        GetMissingFiles.Add(missingCounter, new string[] { libraryJar, libraryJarPath });

                        //MessageBox.Show("You are missing this file:" + libraryfile);
                        missingCounter++;
                    }
                    }
                }
                //MessageBox.Show(mcLibraries);
                mcClass = versionData["mainClass"][0];
                mcArgs = versionData["minecraftArguments"][0];
                mcArgs = mcArgs.Replace("${auth_player_name}", userName);

                //1.7.9 adds this... Add 'requestUser':true to the request JSON when you are Authenticating and Refreshing. You will get in response
                //--userProperties {"twitch_access_token":["ovxsz6xpdhlwblpbuamdr7k1l9t2iqr"]}
                
                mcArgs = mcArgs.Replace("${user_properties}", "{\"twitch_access_token\":[\""+twitch_token+"\"]}");
                mcArgs = mcArgs.Replace("${user_type}", "mojang");


                if (Auth_checkBox.Checked)
                {
                    mcArgs = mcArgs.Replace("${auth_session}", "token:" + token + ":" + Uuid);
                    mcArgs = mcArgs.Replace("${auth_uuid}", Uuid);
                    mcArgs = mcArgs.Replace("${auth_access_token}", token);
                }
                else
                {
                    mcArgs = mcArgs.Replace("${auth_session}", "OFFLINE_MODE");
                    mcArgs = mcArgs.Replace("${auth_uuid}", "OFFLINE_MODE");
                    mcArgs = mcArgs.Replace("${auth_access_token}", "OFFLINE_MODE");
                }
                //mcArgs = mcArgs.Replace("${version_name}", mcSelectVer);
                mcArgs = mcArgs.Replace("${version_name}", version);
                mcArgs = mcArgs.Replace("${game_directory}", "\"" + exePathShort + gameDir + "\"");

                //mcArgs = mcArgs.Replace("${game_assets}", "\"" + exePathShort + "\\assets\"");

                //older than 1.7.4
                mcArgs = mcArgs.Replace("${game_assets}", "\"" + exePathShort + "\\assets\\virtual\\legacy\"");
                //1.7.9 and newer
                mcArgs = mcArgs.Replace("${assets_root}", "\"" + exePathShort + "\\assets\"");
                mcArgs = mcArgs.Replace("${assets_index_name}", versionData["assets"][0]);

                //buildArguments = mcStartRam + " " + mcMaxRam + " " + mcNatives + " " + mcLibraries + mcJar + " " + mcClass + " " + mcArgs;
            }
            catch (Exception eeee)
            {
               
                MessageBox.Show("dunno " + eeee.Message);
            }
            if (missingCounter > 0)
            {



                string missingFiletext = "";
                foreach (KeyValuePair<string, int> file in missingLibraries)
                {
                    //Console.WriteLine("{0}, {1}", file.Key, file.Value);
                    missingFiletext += "\\libraries\\" + file.Key + "\n";


                }
                DialogResult question = MessageBox.Show("You are missing library file(s) to run " + version + ":\n" + missingFiletext + "\n Attempt to download the missing libraries?", "Ctrl+C to copy this error missing", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (question == DialogResult.Yes)
                {
                    //MessageBox.Show("downloading");
                    Downloader MultifileDownload = new Downloader(GetMissingFiles, exePath, "", "");
                    MultifileDownload.ShowDialog();
                }

            }
            //clear missing files in library
            missingLibraries.Clear();
            GetMissingFiles.Clear();
            missingCounter = 0;

            //Check native files and extract
            try
            {
                foreach (string entry in versionData["natives"])
                {
                    if (entry != null)
                    {
                        //MessageBox.Show(entry.ToString());
                        string path = exePath + @"\libraries\" + entry;

                        if (File.Exists(path))
                        {
                            Extract(exePath + @"\libraries\" + entry, exePath + @"\versions\" + versionData["id"][0] + @"\" + versionData["id"][0] + "-natives", "META-INF");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error 33003: "+ex.Message);
            }

            getFiles();



            string mc = JavaFinalArguements + "-Djava.library.path="
                + exePathShort + assets + mcLibraries + exePathShort + jarfile + " " + mcClass + " " + mcArgs + screenmode + serverconnect;

            //MessageBox.Show(testing);

            //string exePath = System.Windows.Forms.Application.StartupPath;
            //  

            //var start = Process.Start(startup, mc);
            string debugmc = "";

            if (checkBox_debug.Checked == true || checkBox_showconsole.Checked == true)
            {
                startup = startupcheck() + " ";
                debugmc = mc.Replace(";", ";\n");
            }
            if (checkBox_debug.Checked == true)
            {
                MessageBox.Show(startup + debugmc, "Press CTRL+C to copy this text");
            }


            var launcher = new StreamWriter(File.Create("Launcher.bat"));

            startup = startupcheck() + " ";
            //startup = startup + " ";

            launcher.Write(startup + mc);
            launcher.Dispose();


            Process Launchit = new Process();
            //ProcessStartInfo Launchit2 = new ProcessStartInfo(startup, mc);

            try
            {


                // You can start any process, HelloWorld is a do-nothing example.
                //Launchit.StartInfo.FileName = exePath + "\\Launcher.bat";

                if (checkBox_showconsole.Checked == true && checkBox_debug.Checked == false)
                {     //locks window fix
                    WatchLog = false;
                   //  m_Watcher.EnableRaisingEvents = false;
                   //  m_Watcher.Dispose();
                    Launchit.StartInfo.UseShellExecute = false;
                 
                    Launchit.StartInfo.FileName = startup;
                    Launchit.StartInfo.Arguments = mc;
                    Launchit.StartInfo.CreateNoWindow = true;
                    Launchit.StartInfo.WorkingDirectory = exePath;
                    //Dev//
                    //To Read Java Errors//
                    //Launchit.StartInfo.RedirectStandardError = false;

                    Launchit.StartInfo.RedirectStandardOutput = true;
                    Launchit.StartInfo.RedirectStandardError = true;

                    myLogger.Clear();
                    myLogger.Add(debugmc.Replace(";\n", ";\r\n"));
                    Launchit.Start();
                   
                    Launchit.OutputDataReceived += p_OutputDataReceived;
                    Launchit.ErrorDataReceived += p_ErrorDataReceived;

                    Launchit.BeginOutputReadLine();
                    Launchit.BeginErrorReadLine();
               
                    //Launchit.WaitForExit();



                } 
                else if (checkBox_debug.Checked == true)
                {
                    WatchLog = false;
                  //  m_Watcher.EnableRaisingEvents = false;
                  //  m_Watcher.Dispose();

                    Launchit.StartInfo.UseShellExecute = false;
                    myLogger.Clear();
                    
                    Launchit.StartInfo.FileName = startup;
                    Launchit.StartInfo.Arguments = mc;
                    Launchit.StartInfo.CreateNoWindow = true;
                    Launchit.StartInfo.WorkingDirectory = exePath;
                    //Dev//
                    //To Read Java Errors//
                    //Launchit.StartInfo.RedirectStandardError = false;

                  //  Launchit.StartInfo.RedirectStandardOutput = true;
                    Launchit.StartInfo.RedirectStandardError = true;
                    myLogger.Add(debugmc);
                   
                        Launchit.Start();

                        //Launchit.OutputDataReceived += p_OutputDataReceived;
                        //Launchit.ErrorDataReceived += p_ErrorDataReceived;

                        //Launchit.BeginOutputReadLine();
                        //Launchit.BeginErrorReadLine();

                        Launchit.WaitForExit();
                    
                    
                    string errorscaught = "";
                    errorscaught = Launchit.StandardError.ReadToEnd();
                    if (!IsNullOrEmpty(errorscaught))
                    // if (!IsNullOrEmpty(LaunchErrors))
                    {
                       // myLogger.Add(errorscaught);
                        MessageBox.Show(errorscaught, "Launch Error");
                    }
                }
                else
                {
                    WatchLog = true;
                    // .bat way
                    Launchit.StartInfo.UseShellExecute = false;
                    Launchit.StartInfo.FileName = exePath + "\\Launcher.bat";
                    Launchit.StartInfo.CreateNoWindow = true;
                    Launchit.Start();
                }
                // This code assumes the process you are starting will terminate itself.  
                // Given that is is started without a window so you cannot terminate it  
                // on the desktop, it must terminate itself or you can do it programmatically 
                // from this application using the Kill method.

                //Dev//
                //To Read Java Errors
              
 
            }
            catch (Exception exx)
            {
                myLogger.Add(exx.Message);
                if (checkBox_debug.Checked == false) {
                    MessageBox.Show(exx.Message, "Launch Error");
                }
            }
             if (SettingsAfterLaunch == "minimize")
            {
                this.WindowState = FormWindowState.Minimized;
            }
            if (SettingsAfterLaunch == "minimize_to_tray")
            {
                Hide();//hides the program on the taskbar
                notifyIcon_tray.Visible = true;//shows our tray icon
            }
            if (SettingsAfterLaunch == "close")
            {
                this.Close();
            }

        }
        #endregion

        private void radioButton_JavaPortable_CheckedChanged(object sender, EventArgs e)
        {
            comboBox_JavaVersion.Enabled = true;
            labelcheck();
        }

        private void radioButton_JavaDefault_CheckedChanged(object sender, EventArgs e)
        {
            comboBox_JavaVersion.Enabled = false;
            labelcheck();

            //int selectedIndex = comboBox_JavaVersion.SelectedIndex;
            //Object selectedItem = comboBox_JavaVersion.SelectedItem;

            //MessageBox.Show("Selected Item Text: " + selectedItem.ToString() + "\n" +
            //                "Index: " + selectedIndex.ToString());
        }

        public void SaveEverything()
        {
            button_Save.Enabled = false;
            button_SaveSettings.Enabled = false;
            try
            {
                //------------------save, same as addprofile-----------------------------------

                _profiles[listView_Profiles.SelectedItems[0].Index].ProProfile = textBox_ProfileName.Text;
                _profiles[listView_Profiles.SelectedItems[0].Index].ProUsername = textBox_Username.Text;
                _profiles[listView_Profiles.SelectedItems[0].Index].ProMcVersion = comboBox_MCVersion.Text;
                _profiles[listView_Profiles.SelectedItems[0].Index].ProScreenconfig = screenmode;

                _profiles[listView_Profiles.SelectedItems[0].Index].ProServerconfig = autoconnect;

                _profiles[listView_Profiles.SelectedItems[0].Index].ProAuthOn = Auth_checkBox.Checked == true
                                                                                    ? "yes"
                                                                                    : "no";
                _profiles[listView_Profiles.SelectedItems[0].Index].ProAuthUser = Premium_user_textBox.Text;
                _profiles[listView_Profiles.SelectedItems[0].Index].ProAuthPass = Premium_pass_textBox.Text;

                _profiles[listView_Profiles.SelectedItems[0].Index].ProAfterLaunch = SettingsAfterLaunch;


                if (checkBoxEnableXMX.Checked == true)
                {
                    //_profiles[listView_Profiles.SelectedItems[0].Index].ProCustomJavaXmx = Convert.ToString(Xmx);
                    _profiles[listView_Profiles.SelectedItems[0].Index].ProCustomJavaXmx = JavaCustomXmX;
                }
                else
                {
                    _profiles[listView_Profiles.SelectedItems[0].Index].ProCustomJavaXmx = "";
                }

                if (checkBoxCustomJAVA.Checked == true)
                {
                    _profiles[listView_Profiles.SelectedItems[0].Index].ProCustomJava = JavaCustomArgs;
                }
                else
                {
                    _profiles[listView_Profiles.SelectedItems[0].Index].ProCustomJava = "";
                }




                //possibly delete--------------------
                if (radioButton_NoAuto.Checked == true)
                {
                    _profiles[listView_Profiles.SelectedItems[0].Index].ProServerconfig = "";
                }
                else if (radioButton_Kraftzone.Checked == true)
                {
                    _profiles[listView_Profiles.SelectedItems[0].Index].ProServerconfig = "mc.kraftzone.net";
                }
                else if (radioButton_CustomServer.Checked == true)
                {
                    if (textBox_CustomServer.Text == "")
                    {
                        _profiles[listView_Profiles.SelectedItems[0].Index].ProServerconfig = "";
                    }
                    else
                    {
                        _profiles[listView_Profiles.SelectedItems[0].Index].ProServerconfig = textBox_CustomServer.Text;
                    }
                }

                //--------------------

                if (radioButton_JavaDefault.Checked == true)
                {

                    _profiles[listView_Profiles.SelectedItems[0].Index].ProJavaVersion = "javaw";
                }
                else if (radioButton_JavaPortable.Checked == true)
                {
                    if (comboBox_JavaVersion.Text == "Java 7 (64bit)")
                    {
                        _profiles[listView_Profiles.SelectedItems[0].Index].ProJavaVersion = "Java 7 (64bit)";
                    }
                    if (comboBox_JavaVersion.Text == "Java 7 (32bit)")
                    {
                        _profiles[listView_Profiles.SelectedItems[0].Index].ProJavaVersion = "Java 7 (32bit)";
                    }
                    if (comboBox_JavaVersion.Text == "Java 8 (64bit)")
                    {
                        _profiles[listView_Profiles.SelectedItems[0].Index].ProJavaVersion = "Java 8 (64bit)";
                    }
                    if (comboBox_JavaVersion.Text == "Java 8 (32bit)")
                    {
                        _profiles[listView_Profiles.SelectedItems[0].Index].ProJavaVersion = "Java 8 (32bit)";
                    }
                }

                //----------------------

                comboBox_Profile.Text = textBox_ProfileName.Text;
            }
            catch (Exception)
            {
                // MessageBox.Show(exx.Message);
                MessageBox.Show("Add a Profile first");
            }
        }


        //Saving Profile info
        private void button_Save_Click(object sender, EventArgs e)
        {
            SaveEverything();
        }

        private void comboBox_MCVersion_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelcheck();

            if (comboBox_MCVersion.Text == null || comboBox_MCVersion.Text == "")
            {
                openVersionFolder_Button.Enabled = false;
                //MessageBox.Show("asdasdasdd");
            }
            else {
                openVersionFolder_Button.Enabled = true;
            }
        }

        private void button_Update_Click(object sender, EventArgs e)
        {
            Update up = new Update("", "", 0, false);
            up.ShowDialog();
        }

       private void button_Addprofile_Click(object sender, EventArgs e)
        {
            if (textBox_ProfileName.Text == "")
            {
                MessageBox.Show("Profile needs a name");
            }
            else
            {
                Profile p = new Profile();

                p.ProProfile = textBox_ProfileName.Text;
                p.ProAuthOn = Auth_checkBox.Checked == true ? "yes" : "no";
                p.ProAuthUser = Premium_user_textBox.Text;
                p.ProAuthPass = Premium_pass_textBox.Text;
                p.ProUsername = textBox_Username.Text;
                p.ProMcVersion = comboBox_MCVersion.Text;
                p.ProScreenconfig = screenmode;
                p.ProServerconfig = autoconnect;

                p.ProCustomJavaXmx = JavaCustomXmX;
                p.ProCustomJava = JavaCustomArgs;

                p.ProAfterLaunch = SettingsAfterLaunch;

                //After launch settings..
                if (radioButton_stayopen.Checked == true)
                {
                    p.ProAfterLaunch = "stayopen";
                }
                else if (radioButton_Minimize.Checked == true)
                {
                    p.ProAfterLaunch = "minimize";
                }
                else if (radioButton_MinimizeToTray.Checked == true)
                {
                    p.ProAfterLaunch = "minimize_to_tray";
                }
                else if (radioButton_closelauncher.Checked == true)
                {
                    p.ProAfterLaunch = "close";
                }
                else
                {
                    p.ProAfterLaunch = "";
                }


                //possibly delete--------------------
                if (radioButton_NoAuto.Checked == true)
                {
                    p.ProServerconfig = "";
                }
                else if (radioButton_Kraftzone.Checked == true)
                {
                    p.ProServerconfig = "mc.kraftzone.net";
                }
                else if (radioButton_CustomServer.Checked == true)
                {
                    if (textBox_CustomServer.Text == "")
                    {
                        p.ProServerconfig = "";
                    }
                    else
                    {
                        p.ProServerconfig = textBox_CustomServer.Text;
                    }
                }

                //--------------------

                if (radioButton_JavaDefault.Checked == true)
                {
                    p.ProJavaVersion = "javaw";
                }
                else if (radioButton_JavaPortable.Checked == true)
                {
                    if (comboBox_JavaVersion.Text == "Java 7 (64bit)")
                    {
                        p.ProJavaVersion = "Java 7 (64bit)";
                    }
                    if (comboBox_JavaVersion.Text == "Java 7 (32bit)")
                    {
                        p.ProJavaVersion = "Java 7 (32bit)";
                    }
                    if (comboBox_JavaVersion.Text == "Java 8 (64bit)")
                    {
                        p.ProJavaVersion = "Java 8 (64bit)";
                    }
                    if (comboBox_JavaVersion.Text == "Java 8 (32bit)")
                    {
                        p.ProJavaVersion = "Java 8 (32bit)";
                    }
                }

                _profiles.Add(p);
                listView_Profiles.Items.Add(p.ProProfile);
                comboBox_Profile.Items.Add(p.ProProfile);
                comboBox_Profile.Text = p.ProProfile;

                //   comboBox_Profile.Items.Add(listView_Profiles.Items(0).Text);
                // bool focused = listView_Profiles.FindItemWithText(p.ProProfile).Index;
                //  listView_Profiles.Items[0].Selected = true;
            }
        }

        private class Profile
        {
            public string ProProfile { get; set; }
            public string ProUsername { get; set; }
            public string ProAuthOn { get; set; }
            public string ProAuthUser { get; set; }
            public string ProAuthPass { get; set; }
            public string ProMcVersion { get; set; }
            public string ProJavaVersion { get; set; }
            public string ProScreenconfig { get; set; }
            public string ProServerconfig { get; set; }
            public string ProAfterLaunch { get; set; }
            public string ProCustomJavaXmx { get; set; }
            public string ProCustomJava { get; set; }
            public string ProSelected { get; set; }
        }


        private void listView_Profiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView_Profiles.SelectedItems.Count == 0) return;

            _profiles[listView_Profiles.SelectedItems[0].Index].ProSelected = "";



            button_Remove.Enabled = true;

            comboBox_Profile.Text = _profiles[listView_Profiles.SelectedItems[0].Index].ProProfile;
            textBox_ProfileName.Text = _profiles[listView_Profiles.SelectedItems[0].Index].ProProfile;
            textBox_Username.Text = _profiles[listView_Profiles.SelectedItems[0].Index].ProUsername;
            comboBox_MCVersion.Text = _profiles[listView_Profiles.SelectedItems[0].Index].ProMcVersion;

            AuthCheckbox = _profiles[listView_Profiles.SelectedItems[0].Index].ProAuthOn;
            Premium_user_textBox.Text = _profiles[listView_Profiles.SelectedItems[0].Index].ProAuthUser;
            Premium_pass_textBox.Text = _profiles[listView_Profiles.SelectedItems[0].Index].ProAuthPass;

            JavaCustomXmX = _profiles[listView_Profiles.SelectedItems[0].Index].ProCustomJavaXmx;
            JavaCustomArgs = _profiles[listView_Profiles.SelectedItems[0].Index].ProCustomJava;
            SettingsAfterLaunch = _profiles[listView_Profiles.SelectedItems[0].Index].ProAfterLaunch;

            //SelectedProfile = _profiles[listView_Profiles.SelectedItems[0].Index].ProSelected;



            if (JavaCustomXmX == "")
            {
                checkBoxEnableXMX.Checked = false;

            }
            else
            {
                checkBoxEnableXMX.Checked = true;
                Xmx = "-Xmx" + JavaCustomXmX + "m ";

                numericUpDownXMX.Value = Convert.ToDecimal(JavaCustomXmX);

                richTextBox_FinalJavaArgs.Text = JavaVersionArgs + Xmx + JavaCustomArgs;
                JavaFinalArguements = JavaVersionArgs + Xmx + JavaCustomArgs;

            }
            if (JavaCustomArgs == "")
            {

                checkBoxCustomJAVA.Checked = false;
                button_SaveSettings.Enabled = false;
                JVMCustom();
            }
            else
            {
                checkBoxCustomJAVA.Checked = true;
                textBox_JavacustomArgs.Text = JavaCustomArgs;
                button_SaveSettings.Enabled = false;
                JVMCustom();

            }

            //After launch settings..
            if (SettingsAfterLaunch == "stayopen")
            {
                radioButton_stayopen.Checked = true;
            }
            else if (SettingsAfterLaunch == "minimize")
            {
                radioButton_Minimize.Checked = true;
            }
            else if (SettingsAfterLaunch == "minimize_to_tray")
            {
                radioButton_MinimizeToTray.Checked = true;
            }
            else if (SettingsAfterLaunch == "close")
            {
                radioButton_closelauncher.Checked = true;

            }
            else
            {
                SettingsAfterLaunch = "";
            }


            if (AuthCheckbox == "no")
            {
                Auth_checkBox.Checked = false;
            }
            else if (AuthCheckbox == "yes")
            {
                Auth_checkBox.Checked = true;
            }
            else
            {
                Auth_checkBox.Checked = false;
            }
            screenmode = _profiles[listView_Profiles.SelectedItems[0].Index].ProScreenconfig;
            autoconnect = _profiles[listView_Profiles.SelectedItems[0].Index].ProServerconfig;

            if (string.IsNullOrEmpty(autoconnect))
            {
                radioButton_NoAuto.Checked = true;
                textBox_CustomServer.Text = "";
            }
            else if (autoconnect == "mc.kraftzone.net")
            {
                radioButton_Kraftzone.Checked = true;

                textBox_CustomServer.Text = "";
            }
            else
            {
                radioButton_CustomServer.Checked = true;
                textBox_CustomServer.Text = _profiles[listView_Profiles.SelectedItems[0].Index].ProServerconfig;
            }
            //--------------------------

            //earlier version expect --width=1280   later version the '=' is not needed.
            switch (screenmode)
            {
                case "":
                    radioButton_ScreenDefault.Checked = true;
                    break;
                case " --width 1280 --height 720":

                    radioButton_Windowed720p.Checked = true;
                    break;
                case " --fullscreen":
                    radioButton_Fullscreen.Checked = true;
                    break;
            }

            //--------------------------
            if (_profiles[listView_Profiles.SelectedItems[0].Index].ProJavaVersion == "javaw")
            {
                radioButton_JavaDefault.Checked = true;
            }
            else if (_profiles[listView_Profiles.SelectedItems[0].Index].ProJavaVersion == "Java 7 (32bit)")
            {
                comboBox_JavaVersion.Text = "Java 7 (32bit)";
                radioButton_JavaPortable.Checked = true;
            }
            else if (_profiles[listView_Profiles.SelectedItems[0].Index].ProJavaVersion == "Java 7 (64bit)")
            {
                comboBox_JavaVersion.Text = "Java 7 (64bit)";
                radioButton_JavaPortable.Checked = true;
            }
            else if (_profiles[listView_Profiles.SelectedItems[0].Index].ProJavaVersion == "Java 8 (32bit)")
            {
                comboBox_JavaVersion.Text = "Java 8 (32bit)";
                radioButton_JavaPortable.Checked = true;
            }
            else if (_profiles[listView_Profiles.SelectedItems[0].Index].ProJavaVersion == "Java 8 (64bit)")
            {
                comboBox_JavaVersion.Text = "Java 8 (64bit)";
                radioButton_JavaPortable.Checked = true;
            }
            //--------------------------
        }
        private void Remove()
        {
            try
            {
                _profiles.RemoveAt(listView_Profiles.SelectedItems[0].Index);
                listView_Profiles.Items.Remove(listView_Profiles.SelectedItems[0]);
                comboBox_Profile.Items.Remove(comboBox_Profile.SelectedItem);
                comboBox_Profile.Text = "";
                // comboBox_Profile.Items.Remove(listView_Profiles.SelectedItems[0].Text);
                comboBox_Profile.SelectedIndex = 0;
            }
            catch (Exception)
            {
                //MessageBox.Show("Nothing to remove" +e.Message);
            }
        }

        private void button_Remove_Click(object sender, EventArgs e)
        {
            Remove();
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(exePath + "\\Minecraft Launcher.xml");
            XmlNode xNode = xDoc.SelectSingleNode("Profiles");
            xNode.RemoveAll();

            //Fixes tray icon being left behind after app closed... Microsuck won't fix the real issue
            notifyIcon_tray.Visible = false;

            if (listView_Profiles.Items.Count != 0)
            {
                _profiles[listView_Profiles.SelectedItems[0].Index].ProSelected = comboBox_Profile.Text;
            }

            // _profiles[listView_Profiles.SelectedItems[0].Index].ProSelected = comboBox_Profile.Text;

            foreach (Profile p in _profiles)
            {
                XmlNode xProfile = xDoc.CreateElement("Profile");
                XmlNode xProProfile = xDoc.CreateElement("ProProfile");
                XmlNode xProUsername = xDoc.CreateElement("ProUsername");
                XmlNode xProMcVersion = xDoc.CreateElement("ProMcVersion");
                XmlNode xProJavaVersion = xDoc.CreateElement("ProJavaVersion");
                XmlNode xProScreenconfig = xDoc.CreateElement("ProScreenconfig");
                XmlNode xProServerconfig = xDoc.CreateElement("ProServerconfig");
                XmlNode xProAuthOn = xDoc.CreateElement("ProAuthOn");
                XmlNode xProAuthUser = xDoc.CreateElement("ProAuthUser");
                XmlNode xProAuthPass = xDoc.CreateElement("ProAuthPass");
                XmlNode xProAfterLaunch = xDoc.CreateElement("ProAfterLaunch");
                XmlNode xProCustomJavaXmx = xDoc.CreateElement("ProCustomJavaXmx");
                XmlNode xProCustomJava = xDoc.CreateElement("ProCustomJava");
                XmlNode xProSelected = xDoc.CreateElement("ProSelected");

                xProProfile.InnerText = p.ProProfile;
                xProUsername.InnerText = p.ProUsername;
                xProMcVersion.InnerText = p.ProMcVersion;
                xProJavaVersion.InnerText = p.ProJavaVersion;
                xProScreenconfig.InnerText = p.ProScreenconfig;
                xProServerconfig.InnerText = p.ProServerconfig;
                xProAuthOn.InnerText = p.ProAuthOn;
                xProAuthUser.InnerText = p.ProAuthUser;
                xProAuthPass.InnerText = p.ProAuthPass;
                xProAfterLaunch.InnerText = p.ProAfterLaunch;
                xProCustomJavaXmx.InnerText = p.ProCustomJavaXmx;
                xProCustomJava.InnerText = p.ProCustomJava;
                xProSelected.InnerText = p.ProSelected;



                xProfile.AppendChild(xProProfile);
                xProfile.AppendChild(xProUsername);
                xProfile.AppendChild(xProMcVersion);
                xProfile.AppendChild(xProJavaVersion);
                xProfile.AppendChild(xProScreenconfig);
                xProfile.AppendChild(xProServerconfig);
                xProfile.AppendChild(xProAuthOn);
                xProfile.AppendChild(xProAuthUser);
                xProfile.AppendChild(xProAuthPass);
                xProfile.AppendChild(xProAfterLaunch);
                xProfile.AppendChild(xProCustomJavaXmx);
                xProfile.AppendChild(xProCustomJava);
                xProfile.AppendChild(xProSelected);

                xDoc.DocumentElement.AppendChild(xProfile);
            }
            try
            {
                xDoc.Save(exePath + "\\Minecraft Launcher.xml");
            }
            catch (Exception exx)
            {
                MessageBox.Show(exx.Message);
            }

            if (LauncherUpdatedFile != null) {
                UpdateApplication(exePath + LauncherUpdatedFile, exePath + "\\" + LauncherFileName, exePath + "\\" + LauncherFileName, "");
            }

        }

        private void comboBox_Profile_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView_Profiles.Items.Count > 0)
            {
                listView_Profiles.Items[comboBox_Profile.SelectedIndex].Selected = true;
                listView_Profiles.Select();

            }
            comboBox_Profile.Select();
            button_Save.Enabled = false;
            button_SaveSettings.Enabled = false;
        }

        private void textBox_ProfileName_TextChanged(object sender, EventArgs e)
        {
            labelcheck();
            button_Addprofile.Enabled = true;

            bool contains = false;

            foreach (string item in comboBox_Profile.Items)
            {
                contains = string.Equals(item, textBox_ProfileName.Text);

                if (contains) break;
            }

            if (contains)
            {
                button_Addprofile.Enabled = false;
            }

            if (textBox_ProfileName.Text == "")
            {
                button_Addprofile.Enabled = false;
            }
            if (textBox_ProfileName.Text == comboBox_Profile.Text)
            {
                button_Addprofile.Enabled = false;
            }
        }

        private void Auth_checkBox_CheckedChanged(object sender, EventArgs e)
        {
            Premium_groupBox.Enabled = Auth_checkBox.Checked == true;

            if (Auth_checkBox.Checked == false)
            {
                textcheck();
            }
            else
            {
                textBox_Username.BackColor = Color.LightGray;
            }
            labelcheck();
        }

        private static bool isOn = true;

        private void ShowPass_button_Click(object sender, EventArgs e)
        {
            if (isOn)
            {
                Premium_pass_textBox.PasswordChar = char.Parse("\0");
                ShowPass_button.Text = "Hide";
            }
            else
            {
                Premium_pass_textBox.PasswordChar = char.Parse("*");
                ShowPass_button.Text = "Show";
            }
            isOn = !isOn;
        }

        private void Premium_user_textBox_TextChanged(object sender, EventArgs e)
        {
            button_Save.Enabled = true;
        }

        private void Premium_pass_textBox_TextChanged(object sender, EventArgs e)
        {
            button_Save.Enabled = true;
        }

        private bool updatecheck = false;

        private void loadmodpack()
        {
            try {
                XmlDataDocument ModPacks = new XmlDataDocument();
                ModPacks.DataSet.ReadXml(Application.StartupPath + "\\ModPacks.xml");
                //dataGridView_modpacks.DataSource = richTextBox_modpacks.DataSet;
                dataGridView_modpacks.DataMember = "Versions";
            } catch (Exception) {

            }
            Modpacks modpackloader = new Modpacks
  {
      mcVersion = "Forge",
      Description = "yeh",
      url = "http://kraftzone.net/mclauncher/files.zip",
      CreatedDate = DateTime.UtcNow

  };
            string json = JsonConvert.SerializeObject(modpackloader, Newtonsoft.Json.Formatting.Indented);
            StreamWriter sx = new StreamWriter(Application.StartupPath + "\\ModPacks.json");
            sx.Write(json);
            sx.Dispose();

            // {
            //   "Email": "james@example.com",
            //   "Active": true,
            //   "CreatedDate": "2013-01-20T00:00:00Z",
            //   "Roles": [
            //     "User",
            //     "Admin"
            //   ]
            // }
            Console.WriteLine(json);

        }
        private void tabControl_Main_Selected(object sender, TabControlEventArgs e)
        {
            //MessageBox.Show(tabControl_Main.SelectedTab.Name);



            if (tabControl_Main.SelectedTab.Name == "tabPage_Settings")
            {
                textBox_mem.Text = getAvailableRAM();
                JVMCustom();
            }


            if (tabControl_Main.SelectedTab.Name == "tabPage_Main")
            {
                JavaPortableCheck();
                string myPath = exePath + "\\" + comboBox_MCVersion.Text;
            }

            if (tabControl_Main.SelectedTab.Name == "tabPage_Updates") {
                if (File.Exists(jre32filename_java7)) {
                    radioButton_Java7_32.Enabled = true;
                }
                if (File.Exists(jre64filename_java7)) {
                    radioButton_Java7_64.Enabled = true;
                }
                if (File.Exists(jre32filename_java8)) {
                    radioButton_Java8_32.Enabled = true;
                }
                if (File.Exists(jre64filename_java8)) {
                    radioButton_Java8_64.Enabled = true;
                }
            }

            if (tabControl_Main.SelectedTab.Name == "tabPage_ModPacks") {
                //loadmodpack();
                RefreshModPackList();
                //VersionsGridView.DataSource = McVersions.CreateSource();
            }


            if (tabControl_Main.SelectedTab.Name == "tabPage_About") {


                WatchLog = false;
                if (WatchLog && File.Exists(exePath + "\\logs\\fml-client-latest.log")) {
                    //WatchLog = false;
                    //    m_Watcher.EnableRaisingEvents = false;
                    //  m_Watcher.Dispose();

                    // WatchLog = true;

                    m_Watcher = new System.IO.FileSystemWatcher();

                    // m_Watcher.Path = Path.GetDirectoryName(filePath1);
                    // m_Watcher.Filter = Path.GetFileName(filePath1);
                    m_Watcher.Path = exePath + "\\logs\\";
                    m_Watcher.Filter = "fml-client-latest.log";

                    m_Watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                                         | NotifyFilters.FileName | NotifyFilters.DirectoryName;
                    m_Watcher.Changed += new FileSystemEventHandler(OnLogChanged);
                    //m_Watcher.Created += new FileSystemEventHandler(OnLogChanged);
                    //m_Watcher.Deleted += new FileSystemEventHandler(OnLogChanged);
                    //m_Watcher.Renamed += new RenamedEventHandler(OnLogChanged);
                    m_Watcher.EnableRaisingEvents = true;

                }

            }



            //{
            //if (tabControl_Main.SelectedTab.Name == "tabPage_Updates" && !updatecheck)
            //{
            //    try
            //    {
            //        WebClient wc = new WebClient();
            //        Update_richTextBox.Text = wc.DownloadString("http://kraftzone.tk/KzLauncher/changelog.txt");
            //        updatecheck = true;
            //    }
            //    catch
            //    {
            //        Update_richTextBox.Text = "Could not connect";
            //        updatecheck = true;
            //    }
            //}
        }
        private void OnLogChanged(object sender, FileSystemEventArgs e)
        {


            // string text = System.IO.File.ReadAllText(exePath + "\\logs\\fml-client-latest.log");


            if (File.Exists(exePath + "\\logs\\fml-client-latest.log")) {


                Invoke(new UpdateDelegate(
                      delegate
                      {
                          using (FileStream fileStream = new FileStream(
              exePath + "\\logs\\fml-client-latest.log", FileMode.Open,
              FileAccess.Read, FileShare.ReadWrite)) {
                              using (var sr = new StreamReader(fileStream)) {
                                  Log_richTextBox.Text = sr.ReadToEnd();
                              }

                          }

                          // Log_richTextBox.Text = text;
                          if (checkBoxAutoScroll.Checked) {
                              Log_richTextBox.SelectionStart = Log_richTextBox.Text.Length;
                              Log_richTextBox.ScrollToCaret();
                          }

                      })
                  );
            }
        }


        public void RefreshModPackList()
        {
            ModpacksGridView.DataSource = ModPacks.CreateSource();
            ModpacksGridView.EndEdit();
            ModpacksGridView.Refresh();
            
            //Application.DoEvents();
            //foreach (DataGridViewRow row in ModpacksGridView.Rows)
            //{

            //    foreach (DataGridViewColumn column in ModpacksGridView.Columns)
            //    {
            //        if (row.Cells[0].Value.ToString() == "✓")
            //        {
            //            ModpacksGridView.Rows[row.Index].Cells[column.Index].Style.ForeColor = Color.LightGreen;
            //            ModpacksGridView.Rows[row.Index].Cells[column.Index].Style.BackColor = Color.DarkGreen;
                        
            //            //ModpacksGridView[column.Index, row.Index].Style.ForeColor = Color.GreenYellow;
            //            break; //Otherwise it will continune painting all the other cells on the same row.
            //            // ModpacksGridView.Columns[0].DefaultCellStyle.ForeColor = Color.GreenYellow;
            //            //ModpacksGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.GreenYellow;
            //        }
            //        else
            //        {
            //            ModpacksGridView.Rows[row.Index].Cells[column.Index].Style.ForeColor = Color.Red;
            //            ModpacksGridView.Rows[row.Index].Cells[column.Index].Style.BackColor = Color.DarkRed;
            //            break;
            //            //ModpacksGridView.Columns[0].DefaultCellStyle.ForeColor = Color.Red;
            //        }
            //    }

            //}

            ModpacksGridView.Columns["Installed"].Width = 52;
            ModpacksGridView.Columns["Installed"].MinimumWidth = 52;
            ModpacksGridView.Columns["Name"].Width = 100; 
            ModpacksGridView.Columns["Name"].MinimumWidth = 100;
            ModpacksGridView.Columns["BaseType"].Width = 80;
            ModpacksGridView.Columns["BaseType"].MinimumWidth = 80;
            ModpacksGridView.Columns["Description"].Width = 108;//Description
            ModpacksGridView.Columns["Description"].Visible = false; 
            ModpacksGridView.Columns["Modlist"].Width = 108;//Modlist
            ModpacksGridView.Columns["Modlist"].Visible = false; //Modlist
            ModpacksGridView.Columns["Type"].Width = 108;//Type
            ModpacksGridView.Columns["Type"].Visible = false; //Type
            ModpacksGridView.Columns["Size"].Width = 50;//Size
            ModpacksGridView.Columns["Size"].MinimumWidth = 50;//Size
            ModpacksGridView.Columns["Url"].Width = 50;//Url
            ModpacksGridView.Columns["Url"].Visible = false; //Url
            ModpacksGridView.Columns["ReleaseTime"].Width = 115;//ReleaseTime
           
        }
        private void ModpacksGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (ModpacksGridView.SelectedCells.Count > 0)
            {
                int selectedrowindex = ModpacksGridView.SelectedCells[0].RowIndex;

                DataGridViewRow selectedRow = ModpacksGridView.Rows[selectedrowindex];

                //Fix row selection highlight coloring.. looks pretty
                if (selectedRow.Cells[0].Style.ForeColor == Color.LightGreen) {
                    selectedRow.Cells[0].Style.SelectionForeColor = Color.LightGreen;
                    selectedRow.Cells[0].Style.SelectionBackColor = Color.DarkGreen;
                } else {
                    selectedRow.Cells[0].Style.SelectionForeColor = Color.Red;
                    selectedRow.Cells[0].Style.SelectionBackColor = Color.Maroon;
                }

                string filenamefromurl = Path.GetFileName(selectedRow.Cells["url"].Value.ToString());
           

                foreach (string java in JavaPortableFound)
                {
                    if (java.Equals(filenamefromurl))
                    {
                        if (File.Exists(filenamefromurl))
                        {
                            button_ModPackExtract.Enabled = true;
                        break;
                        }
                    }
                    else
                    {
                        button_ModPackExtract.Enabled = false;
                    }
                }

                //selectedRow.Cells[0].Style.SelectionBackColor = Color.Transparent;
                //ModpackName_TextBox.Text = selectedRow.Cells["Name"].Value.ToString();
                // ModpackName_TextBox.Text = row.Cells["BaseVersion"].Value.ToString();
                //textBox_ModDescription.Text = selectedRow.Cells["Description"].Value.ToString();
                //textBox_Mods.Text = selectedRow.Cells["Modlist"].Value.ToString();
                // ModpackName_TextBox.Text = row.Cells["Type"].Value.ToString();
                //ModpackName_TextBox.Text = row.Cells["ReleaseTime"].Value.ToString();
                //textBox_ModSize.Text = selectedRow.Cells["Size"].Value.ToString();

                var sb = new StringBuilder();
                sb.Append(@"{\rtf1\ansi");
                sb.Append(@"\b Name: \b0 ");
                sb.Append(selectedRow.Cells["Name"].Value.ToString());
                sb.Append(@" \line \line ");
                sb.Append(@"\b Description: \b0 ");
                sb.Append(selectedRow.Cells["Description"].Value.ToString());
                sb.Append(@" \line \line");
                sb.Append(@"\b Included Files: \b0 ");
                sb.Append(selectedRow.Cells["Modlist"].Value.ToString());
                sb.Append(@" \line \line ");
                sb.Append(@"}");
                richTextBox_modpacks.Rtf = sb.ToString();

                bool fileExists = File.Exists(filenamefromurl);

                string a = Convert.ToString(selectedRow.Cells["Installed"].Value);
                if (a == "✓")
                {
                    button_ModpackDownload.Text = "Uninstall";
                    uninstallversion = true;
                    if (fileExists)
                    {
                        button_ModPackExtract.Enabled = true;
                        button_ModPackExtract.Text = "Install Again?";
                    }
                    else
                    {
                        button_ModPackExtract.Enabled = false;
                        button_ModPackExtract.Text = ">>>";
                    }
                }
                else // a == " X"
                {
                    button_ModpackDownload.Text = "Download";
                    uninstallversion = false;
                    if (fileExists)
                    {
                        button_ModPackExtract.Enabled = true;
                        button_ModPackExtract.Text = "Install";
                        button_ModpackDownload.Text = "Download Again?";
                    }
                    else {
                        button_ModPackExtract.Enabled = false;
                        button_ModPackExtract.Text = ">>>";
                    }
                }

            }
        }

        private void button_updateMods_Click(object sender, EventArgs e)
        {
            DialogResult yesnoDialog = MessageBox.Show("Try download new version modpack list?", "Update modpacks.json?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (yesnoDialog == DialogResult.Yes)
            {
                Dictionary<int, string[]> LauncherDownload = new Dictionary<int, string[]>{
                           {0, new string[] { "http://kraftzone.net/KzLauncher/modpacks.json", @"\modpacks.json"}}
                            };

                Downloader MultifileDownload = new Downloader(LauncherDownload, exePath, "", "",2);
                MultifileDownload.ShowDialog();

            }
            RefreshModPackList();
        }

        private void button_ModpackDownload_Click(object sender, EventArgs e)
        {
            int selectedIndex = ModpacksGridView.SelectedRows[0].Index;

            string modpackName = ModpacksGridView.SelectedRows[0].Cells[1].Value.ToString(); //id name
            string modpackZipUrl = ModpacksGridView.SelectedRows[0].Cells[7].Value.ToString();
            
            string modpackZipFileName = Path.GetFileName(modpackZipUrl);

            bool BaseVersionExists = false;
            string version = ModpacksGridView.SelectedRows[0].Cells[2].Value.ToString(); //baseversion
            string BaseVersionDirectory = exePath + @"\versions\" + version;


           if (Directory.Exists(BaseVersionDirectory))
                {
                    BaseVersionExists = true;
                }
 
    
            if (uninstallversion)
            {
                if (version == "Java Portable")
                {
                    DialogResult yesnoDialog4 = MessageBox.Show("Uninstall: /" + modpackName, "Are you sure?", MessageBoxButtons.YesNo);
                    if (yesnoDialog4 == DialogResult.Yes)
                    {
                        try
                        {
                            Directory.Delete(modpackName, true);
                        }
                        catch (Exception erer) {
                            MessageBox.Show("Error 34342: " + erer);
                        }
                        JavaPortableCheck();
                    }
                
                }else{

                string VersionDirectory = exePath + @"\versions\" + modpackName;
                string GameVersionDirectory = exePath + @"\" + modpackName;

                DialogResult yesnoDialog = MessageBox.Show("Delete this version ?\n\nLocation: " + VersionDirectory, "Are you sure?", MessageBoxButtons.YesNo);
                if (yesnoDialog == DialogResult.Yes)
                {

                    Directory.Delete(VersionDirectory, true);
                    if (Directory.Exists(GameVersionDirectory))
                    {
                        DialogResult yesnoDialog2 = MessageBox.Show("Also remove the associated game directory? It may include texturespacks, mods, screenshots etc. \n\nLocation: " + GameVersionDirectory, "Are you sure?", MessageBoxButtons.YesNo);
                        if (yesnoDialog2 == DialogResult.Yes)
                        { // deletes all files, subfolders and the directory itself
                            Directory.Delete(GameVersionDirectory, true);
                        }

                      }
                    }
                }
               
            }
            else
            {
                if (BaseVersionExists == false && version != "Java Portable")
                {
                    string versionJar = "http://s3.amazonaws.com/Minecraft.Download/versions/" + version + "/" + version + ".jar";
                    string versionJson = "http://s3.amazonaws.com/Minecraft.Download/versions/" + version + "/" + version + ".json";
                    string versionJarPath = @"\versions\" + version + @"\" + Path.GetFileName(versionJar);
                    string versionJsonPath = @"\versions\" + version + @"\" + Path.GetFileName(versionJson);


                    Dictionary<int, string[]> ModpackDownload = new Dictionary<int, string[]>{
                 {0, new string[] { versionJson, versionJsonPath }},
                 {1, new string[] { versionJar, versionJarPath }},
                 {2, new string[] { modpackZipUrl, "\\"+modpackZipFileName }}
                  };

                    Downloader MultifileDownload = new Downloader(ModpackDownload, exePath, exePath + versionJsonPath, modpackZipFileName);
                    MultifileDownload.ShowDialog();

                }
                else //BaseVersionExists true
                {

                    // http://s3.amazonaws.com/Minecraft.Download/versions/1.7.9/1.7.9.json

                    Dictionary<int, string[]> ModpackDownload = new Dictionary<int, string[]>{
                 {0, new string[] { modpackZipUrl, "\\"+modpackZipFileName }}
                  };

                    //   Dictionary<int, string[]> demoDownloadDictonary = new Dictionary<int, string[]>{
                    //{0, new string[] { versionJar, path }},
                    //{1, new string[] { versionJson, path }},
                    //{2, new string[] { "http://kraftzone.net/downloads/MinecraftLauncher_v1.10.zip", @"Folder\MinecraftLauncher_v1.10.zip" }},
                    //{3, new string[] { "http://kraftzone.net/downloads/LiteLoader1.7.2.jar", @"Folder\LiteLoader1.7.2.jar" }}
                    // };

                    Downloader MultifileDownload = new Downloader(ModpackDownload, exePath, "", modpackZipFileName);
                    MultifileDownload.ShowDialog();
                }
            }

            if (version == "Java Portable")
            {
                JavaPortableCheck();
            }
            else
            {
                if (!button_ModpackDownload.Text.Equals("Download Again?"))
                { 
                comboBox_MCVersion.Items.Add(modpackName);
                comboBox_MCVersion.Enabled = true;


                if (comboBox_MCVersion.Items.Count == 0) {
                    comboBox_MCVersion.Text = modpackName;
                    }
                }
                //refreshMCVersion();
            }

            RefreshModPackList();

            // VersionsGridView.SelectedRows.Clear();
            foreach (DataGridViewRow row in ModpacksGridView.Rows)
            {
                if (row.Index == selectedIndex)
                    row.Selected = true;
                ModpacksGridView.FirstDisplayedScrollingRowIndex = selectedIndex;
                ModpacksGridView.Update();
            }


        }

        private void ModpacksGridView_MouseEnter(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in ModpacksGridView.Rows)
            //{
            //  //  foreach (DataGridViewColumn column in ModpacksGridView.Columns)
            //   // {
            //        if (row.Cells[0].Value.ToString() == "✓")
            //        {
            //            ModpacksGridView.Rows[row.Index].Cells[0].Style.ForeColor = Color.LightGreen;
            //            ModpacksGridView.Rows[row.Index].Cells[0].Style.BackColor = Color.Green;
            //            //ModpacksGridView[column.Index, row.Index].Style.ForeColor = Color.GreenYellow;
            //           // break; //Otherwise it will continune painting all the other cells on the same row.
            //        }
            //        else
            //        {
            //            ModpacksGridView.Rows[row.Index].Cells[0].Style.ForeColor = Color.Red;
            //            ModpacksGridView.Rows[row.Index].Cells[0].Style.BackColor = Color.DarkRed;
            //            //break;
            //        }
            //    //}

            //}
        }
        private void button_ModPackExtract_Click(object sender, EventArgs e)
        {
            int selectedIndex = ModpacksGridView.SelectedRows[0].Index;

            string modpackName = ModpacksGridView.SelectedRows[0].Cells[1].Value.ToString(); //id name
            string modpackZipUrl = ModpacksGridView.SelectedRows[0].Cells[7].Value.ToString();

            string modpackZipFileName = Path.GetFileName(modpackZipUrl);

            string version = ModpacksGridView.SelectedRows[0].Cells[2].Value.ToString(); //baseversion
        

                    Dictionary<int, string[]> ModpackDownload = new Dictionary<int, string[]>{
                 {0, new string[] { modpackZipUrl, "\\"+modpackZipFileName }}
                  };

                    //   Dictionary<int, string[]> demoDownloadDictonary = new Dictionary<int, string[]>{
                    //{0, new string[] { versionJar, path }},
                    //{1, new string[] { versionJson, path }},
                    //{2, new string[] { "http://kraftzone.net/downloads/MinecraftLauncher_v1.10.zip", @"Folder\MinecraftLauncher_v1.10.zip" }},
                    //{3, new string[] { "http://kraftzone.net/downloads/LiteLoader1.7.2.jar", @"Folder\LiteLoader1.7.2.jar" }}
                    // };

                    Downloader MultifileDownload = new Downloader(ModpackDownload, exePath, "", modpackZipFileName,0,false,true);
                    MultifileDownload.ShowDialog();
                
            

            if (version == "Java Portable")
            {
                JavaPortableCheck();
            }
            else
            {
                //FindStringExact returns -1 if nothing is found, otherwise it returns the index of found string.
                //avoids adding duplicates
                if (comboBox_MCVersion.FindStringExact(modpackName) < 0)
                {
                    //todo: make new version check function
                    comboBox_MCVersion.Items.Add(modpackName);
                    comboBox_MCVersion.Enabled = true;

                }
                //not needed for modpack
               // refreshMCVersion();
            }

            RefreshModPackList();

            // VersionsGridView.SelectedRows.Clear();
            foreach (DataGridViewRow row in ModpacksGridView.Rows)
            {
                if (row.Index == selectedIndex)
                    row.Selected = true;
                ModpacksGridView.FirstDisplayedScrollingRowIndex = selectedIndex;
                ModpacksGridView.Update();
            }
        }
        private void ModpacksGridView_Sorted(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in ModpacksGridView.Rows)
            //{
            //    foreach (DataGridViewColumn column in ModpacksGridView.Columns)
            //    {
            //        if (row.Cells[0].Value.ToString() == "✓")
            //        {
            //            ModpacksGridView.Rows[row.Index].Cells[column.Index].Style.ForeColor = Color.LightGreen;
            //            ModpacksGridView.Rows[row.Index].Cells[column.Index].Style.BackColor = Color.Green;
            //            //ModpacksGridView[column.Index, row.Index].Style.ForeColor = Color.GreenYellow;
            //            break; //Otherwise it will continune painting all the other cells on the same row.
            //        }
            //        else
            //        {
            //            ModpacksGridView.Rows[row.Index].Cells[column.Index].Style.ForeColor = Color.Red;
            //            ModpacksGridView.Rows[row.Index].Cells[column.Index].Style.BackColor = Color.DarkRed;
            //            break;
            //        }
            //    }

            //}
        }


        private void DownloadUpdate_button_Click(object sender, EventArgs e)
        {
            if (radioButton_Java7_32.Checked == true)
            {
                Update up5 = new Update(kzLauncherURL + jre32filename_java7,
                                        "Get portable java 1.7_u25 (32)?", jre32bytesize_java7, false);
                up5.ShowDialog();
            }
            if (radioButton_Java7_64.Checked == true)
            {
                Update up6 = new Update(kzLauncherURL + jre64filename_java7,
                                        "Get portable java 1.7_u25 (64)?", jre64bytesize_java7, false);
                up6.ShowDialog();
            }

            if (radioButton_Minecraft172.Checked == true)
            {
                Update up7 = new Update(kzLauncherURL + mc172filename,
                                        "Get 1.7.2 files?", mc172bytesize, false);
                up7.ShowDialog();
            }
            if (radioButton_Java8_32.Checked == true)
            {
                Update up9 = new Update(kzLauncherURL + jre32filename_java8,
                                        "Get portable java 1.8_u5 (32)?", jre32bytesize_java8, false);
                up9.ShowDialog();
            }
            if (radioButton_Java8_64.Checked == true)
            {
                Update up10 = new Update(kzLauncherURL + jre64filename_java8,
                                        "Get portable java 1.8_u5 (64)?", jre64bytesize_java8, false);
                up10.ShowDialog();
            }
        }
        private void radioButton_Java32_CheckedChanged(object sender, EventArgs e)
        {
            if (File.Exists(jre32filename_java7))
            {
                // MessageBox.Show("file exists true");
                FileInfo f = new FileInfo(jre32filename_java7);
                long s1 = f.Length;

                if (s1 == jre32bytesize_java7)
                {
                    //   MessageBox.Show("match length");
                    DownloadExtract_button.Enabled = true;
                    DownloadUpdate_button.Enabled = false;
                }
                else
                {
                    DownloadExtract_button.Enabled = false;
                    DownloadUpdate_button.Enabled = true;
                }
            }
            else
            {
                DownloadExtract_button.Enabled = false;
                DownloadUpdate_button.Enabled = true;
            }
        }
        private void radioButton_Java64_CheckedChanged(object sender, EventArgs e)
        {
            if (File.Exists(jre64filename_java7))
            {
                //  MessageBox.Show("file exists true");
                FileInfo f = new FileInfo(jre64filename_java7);
                long s1 = f.Length;

                if (s1 == jre64bytesize_java7)
                {
                    //    MessageBox.Show("match length");
                    DownloadExtract_button.Enabled = true;
                    DownloadUpdate_button.Enabled = false;
                }
                else
                {
                    DownloadExtract_button.Enabled = false;
                    DownloadUpdate_button.Enabled = true;
                }
            }
            else
            {
                DownloadExtract_button.Enabled = false;
                DownloadUpdate_button.Enabled = true;
            }
        }

        private void button_checkupdate_Click(object sender, EventArgs e)
        {
            string tempUpdate = Update_richTextBox.Text;
            try
            {
                WebClient wc = new WebClient();
                Update_richTextBox.Text = wc.DownloadString(kzLauncherURL + "changelog.txt");
                updatecheck = true;
                var json = wc.DownloadString(kzLauncherURL + "LauncherUpdate.json");
                dynamic launcherUpdate = JsonConvert.DeserializeObject(json);
                foreach (var typeStr in launcherUpdate.latest)
                {
                    if (LauncherVersion < (decimal)typeStr.version)
                    {
                        DialogResult yesnoDialog = MessageBox.Show("New Update, version: " + typeStr.version + "\nDescription: " + typeStr.Description, "Download launcher update?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (yesnoDialog == DialogResult.Yes)
                        {

                            string Downloadfilename = Path.GetFileName((string)typeStr.url);

                            Dictionary<int, string[]> LauncherDownload = new Dictionary<int, string[]>{
                           {0, new string[] { (string)typeStr.url, @"\" + Downloadfilename }},
                           {1, new string[] { "http://s3.amazonaws.com/Minecraft.Download/versions/versions.json", @"\versions.json"}}
                            };

                            Downloader MultifileDownload = new Downloader(LauncherDownload, exePath, "", Downloadfilename);
                            DialogResult result = MultifileDownload.ShowDialog();

                            if (result == DialogResult.OK)
                            {
                                DialogResult yesnoDialog2 = MessageBox.Show("Update and relaunch now?\n Clicking 'No' will update after you close the launcher yourself.", "Restart now?", MessageBoxButtons.YesNo);
                                if (yesnoDialog2 == DialogResult.Yes)
                                {
                                  //  MessageBox.Show("Application will close and update the launcher.", "Restart now?", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    UpdateApplication(exePath + @"\" + Path.GetFileName((string)typeStr.url), exePath + "\\" + LauncherFileName, exePath + "\\" + LauncherFileName, "");
                                    Application.Exit();
                                }
                                else if (yesnoDialog2 == DialogResult.No)
                                {
                                    LauncherUpdatedFile = @"\" + Path.GetFileName((string)typeStr.url);
                                    //Application exit will check LauncherUpdatedFile and run UpdateApplication
                                }
                            }
                            else if (result == DialogResult.Abort)
                            {
                                MessageBox.Show("The update download was cancelled.\nThis program has not been modified.", "Update Download Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("There was a problem downloading the update.\nPlease try again later.", "Update Download Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                        }
                    }
                    else
                    {
                        MessageBox.Show("You already have the latest update available", "No new updates", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    //Console.WriteLine(typeStr.version);
                    //Console.WriteLine(typeStr.url);
                    //Console.WriteLine(typeStr.releaseTime);
                    //Console.WriteLine(typeStr.Description);
                }

            }
            catch (Exception UpdateEx)
            {
                if (checkBox_debug.Enabled)
                {
                    Update_richTextBox.Text = UpdateEx.ToString();
                    updatecheck = true;
                }
                else
                {
                    Update_richTextBox.Text = tempUpdate;
                }

                MessageBox.Show("Could not get update information, try again later or \nReport the problem at http:\\\\Kraftzone.net", "Error checking for updates", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        /// <summary>
        /// Hack to close program, delete original, move the new one to that location
        /// </summary>
        /// <param name="tempFilePath">The temporary file's path</param>
        /// <param name="currentPath">The path of the current application</param>
        /// <param name="newPath">The new path for the new file</param>
        /// <param name="launchArgs">The launch arguments</param>
        private void UpdateApplication(string tempFilePath, string currentPath, string newPath, string launchArgs)
        {
            string argument = "/C choice /C Y /N /D Y /T 4 & Del /F /Q \"{0}\" & choice /C Y /N /D Y /T 2 & Move /Y \"{1}\" \"{2}\" & Start \"\" /D \"{3}\" \"{4}\" {5}";

            ProcessStartInfo Info = new ProcessStartInfo();
            Info.Arguments = String.Format(argument, currentPath, tempFilePath, newPath, Path.GetDirectoryName(newPath), Path.GetFileName(newPath), launchArgs);
            Info.WindowStyle = ProcessWindowStyle.Hidden;
            Info.CreateNoWindow = true;
            Info.FileName = "cmd.exe";
            Process.Start(Info);
        }
        private void DownloadExtract_button_Click(object sender, EventArgs e)
        {
            if (radioButton_Java7_32.Checked == true)
            {
                Update up2 = new Update(kzLauncherURL + jre32filename_java7,
                                        "Extract java 1.7_u25 (32)", jre32bytesize_java7, true);
                up2.ShowDialog();
            }
            if (radioButton_Java7_64.Checked == true)
            {
                Update up3 = new Update(kzLauncherURL + jre64filename_java7,
                                        "Extract java 1.7_u25 (64)", jre64bytesize_java7, true);
                up3.ShowDialog();
            }

            if (radioButton_Minecraft172.Checked == true)
            {
                Update up4 = new Update(kzLauncherURL + mc172filename,
                                        "Extract files required to launch mc", mc172bytesize, true);
                up4.ShowDialog();
            }

            JavaPortableCheck();
        }

        private void button_openportablefolder_Click(object sender, EventArgs e)
        {
            string myPath = exePath;

            System.Diagnostics.Process prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = myPath;
            prc.Start();
        }



        private void button_checkcpumem_Click(object sender, EventArgs e)
        {
            // textBox_cpu.Text = getCurrentCpuUsage();
            textBox_mem.Text = getAvailableRAM();
            // numericUpDownXMX.Maximum = System.Convert.ToDecimal(ramCounter.NextValue());

          //  cpuCounter = new System.Diagnostics.PerformanceCounter();
          //  cpuCounter.CategoryName = "Processor";
          //  cpuCounter.CounterName = "% Processor Time";
          //  cpuCounter.InstanceName = "_Total";
            decimal xmxMax = 1024;
            try {
                System.Diagnostics.PerformanceCounter ramCounter;
                ramCounter = new System.Diagnostics.PerformanceCounter("Memory", "Available MBytes");
                xmxMax = Convert.ToDecimal(ramCounter.NextValue());
            } catch (Exception asdsad) {
                MessageBox.Show("Error 34342 memPerfcounter: " + asdsad);
                xmxMax = 1024;
            }

            


            if ((CurrentXMX + 64) < xmxMax)
            {
                CurrentXMX = numericUpDownXMX.Value + 64;
                numericUpDownXMX.Maximum = numericUpDownXMX.Maximum + 64;
            }
            else
            {
                CurrentXMX = numericUpDownXMX.Value;
                numericUpDownXMX.Maximum = numericUpDownXMX.Value;
            }

        }


        //public string getCurrentCpuUsage()
        //{
        //    return cpuCounter.NextValue() + "%";
        //}

        // Call this method every time you need to get the amount of the available RAM in Mb 
        public string getAvailableRAM()
        {
            try {
                System.Diagnostics.PerformanceCounter ramCounter;
                ramCounter = new System.Diagnostics.PerformanceCounter("Memory", "Available MBytes");
                return ramCounter.NextValue() + "Mb";
            } catch (Exception asd2) {
                MessageBox.Show("Error 31312 memPerfcounter: " + asd2);
                return 1024 + "Mb";
            }
            
        }
        private void numericUpDownXMX_ValueChanged(object sender, EventArgs e)
        {
            textBox_mem.Text = getAvailableRAM();
            //decimal xmxMax = numericUpDownXMX.Maximum;

            decimal xmxMax = 1024;
            try {
                System.Diagnostics.PerformanceCounter ramCounter;
                ramCounter = new System.Diagnostics.PerformanceCounter("Memory", "Available MBytes");
                xmxMax = Convert.ToDecimal(ramCounter.NextValue());
            } catch (Exception ee3) {
                MessageBox.Show("Error 34312 memPerfcounter: " + ee3);
                xmxMax = 1024;
            }



            button_SaveSettings.Enabled = true;
            ////numericUpDownXMX.Maximum = System.Convert.ToDecimal(textBox_mem.Text);
            // numericUpDownXMX.Maximum = System.Convert.ToDecimal(ramCounter.NextValue());

            //modulus
            //int maxx = System.Convert.ToInt32((ramCounter.NextValue()) % 128);
            //MessageBox.Show(numericUpDownXMX.Maximum +" "+ maxx.ToString());

            if ((CurrentXMX + 64) < xmxMax)
            {
                numericUpDownXMX.Value = numericUpDownXMX.Value;
                CurrentXMX = numericUpDownXMX.Value + 64;
                //numericUpDownXMX.Maximum = numericUpDownXMX.Maximum + 128;
            }
            else
            {
                CurrentXMX = numericUpDownXMX.Value;
                //numericUpDownXMX.Maximum = numericUpDownXMX.Value;
            }

            numericUpDownXMX.BackColor = Color.LimeGreen;

            if (numericUpDownXMX.Value < Convert.ToDecimal(JavaVersionXmX))
            {
                numericUpDownXMX.BackColor = Color.Yellow;
            }


            if (numericUpDownXMX.Value > xmxMax)
            {
                //textBox_mem.BackColor = Color.Yellow;
                numericUpDownXMX.BackColor = Color.Orange;
            }

            // textBox_mem.BackColor = System.Drawing.Color.FromKnownColor(KnownColor.Control);



            //Xmx = System.Convert.ToInt32(numericUpDownXMX.Value);
            Xmx = System.Convert.ToString(numericUpDownXMX.Value);
            JVMCustom();
        }

        private void checkBoxEnableXMX_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxXMX.Enabled = checkBoxEnableXMX.Checked;
            //Xmx = "1024";

            JVMCustom();


        }

        private void checkBoxCustomJAVA_CheckedChanged(object sender, EventArgs e)
        {
            groupBox_Customjvm.Enabled = checkBoxCustomJAVA.Checked;
            //JavaCustomArgs = textBox_JavacustomArgs.Text;
            JVMCustom();
            if (checkBoxCustomJAVA.Checked == false)
            {
                button_SaveSettings.Enabled = true;
                textBox_JavacustomArgs.Text = "";
                JVMCustom();
            }

        }

        private void tabPage_Settings_Click(object sender, EventArgs e)
        {

        }

        private void textBox_JavacustomArgs_TextChanged(object sender, EventArgs e)
        {
            JavaCustomArgs = textBox_JavacustomArgs.Text.Trim();
            button_SaveSettings.Enabled = true;
            JVMCustom();
            //todo add proxy socks stuff
            //-DsocksProxyHost=127.0.0.1 -DsocksProxyPort=8080 -Djava.net.preferIPv4Stack=true -Djava.net.preferIPv6Addresses=false
            //-Dhttp.useProxy=true -Dhttps.proxyHost=127.0.0.1 -Dhttps.proxyPort=8888 -Dhttp.proxyHost=127.0.0.1 -Dhttp.proxyPort=8888        
        }

        private void JVMCustom()
        {
            try { 
            System.Diagnostics.PerformanceCounter ramCounter;
            ramCounter = new System.Diagnostics.PerformanceCounter("Memory", "Available MBytes");
            decimal freemem = Convert.ToDecimal(ramCounter.NextValue());
                    if (freemem < 777)
                    {
                        JavaVersionXmX = "512";
                    }
                    if (freemem < 1024)
                    {
                        JavaVersionXmX = "768";
                    }
                    if (freemem > 1200)
                    {
                        JavaVersionXmX = "1024";
                    }
                    if (freemem > 1800)
                    {
                        JavaVersionXmX = "1500";
                    }
                }catch(Exception ere){
                    MessageBox.Show("Error 2425 memPerfcounter: " + ere);
                    JavaVersionXmX = "1024";
            }

            if (groupBoxXMX.Enabled == true && groupBox_Customjvm.Enabled == true)
            {
                JavaCustomArgs = textBox_JavacustomArgs.Text.Trim();
                JavaCustomXmX = Convert.ToString(numericUpDownXMX.Value);
                JavaVersionXmX = JavaCustomXmX;
                Xmx = "-Xmx" + JavaCustomXmX + "m ";

                //MessageBox.Show("1"+JavaCustomXmX);
                richTextBox_FinalJavaArgs.Text = Xmx + JavaCustomArgs + " " + JavaVersionArgs;
                JavaFinalArguements = Xmx + JavaCustomArgs + " " + JavaVersionArgs;
            }
            if (groupBoxXMX.Enabled == true && groupBox_Customjvm.Enabled == false)
            {

                JavaCustomXmX = Convert.ToString(numericUpDownXMX.Value);
                JavaVersionXmX = JavaCustomXmX;
                Xmx = "-Xmx" + JavaCustomXmX + "m ";
                richTextBox_FinalJavaArgs.Text = Xmx + JavaVersionArgs;
                JavaFinalArguements = Xmx + JavaVersionArgs;
                //MessageBox.Show("2" + JavaCustomXmX);
            }

            if (groupBoxXMX.Enabled == false && groupBox_Customjvm.Enabled == true)
            {
                JavaCustomArgs = textBox_JavacustomArgs.Text.Trim();
                JavaCustomXmX = JavaVersionXmX;
                Xmx = "-Xmx" + JavaVersionXmX + "m ";
                richTextBox_FinalJavaArgs.Text = Xmx + JavaCustomArgs+ " " + JavaVersionArgs;
                JavaFinalArguements = Xmx + JavaCustomArgs + " " +JavaVersionArgs;
                //MessageBox.Show("3" + JavaCustomXmX);

            }
            if (groupBoxXMX.Enabled == false && groupBox_Customjvm.Enabled == false)
            {

                JavaCustomXmX = JavaVersionXmX;
                Xmx = "-Xmx" + JavaVersionXmX + "m ";
                JavaCustomArgs = "";
                richTextBox_FinalJavaArgs.Text = Xmx + JavaVersionArgs;
                JavaFinalArguements = Xmx + JavaVersionArgs;
                //MessageBox.Show("4" + JavaCustomXmX);
            }

        }

        private void button_JavaOptions_Click(object sender, EventArgs e)
        {
            tabControl_Main.SelectTab(tabPage_Settings);
            tabControl_Settings.SelectTab(JavaOptions);
        }

        private void radioButton_stayopen_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_stayopen.Checked == true)
            {
                SettingsAfterLaunch = "stayopen";
            }
            button_SaveSettings.Enabled = true;

        }
        private void radioButton_Minimize_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Minimize.Checked == true)
            {
                SettingsAfterLaunch = "minimize";
            }
            button_SaveSettings.Enabled = true;

        }

        private void radioButton_MinimizeToTray_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_MinimizeToTray.Checked == true)
            {
                SettingsAfterLaunch = "minimize_to_tray";
            }
            button_SaveSettings.Enabled = true;
        }

        private void radioButton_closelauncher_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_closelauncher.Checked == true)
            {
                SettingsAfterLaunch = "close";
            }
            button_SaveSettings.Enabled = true;
        }

        private void button_SettingsSave_Click(object sender, EventArgs e)
        {
            SaveEverything();
        }

        private void notifyIcon_tray_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();//shows the program on taskbar
            this.WindowState = FormWindowState.Normal;//undoes the minimized state of the form
            notifyIcon_tray.Visible = false;//hides tray icon again
        }

        private void checkBox_debug_CheckedChanged(object sender, EventArgs e)
        {
            //checkBox_debug.ForeColor = checkBox_debug.Checked == true ? Color.Black : Color.DimGray;
            if (checkBox_debug.Checked) {
                debug_info.Visible = true;
                checkBox_showconsole.Enabled = false;
            }
            else { 
                debug_info.Visible = false;
                checkBox_showconsole.Enabled = true;
            }
        }

        private void radioButton_Minecraft172_CheckedChanged(object sender, EventArgs e)
        {
            if (File.Exists(mc172filename))
            {
                //  MessageBox.Show("file exists true");
                FileInfo f = new FileInfo(mc172filename);
                long s1 = f.Length;

                if (s1 == mc172bytesize)
                {
                    //    MessageBox.Show("match length");
                    DownloadExtract_button.Enabled = true;
                    DownloadUpdate_button.Enabled = false;
                }
                else
                {
                    DownloadExtract_button.Enabled = false;
                    DownloadUpdate_button.Enabled = true;
                }
            }
            else
            {
                DownloadExtract_button.Enabled = false;
                DownloadUpdate_button.Enabled = true;
            }
        }

        private void Main_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;

        }

        private void Main_DragDrop(object sender, DragEventArgs e)
        {
            if (tabControl_Main.SelectedTab.Name == "tabPage_Main")
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);

                foreach (string file in files)
                {

                    string temp = Path.GetExtension(file);
                    if (temp == ".jar")
                    {

                        // MessageBox.Show(file);

                        var launcher = new StreamWriter(File.Create(Path.GetFileName(file) + ".bat"));

                        string startup = startupcheck() + " ";
                        //startup = startup + " ";

                        launcher.Write(startup + "-Xmx512m -jar \"" + file + "\"");
                        launcher.Dispose();

                        Process Launchit = new Process();

                        try
                        {
                            Launchit.StartInfo.UseShellExecute = false;
                            // You can start any process, HelloWorld is a do-nothing example.
                            Launchit.StartInfo.FileName = exePath + "\\" + Path.GetFileName(file) + ".bat";
                            //MessageBox.Show(exePath);
                            Launchit.StartInfo.CreateNoWindow = true;
                            // Launchit.StartInfo.Arguments = mcpatcher;

                            Launchit.Start();
                            // This code assumes the process you are starting will terminate itself.  
                            // Given that is is started without a window so you cannot terminate it  
                            // on the desktop, it must terminate itself or you can do it programmatically 
                            // from this application using the Kill method.
                        }
                        catch (Exception exx)
                        {
                            MessageBox.Show(exx.Message);
                        }
                    }

                    // MessageBox.Show(file);
                }
            }
            else if (tabControl_Main.SelectedTab.Name == "tabPage_ModPacks")
            {

            }
        }

        private void listView_Profiles_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo info = listView_Profiles.HitTest(e.X, e.Y);
            ListViewItem item = info.Item;

            if (item != null)
            {
                //MessageBox.Show("The selected Item Name is: " + item.Text);
                button_Launch.PerformClick();
                // MessageBox.Show(item.Text);

            }
            else
            {
                // this.listView_Profiles.SelectedItems.Clear();
                //  MessageBox.Show("No Item is selected");


            }
        }

        private void listView_Profiles_MouseDown(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo info = listView_Profiles.HitTest(e.X, e.Y);
            ListViewItem item = info.Item;

            if (item != null)
            {
                //this.textBox1.Text = item.Text;
                //   MessageBox.Show(item.Text);

            }
            else
            {

                //  MessageBox.Show(item.Text);
                // this.listView_Profiles.SelectedItems.Clear();
                // this.textBox1.Text = "No Item is Selected";


            }
        }

        private void listView_Profiles_MouseUp(object sender, MouseEventArgs e)
        {
            int selectedRowIndex = 0;

            if (listView_Profiles.SelectedItems.Count > 0)
            {
                selectedRowIndex = listView_Profiles.SelectedItems[0].Index;
            }
            else
            {
                listView_Profiles.Items[selectedRowIndex].Selected = true;
            }
            //SaveEverything();
        }


        //private void VersionsGridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
       // {
            //if (e.RowIndex == -1)
            //{
            //    //SolidBrush br = new SolidBrush(Color.Blue);
            //    //e.Graphics.FillRectangle(br, e.CellBounds);
            //    //e.PaintContent(e.ClipBounds);
            //    //e.Handled = true;
            //}
            //else
            //{
            //    if (e.RowIndex % 2 == 0)
            //    {
            //        //SolidBrush br = new SolidBrush(Color.Gainsboro);

            //        //e.Graphics.FillRectangle(br, e.CellBounds);
            //        //e.PaintContent(e.ClipBounds);
            //        //e.Handled = true;
            //    }
            //    else
            //    {
            //        //SolidBrush br = new SolidBrush(Color.White);
            //        //e.Graphics.FillRectangle(br, e.CellBounds);
            //        //e.PaintContent(e.ClipBounds);
            //        //e.Handled = true;
            //    }

            //}

        //}
       
        private void VersionsGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (VersionsGridView.SelectedCells.Count > 0)
            {
                int selectedrowindex = VersionsGridView.SelectedCells[0].RowIndex;

                DataGridViewRow selectedRow = VersionsGridView.Rows[selectedrowindex];

                //Fix row selection highlight coloring.. looks pretty
                if (selectedRow.Cells[0].Style.ForeColor == Color.LightGreen)
                {
                    selectedRow.Cells[0].Style.SelectionForeColor = Color.LightGreen;
                    selectedRow.Cells[0].Style.SelectionBackColor = Color.DarkGreen;
                }
                else
                {
                    selectedRow.Cells[0].Style.SelectionForeColor = Color.Red;
                    selectedRow.Cells[0].Style.SelectionBackColor = Color.Maroon;
                }

                string a = Convert.ToString(selectedRow.Cells["Installed"].Value);
                if (a == "✓")
                {
                    VersionDownloadbutton.Text = "Uninstall Selected?";
                    uninstallversion = true;
                }
                else
                {
                    VersionDownloadbutton.Text = "Download Selected?";
                    uninstallversion = false;
                }


            }
        }

        int added = 0;
        bool addedcheck =false;
        private void tabControl_Settings_Selected(object sender, TabControlEventArgs e)
        {
            int currentHeight = tabControl_Settings.Size.Height;
            int currentWidth = tabControl_Settings.Size.Width;
            
            if (tabControl_Settings.SelectedTab.Name == "McReleases")
            {
                button_SaveSettings.Visible = false;
                addedcheck = true;
                //tabControl_Settings.Size.Height += 29;
                added = 29;
                tabControl_Settings.Size = new Size(currentWidth, currentHeight + added);

                //VersionsGridView.DataSource = McVersions.CreateSource();
                refreshMCVersion();

            } else{
                if (addedcheck) {
                    addedcheck = false;
                    button_SaveSettings.Visible = true;
                    tabControl_Settings.Size = new Size(currentWidth, currentHeight - added);
                }
            }
        }


        public void refreshMCVersion()
        {
            VersionsGridView.DataSource = McVersions.CreateSource();
            VersionsGridView.EndEdit();
            VersionsGridView.Refresh();
            //foreach (DataGridViewRow row in VersionsGridView.Rows)
            //{
            //    foreach (DataGridViewColumn column in VersionsGridView.Columns)
            //    {
            //        if (row.Cells[0].Value.ToString() == "✓")
            //        {
            //            VersionsGridView.Rows[row.Index].Cells[column.Index].Style.ForeColor = Color.LightGreen;
            //            VersionsGridView.Rows[row.Index].Cells[column.Index].Style.BackColor = Color.Green;
            //            //ModpacksGridView[column.Index, row.Index].Style.ForeColor = Color.GreenYellow;
            //            break; //Otherwise it will continune painting all the other cells on the same row.
            //        }
            //        else
            //        {
            //            VersionsGridView.Rows[row.Index].Cells[column.Index].Style.ForeColor = Color.Red;
            //            VersionsGridView.Rows[row.Index].Cells[column.Index].Style.BackColor = Color.DarkRed;
            //            break;
            //        }
            //    }

            //}

        
            VersionsGridView.Columns["Installed"].Width = 52;
            VersionsGridView.Columns["Installed"].MinimumWidth = 52;
            VersionsGridView.Columns["Name"].Width = 64;
            VersionsGridView.Columns["Name"].MinimumWidth = 64;
            VersionsGridView.Columns["Type"].Width = 55;
            VersionsGridView.Columns["Type"].MinimumWidth = 55;
            VersionsGridView.Columns["Time"].Width = 108;
            VersionsGridView.Columns["Time"].MinimumWidth = 108;
            VersionsGridView.Columns["ReleaseTime"].Width = 108;
            VersionsGridView.Columns["ReleaseTime"].MinimumWidth = 108;
            VersionsGridView.Columns[0].Visible = true;


        }
        private void ResizeHeaders_Click(object sender, EventArgs e)
        {
            int selectedIndex = VersionsGridView.SelectedRows[0].Index;

            refreshMCVersion();

         /*   int colCount = this.VersionsGridView.Columns.Count; // this returns the total number of columns (=6)
            //MessageBox.Show(colCount.ToString());
            colCount = colCount - 1; // =5
            for (int i = 0; i < colCount; i++)
            {
                DataGridViewColumn column = VersionsGridView.Columns[i]; // column[1] selects the required column 
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells; // sets the AutoSizeMode of column defined in previous line
                int colWidth = column.Width; // store columns width after auto resize
                //MessageBox.Show(colWidth.ToString()); // show me the autoresize width (used as a visual check really)

                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.None; // set the column resize mode to 'none' to allow manual/program changes
                colWidth += 0; // add 20 pixels to what 'colWidth' already is
                this.VersionsGridView.Columns[i].Width = colWidth; // set the columns width to the value stored in 'colWidth'
            }
          */
            VersionsGridView.Columns["Installed"].Width = 52;
            VersionsGridView.Columns["Installed"].MinimumWidth = 52;
            VersionsGridView.Columns["Name"].Width = 64;
            VersionsGridView.Columns["Name"].MinimumWidth = 64;
            VersionsGridView.Columns["Type"].Width = 55;
            VersionsGridView.Columns["Type"].MinimumWidth = 55;
            VersionsGridView.Columns["Time"].Width = 108;
            VersionsGridView.Columns["Time"].MinimumWidth = 108;
            VersionsGridView.Columns["ReleaseTime"].Width = 108;
            VersionsGridView.Columns["ReleaseTime"].MinimumWidth = 108;
            


            //refreshMCVersion();
            // VersionsGridView.SelectedRows.Clear();
            foreach (DataGridViewRow row in VersionsGridView.Rows)
            {
                if (row.Index == selectedIndex)
                    row.Selected = true;
                VersionsGridView.FirstDisplayedScrollingRowIndex = selectedIndex;
                VersionsGridView.Update();
            }




        }
        private void VersionsGridView_Sorted(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in VersionsGridView.Rows)
            //{
            //    foreach (DataGridViewColumn column in VersionsGridView.Columns)
            //    {
            //        if (row.Cells[0].Value.ToString() == "✓")
            //        {
            //            VersionsGridView.Rows[row.Index].Cells[column.Index].Style.ForeColor = Color.LightGreen;
            //            VersionsGridView.Rows[row.Index].Cells[column.Index].Style.BackColor = Color.Green;
            //            //ModpacksGridView[column.Index, row.Index].Style.ForeColor = Color.GreenYellow;
            //            break; //Otherwise it will continune painting all the other cells on the same row.
            //        }
            //        else
            //        {
            //            VersionsGridView.Rows[row.Index].Cells[column.Index].Style.ForeColor = Color.Red;
            //            VersionsGridView.Rows[row.Index].Cells[column.Index].Style.BackColor = Color.DarkRed;
            //            break;
            //        }
            //    }

            //}
        }


        private void VersionsGridView_MouseEnter(object sender, EventArgs e)
        {
            //foreach (DataGridViewRow row in VersionsGridView.Rows)
            //{
            //    foreach (DataGridViewColumn column in VersionsGridView.Columns)
            //    {
            //        if (row.Cells[0].Value.ToString() == "✓")
            //        {
            //            VersionsGridView.Rows[row.Index].Cells[column.Index].Style.ForeColor = Color.LightGreen;
            //            VersionsGridView.Rows[row.Index].Cells[column.Index].Style.BackColor = Color.Green;
            //            //ModpacksGridView[column.Index, row.Index].Style.ForeColor = Color.GreenYellow;
            //            break; //Otherwise it will continune painting all the other cells on the same row.
            //        }
            //        else
            //        {
            //            VersionsGridView.Rows[row.Index].Cells[column.Index].Style.ForeColor = Color.Red;
            //            VersionsGridView.Rows[row.Index].Cells[column.Index].Style.BackColor = Color.DarkRed;
            //            break;
            //        }
            //    }

            //}
        }
        private void VersionDownloadbutton_Click(object sender, EventArgs e)
        {
            int selectedIndex = VersionsGridView.SelectedRows[0].Index;

            string version = VersionsGridView.SelectedRows[0].Cells[1].Value.ToString();
            string versionJar = "http://s3.amazonaws.com/Minecraft.Download/versions/" + version + "/" + version + ".jar";
            string versionJson = "http://s3.amazonaws.com/Minecraft.Download/versions/" + version + "/" + version + ".json";
            string versionJarPath = @"\versions\" + version + @"\" + Path.GetFileName(versionJar);
            string versionJsonPath = @"\versions\" + version + @"\" + Path.GetFileName(versionJson);

            if (uninstallversion)
            {
                string VersionDirectory = exePath + @"\versions\" + version;
                string GameVersionDirectory = exePath + @"\" + version;

                DialogResult yesnoDialog = MessageBox.Show("Delete this version ?\n\nLocation: " + VersionDirectory, "Are you sure?", MessageBoxButtons.YesNo);
                if (yesnoDialog == DialogResult.Yes)
                {

                    Directory.Delete(VersionDirectory, true);
                    if (Directory.Exists(GameVersionDirectory))
                    {
                        DialogResult yesnoDialog2 = MessageBox.Show("Also remove the associated game directory? Which might include texturespacks, mods, screenshots etc for this version. \n\nLocation: " + GameVersionDirectory, "Are you sure?", MessageBoxButtons.YesNo);
                        if (yesnoDialog2 == DialogResult.Yes)
                        { // deletes all files, subfolders and the directory itself
                            Directory.Delete(GameVersionDirectory, true);
                        }

                    }
                    //does not delete the directory or subfolders+files
                    //Directory.GetFiles(directoryPath).ToList().ForEach(File.Delete);
                    //Directory.GetDirectories(directoryPath).ToList().ForEach(Directory.Delete);

                    VersionsGridView.DataSource = McVersions.CreateSource();
                    VersionsGridView.EndEdit();
                    VersionsGridView.Refresh();

                    // VersionsGridView.SelectedRows.Clear();
                    foreach (DataGridViewRow row in VersionsGridView.Rows)
                    {
                        if (row.Index == selectedIndex)
                            row.Selected = true;
                        VersionsGridView.FirstDisplayedScrollingRowIndex = selectedIndex;
                        VersionsGridView.Update();
                    }
                }
            }
            else
            {
                // http://s3.amazonaws.com/Minecraft.Download/versions/1.7.9/1.7.9.json



                Dictionary<int, string[]> VersionDownload = new Dictionary<int, string[]>{
                 {0, new string[] { versionJar, versionJarPath }},
                 {1, new string[] { versionJson, versionJsonPath }}
                  };

                //   Dictionary<int, string[]> demoDownloadDictonary = new Dictionary<int, string[]>{
                //{0, new string[] { versionJar, path }},
                //{1, new string[] { versionJson, path }},
                //{2, new string[] { "http://kraftzone.net/downloads/MinecraftLauncher_v1.10.zip", @"Folder\MinecraftLauncher_v1.10.zip" }},
                //{3, new string[] { "http://kraftzone.net/downloads/LiteLoader1.7.2.jar", @"Folder\LiteLoader1.7.2.jar" }}
                // };

                Downloader MultifileDownload = new Downloader(VersionDownload, exePath, exePath + versionJsonPath);
                MultifileDownload.ShowDialog();

                //After  MultifileDownload recheck versions
                comboBox_MCVersion.Items.Add(version);
                comboBox_MCVersion.Enabled = true;

                // object asd = VersionsGridView.SelectedRows[0].Cells[1].Value;

                //Store current row index selection

                refreshMCVersion();

                //reselect last selected row after refresh

                foreach (DataGridViewRow row in VersionsGridView.Rows)
                {
                    if (row.Index == selectedIndex)
                        row.Selected = true;
                    VersionsGridView.FirstDisplayedScrollingRowIndex = selectedIndex;
                    VersionsGridView.Update();
                }


                //MessageBox.Show("Added to installed mc versions: " + VersionsGridView.SelectedRows[0].Cells[1].Value.ToString());
            }

        }


        /// <summary>
        /// Decompress a zip file to a specified directory. Exclude by matching string.
        /// </summary>
        /// <param name="zipFileName">Location and file name of the zip file. Example: "C:\LOCATION\file.zip"</param>
        /// <param name="outputDirectory">Output of the files extracted. Example: "C:\LOCATION\"</param>
        /// <param name="excludeCard">Anything that matches any part of this string will not get extracted. Example: "blah"</param>
        public static void Extract(string zipFileName, string outputDirectory, string excludeCard = "")
        {
            bool exclude = true;
            if (excludeCard == "")
            {
                exclude = false;
            }
            Ionic.Zip.ZipFile zip = ZipFile.Read(zipFileName);
            Directory.CreateDirectory(outputDirectory);
            foreach (ZipEntry e in zip)
            {
                if (exclude)
                {
                    if (!e.FileName.Contains(excludeCard))
                    {
                        e.Extract(outputDirectory, ExtractExistingFileAction.DoNotOverwrite);
                        //e.Extract(outputDirectory, ExtractExistingFileAction.OverwriteSilently);
                    }
                }
                else
                {
                    e.Extract(outputDirectory, ExtractExistingFileAction.DoNotOverwrite);
                    //e.Extract(outputDirectory, ExtractExistingFileAction.OverwriteSilently);
                }
            }            
        }


        internal static string Truncate(string value, int maxLength)
        {
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static string getFiles()
        {

            
            Dictionary<int, string[]> fileInput = new Dictionary<int, string[]>();
            Dictionary<int, string[]> modInput = new Dictionary<int, string[]>();
            string status = "Successful";
            string exePath = System.Windows.Forms.Application.StartupPath;
            try
            {

                int x = 0;
                int i = 0;
                //foreach (string entry in versionData["libraries"])
                //{
                //    if (versionData["urlLibraries"][i] == "")
                //    {
                //        fileInput.Add(x, new string[] { "https://libraries.minecraft.net/" + entry.Replace(@"\", "/"), @"libraries\" + entry }); x++;
                //        fileInput.Add(x, new string[] { "https://libraries.minecraft.net/" + entry.Replace(@"\", "/") + ".sha1", @"libraries\" + entry + ".sha" }); x++;
                //    }
                //    else
                //    {
                //        modInput.Add(x, new string[] { "URL" + entry.Replace(@"\", "/"), @"libraries\" + entry }); x++;
                //        //Dev// rewrite bad, Possible idea, check to see if download is possbile, if not, ... error?
                //    }
                //    i++;
                //}
                //Dev//
                // Create a method to check for multiple xml files.
                string fileName = exePath + @"\assets\indexes\" + versionData["assets"][0] + ".json";
                string subString = "";
                bool returntoDownload = false;

                if (!File.Exists(fileName))
                {
                    //if ((DateTime.Now - File.GetLastWriteTime(fileName)).TotalHours > 8)
                    // {
                    //Dev//
                    //In the future, possibly check ETAG instead with all the files.
                    try
                    {

                        Dictionary<int, string[]> demoDownloadDictonary = new Dictionary<int, string[]>{
                            {0, new string[] {"https://s3.amazonaws.com/Minecraft.Download/indexes/" + versionData["assets"][0] + ".json", @"\assets\indexes\" + versionData["assets"][0] + ".json" }}
                             };

                        Downloader MultifileDownload = new Downloader(demoDownloadDictonary, exePath, "", "");
                        MultifileDownload.ShowDialog();

                    }
                    catch (Exception ex)
                    {
                        subString = ex.Message;
                    }
                    //}
                }

                // assetsList = JsonLoader.getVersionData(fileName);

                try
                {
                    if (File.Exists(fileName))
                    {
                        assetsList = JsonLoader.getAssetsList(fileName);
                        foreach (KeyValuePair<string, string[]> entry in assetsList)
                        {
                            if (!File.Exists(exePath + @"\assets\objects\" + Truncate(entry.Value[0], 2) + @"\" + entry.Value[0]))
                            {
                                returntoDownload = true;
                                fileInput.Add(x, new string[] { "http://resources.download.minecraft.net/" + Truncate(entry.Value[0], 2) + "/" + entry.Value[0], @"\assets\objects\" + Truncate(entry.Value[0], 2) + @"\" + entry.Value[0] }); x++;
                            }
                        }
                        //works      
                        if (returntoDownload)
                        {
                            Downloader MultifileDownload = new Downloader(fileInput, exePath, "", "");
                            MultifileDownload.ShowDialog();
                        }
                    }
                    else
                    {
                        MessageBox.Show(" Assets index missing.");
                    }
                }
                catch
                {
                    if (status == "")
                    {
                        status = "Minecraft Resources Error: Please login again.";
                    }
                }
                //if (status == "Successful")
                //{
                //    fileInput.Add(x, new string[] { "http://s3.amazonaws.com/Minecraft.Download/versions/" + versionData["id"][0] + "/" + versionData["id"][0] + ".jar", @"versions\" + versionData["id"][0] + @"\" + versionData["id"][0] + ".jar" }); x++;
                //    Dictionary<int, string[]> downloadInput = atomFileData.fileCheck(fileInput, mcLocation);
                //    Dictionary<int, string[]> modCheckInput = atomFileData.fileCheck(modInput, mcLocation);
                //    if (modCheckInput.Count > 0)
                //    {
                //        status = "Error: Modifcation Files Missing, Please reinstall mod files.";
                //    }
                //    else
                //    {
                //        //if (mcOnlineMode)
                //        // {
                //        atomDownloading.Multi(downloadInput, mcLocation);
                //        // }
                //        //     else
                //        //   {
                //        if (downloadInput.Count > 0)
                //        {
                //            status = "Offline Mode, Files Missing. You need to login and download first, before offline mode can be used.";
                //        }
                //        //  }
                //    }
                if (versionData["assets"][0] == "legacy")
                {
                    int z = 0;
                    //formText("formLabelStatus", "Checking and creating legacy files...");
                    foreach (KeyValuePair<string, string[]> entry in assetsList)
                    {
                        //formText("formLabelFileMB", (z + 1) + " / " + assetsList.Count());
                        //barValues((z + 1) * 100 / assetsList.Count(), 0);
                        if (!File.Exists(exePath + @"\assets\virtual\legacy\" + entry.Key.Replace("/", @"\")))
                        {

                            Directory.CreateDirectory(Path.GetDirectoryName(exePath + @"\assets\virtual\legacy\" + entry.Key.Replace("/", @"\")));
                            File.Copy(exePath + @"\assets\objects\" + Truncate(entry.Value[0], 2) + @"\" + entry.Value[0], exePath + @"\assets\virtual\legacy\" + entry.Key.Replace("/", @"\"));
                            z++;
                        }
                        else
                        {
                            Random rnd = new Random();
                            z += rnd.Next(2, 26);
                            if (z >= assetsList.Count())
                            {
                                break;
                            }
                        }
                    }
                    //atomLauncher.atomLaunch.formText("formLabelFileMB", "");
                    // atomLauncher.atomLaunch.barValues(0, 0);
                }
            }

            catch (Exception ex)
            {
                //if (atomLauncher.cancelPressed)
                //{
                //    status = "Canceled: " + ex.Message;
                //}
                //else
                //{
                status = "Error: Checking Minecraft Files: " + ex.Message;
                //}
            }
            return status;
        }

        private void McReleaseUpdate_Button_Click(object sender, EventArgs e)
        {
            DialogResult yesnoDialog = MessageBox.Show("Try download new version list update?", "Update versions.json?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (yesnoDialog == DialogResult.Yes)
            {
                Dictionary<int, string[]> LauncherDownload = new Dictionary<int, string[]>{
                           {0, new string[] { "http://s3.amazonaws.com/Minecraft.Download/versions/versions.json", @"\versions.json"}}
                            };

                Downloader MultifileDownload = new Downloader(LauncherDownload, exePath, "", "", 2);
                MultifileDownload.ShowDialog();


            }
            refreshMCVersion();
        }
        public void WriteResourceToFile(string resourceName, string fileName)
        {
            using (var resource = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
            {
                using (var file = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    resource.CopyTo(file);
                }
            }
        }

        private void LauncherLocation_Checkbox_CheckedChanged(object sender, EventArgs e)
        {
            if (LauncherLocation_Checkbox.Checked)
            {
                this.Text = exePath + "\\" + LauncherFileName;
            }
           if (!LauncherLocation_Checkbox.Checked){
                this.Text = "Minecraft Launcher";
           }
        }

        private void launcherlocation_Click(object sender, EventArgs e)
        {

        }

        private void radioButton_Java8_32_CheckedChanged(object sender, EventArgs e)
        {
            if (File.Exists(jre32filename_java8))
            {
                // MessageBox.Show("file exists true");
                FileInfo f = new FileInfo(jre32filename_java8);
                long s1 = f.Length;

                if (s1 == jre32bytesize_java8)
                {
                    //   MessageBox.Show("match length");
                    DownloadExtract_button.Enabled = true;
                    DownloadUpdate_button.Enabled = false;
                }
                else
                {
                    DownloadExtract_button.Enabled = false;
                    DownloadUpdate_button.Enabled = true;
                }
            }
            else
            {
                DownloadExtract_button.Enabled = false;
                DownloadUpdate_button.Enabled = true;
            }
        }

        private void radioButton_Java8_64_CheckedChanged(object sender, EventArgs e)
        {
            
                 if (File.Exists(jre64filename_java8))
            {
                // MessageBox.Show("file exists true");
                FileInfo f = new FileInfo(jre64filename_java8);
                long s1 = f.Length;

                if (s1 == jre64bytesize_java8)
                {
                    //   MessageBox.Show("match length");
                    DownloadExtract_button.Enabled = true;
                    DownloadUpdate_button.Enabled = false;
                }
                else
                {
                    DownloadExtract_button.Enabled = false;
                    DownloadUpdate_button.Enabled = true;
                }
            }
            else
            {
                DownloadExtract_button.Enabled = false;
                DownloadUpdate_button.Enabled = true;
            }
        }

        private void comboBox_JavaVersion_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelcheck();
        }
        //Debug launcher
        void p_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!IsNullOrEmpty(e.Data))
            {
                myLogger.Add("Info: " + e.Data);
                Console.WriteLine("I_out: " + e.Data);
            }
        }
        
        void p_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!IsNullOrEmpty(e.Data)) { 
            myLogger.Add("Error: " + e.Data);
            Console.WriteLine("E_out: " + e.Data);

            LaunchErrors += e.Data;
           /* if (checkBox_debug.Checked == true)
            {
                MessageBox.Show(e.Data, "Game Error");
            }*/
        }
        }

        private void button_CopyLog_Click(object sender, EventArgs e)
        {
           
            if (!IsNullOrEmpty(myLogger.Log))
            {
                if (myLogger.Log != "myLogger.Log") { 
                System.Windows.Forms.Clipboard.SetText(myLogger.Log);
                }
            }
        }

        private void tabControl_Mods_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl_Mods.SelectedTab.Name == "tabPage_modlists")
            {


                comboBox_modsVersion.Items.Add("1.8");
                comboBox_modsVersion.Items.Add("1.7.10");
                comboBox_modsVersion.Items.Add("1.7.2");
                comboBox_modsVersion.Items.Add("1.6.4");
                comboBox_modsVersion.Text = "1.8";

                RefreshModList(comboBox_modsVersion.Text);

                //Mods[] publicFeed = new Mods[7];
                //List<int> public7 = new List<int>();
                try
                {
                   // Modlist modli6ts = new Modlist();

                    //Works----------
                    //var json = System.IO.File.ReadAllText(exePath + "\\1.7.10.json");
                    //Mods[] modlists = JsonConvert.DeserializeObject<Mods[]>(json);
                    //MessageBox.Show(modlists[2].name.ToString());
                    //--------------


                    //modlists = JsonConvert.DeserializeObject<>(json);

                    //modli6ts = JsonConvert.DeserializeObject<Modlist>(json);
                    //MessageBox.Show(modli6ts.mods[1].name.ToString());    

              

                    //WebClient wc = new WebClient();
                    //var json = wc.DownloadString("http://modlist.mcf.li/api/v3/1.7.10.json");
                    //wc.Dispose();
                   // var json = System.IO.File.ReadAllText(exePath + "\\1.7.10.json");
                    
                   //Mods[] publicFeed = JsonConvert.DeserializeObject<Mods[]>(json);
         
                   //MessageBox.Show(publicFeed[1].name.ToString());
                   //MessageBox.Show(publicFeed[2].name.ToString());
                }
                catch (Exception wcE)
                {
                    MessageBox.Show("Modlist error: "+wcE.ToString());

                }
                //var json = System.IO.File.ReadAllText(exePath + "\\1.7.10.json");
                //ModList publicFeed = JsonConvert.DeserializeObject<ModList>(json);
                //MessageBox.Show(publicFeed.modlist[0].name.ToString());
            }
        }

        public void RefreshModList(string modlistVersion)
        {
            dataGridView_Modlists.DataSource = Modlistings.CreateSource(modlistVersion);
            dataGridView_Modlists.EndEdit();
            dataGridView_Modlists.Refresh();

            label_totalmods.Text= "Total mods listed: "+ dataGridView_Modlists.Rows.Count;



            dataGridView_Modlists.Columns["ID"].Width = 40; //ID
            //dataGridView_Modlists.Columns[0].MinimumWidth = 40; //ID
            dataGridView_Modlists.Columns["ID"].Visible = false; //ID
            dataGridView_Modlists.Columns["Source"].Width = 44; //Source
            dataGridView_Modlists.Columns["Source"].MinimumWidth = 44; //Source
            dataGridView_Modlists.Columns["Source"].Visible = false; //Source
            dataGridView_Modlists.Columns["Source"].FillWeight = 1;
            dataGridView_Modlists.Columns["Name"].Width = 120; //Name
            dataGridView_Modlists.Columns["Name"].MinimumWidth = 120; //Name
            dataGridView_Modlists.Columns["Name"].Visible = true; //Name
            dataGridView_Modlists.Columns["Name"].FillWeight = 15;
            //dataGridView_Modlists.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView_Modlists.Columns["Dependencies"].Width = 120; //Name
            dataGridView_Modlists.Columns["Dependencies"].MinimumWidth = 120; //Name
            dataGridView_Modlists.Columns["Dependencies"].Visible = true; //Name
            dataGridView_Modlists.Columns["Dependencies"].FillWeight = 40;
            //dataGridView_Modlists.Columns["Description"].Width = 400;//Description
            dataGridView_Modlists.Columns["Description"].Visible = false; //Description
            dataGridView_Modlists.Columns["Versions"].Width = 100;//Versions
            dataGridView_Modlists.Columns["Versions"].Visible = true; //Versions
            dataGridView_Modlists.Columns["Versions"].FillWeight = 50;
            //dataGridView_Modlists.Columns["Versions"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            //dataGridView_Modlists.Columns[4].Width = 108;//Modlist
            //dataGridView_Modlists.Columns[5].Width = 108;//Type
            //dataGridView_Modlists.Columns[6].Width = 50;//Size
            //dataGridView_Modlists.Columns[7].Width = 50;//Url
            //dataGridView_Modlists.Columns[8].Width = 115;//ReleaseTime
            //dataGridView_Modlists.Columns[3].Visible = false; //Description
            //dataGridView_Modlists.Columns[4].Visible = false; //Modlist
            //dataGridView_Modlists.Columns[5].Visible = false; //Type
            ////dataGridView_Modlists.Columns[6].Visible = false; //Size
            //dataGridView_Modlists.Columns[7].Visible = false; //Url
        }


        private void dataGridView_Modlists_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView_Modlists.SelectedCells.Count > 0)
            {
                int selectedrowindex = dataGridView_Modlists.SelectedCells[0].RowIndex;

                DataGridViewRow selectedRow = dataGridView_Modlists.Rows[selectedrowindex];


              //Fix row selection highlight coloring.. looks pretty
                if (selectedRow.Cells[0].Style.ForeColor == Color.LightGreen) {
                    selectedRow.Cells[0].Style.SelectionForeColor = Color.LightGreen;
                    selectedRow.Cells[0].Style.SelectionBackColor = Color.DarkGreen;
                } else {
                    selectedRow.Cells[0].Style.SelectionForeColor = Color.Red;
                    selectedRow.Cells[0].Style.SelectionBackColor = Color.Maroon;
                }


                int id = (int)selectedRow.Cells["ID"].Value;

                richTextBox_Modlists.Text = Modlistings.Modlisting[id].desc;

                var sb = new StringBuilder();
                sb.Append(@"{\rtf1\ansi");
                sb.Append(@"\b Name: \b0 ");
                sb.Append(Modlistings.Modlisting[id].name+"  ");
                sb.Append(@"\b Author(s): \b0 ");
                string authors = "";
                foreach (string s in Modlistings.Modlisting[id].author) {
                    authors += s + ", ";
                }
                sb.Append(authors);
                sb.Append(@" \line "); 
                sb.Append(@"\b URL: \b0 ");
                sb.Append(Modlistings.Modlisting[id].link+"  ");

                sb.Append(@" \line \line ");
                sb.Append(@"\b Description: \b0 ");
                sb.Append(Modlistings.Modlisting[id].desc);

                //sb.Append(@"\b Version(s): \b0 ");
                //string versions = "";
                //foreach (string s in Modlistings.Modlisting[id].versions) {
                //    versions += s + ", ";
                //}
                //sb.Append(versions);
                sb.Append(@" \line \line ");
                if (Modlistings.Modlisting[id].source != null) {
                    // sb.Append(@" \line ");
                    sb.Append(@"\b Source Code: \b0 ");
                    sb.Append(Modlistings.Modlisting[id].source);
                    sb.Append(@" \line ");
                } 
                sb.Append(@"\b Mod dependencies: \b0 ");
                string dependencies = "";
                foreach (string s in Modlistings.Modlisting[id].dependencies) {
                    dependencies += s + ", ";
                }
                sb.Append(dependencies);

            

                sb.Append(@"}");
                richTextBox_Modlists.Rtf = sb.ToString();


                //Console.WriteLine(Modlistings.Modlisting[id].desc);
            }
        }

        private void ModpacksGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow row in ModpacksGridView.Rows) {
                if (row.Cells[0].Value.ToString() == "✓") {
                    ModpacksGridView.Rows[row.Index].Cells[0].Style.ForeColor = Color.LightGreen;
                    ModpacksGridView.Rows[row.Index].Cells[0].Style.BackColor = Color.Green;
           
                } else {
                    ModpacksGridView.Rows[row.Index].Cells[0].Style.ForeColor = Color.Red;
                    ModpacksGridView.Rows[row.Index].Cells[0].Style.BackColor = Color.DarkRed;
                }
            }
        }

        private void VersionsGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow row in VersionsGridView.Rows) {
                if (row.Cells[0].Value.ToString() == "✓") {
                    VersionsGridView.Rows[row.Index].Cells[0].Style.ForeColor = Color.LightGreen;
                    VersionsGridView.Rows[row.Index].Cells[0].Style.BackColor = Color.Green;

                } else {
                    VersionsGridView.Rows[row.Index].Cells[0].Style.ForeColor = Color.Red;
                    VersionsGridView.Rows[row.Index].Cells[0].Style.BackColor = Color.DarkRed;
                }
            }
        }

        private void richTextBox_Modlists_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            DialogResult yesnoDialog = MessageBox.Show(e.LinkText.ToString(), "Copy the link below to clipboard?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (yesnoDialog == DialogResult.Yes)
            {
                Clipboard.SetText(e.LinkText);
            }
            else {
                ProcessStartInfo sInfo = new ProcessStartInfo(e.LinkText.ToString());
                Process.Start(sInfo);
            }
        }

        private void dataGridView_Modlists_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
        //    foreach (DataGridViewRow row in dataGridView_Modlists.Rows) {
        //        if (row.Cells[0].Value.ToString() == "✓") {
        //            dataGridView_Modlists.Rows[row.Index].Cells[0].Style.ForeColor = Color.LightGreen;
        //            dataGridView_Modlists.Rows[row.Index].Cells[0].Style.BackColor = Color.Green;

        //        } else {
        //            dataGridView_Modlists.Rows[row.Index].Cells[0].Style.ForeColor = Color.Red;
        //            dataGridView_Modlists.Rows[row.Index].Cells[0].Style.BackColor = Color.DarkRed;
        //        }
        //    }
        }

        private void button_ModlistRefresh_Click(object sender, EventArgs e)
        {
            string modversion = comboBox_modsVersion.Text;

            int current = dataGridView_Modlists.Rows.Count;
                
                Dictionary<int, string[]> ModlistUpdate = new Dictionary<int, string[]>{
                           {0, new string[] { "http://modlist.mcf.li/api/v3/"+modversion+".json", @"\"+modversion+".json"}}
                            };

                Downloader MultifileDownload = new Downloader(ModlistUpdate, exePath, "", "", 2);
                MultifileDownload.ShowDialog();

               RefreshModList(comboBox_modsVersion.Text);

                if (current < dataGridView_Modlists.Rows.Count)
                {
                    MessageBox.Show((dataGridView_Modlists.Rows.Count - current) +" new mods added to the list since last update.");
                }
        }

        private void openVersionFolder_Button_Click(object sender, EventArgs e)
        {
            string myPath = exePath + "\\" + comboBox_MCVersion.Text;

            if (Directory.Exists(myPath))
            {
                menuItem_gameDir.Enabled = true;
                if (Directory.Exists(myPath + "\\mods"))
                {
                    menuItem_mods.Enabled = true;
                }
                else {
                    menuItem_mods.Enabled = false;
                }
                if (Directory.Exists(myPath + "\\shaderpacks"))
                {
                    menuItem_shaderpacks.Enabled = true;
                }
                else
                {
                    menuItem_shaderpacks.Enabled = false;
                }
                if (Directory.Exists(myPath + "\\resourcepacks"))
                {
                    menuItem_resources.Enabled = true;
                }
                else
                {
                    menuItem_resources.Enabled = false;
                }
            }
            else {
                menuItem_gameDir.Enabled = false;
                menuItem_mods.Enabled = false;
                menuItem_shaderpacks.Enabled = false;
                menuItem_resources.Enabled = false;
            }
            if (Directory.Exists(exePath + "\\versions\\" + comboBox_MCVersion.Text))
            {
                menuItem_version.Enabled = true;
            }
            else
            {
                menuItem_version.Enabled = false;
            }
            Folder_contextMenu.Show(openVersionFolder_Button, new Point(0, openVersionFolder_Button.Height));
            

        }

        private void comboBox_modsVersion_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (File.Exists(exePath + "\\" + comboBox_modsVersion.Text+".json"))
            {
                modlistDl_button.Enabled = false;
                RefreshModList(comboBox_modsVersion.Text);
            }
            else {
                modlistDl_button.Enabled = true;
            }

        }

        private void linkLabel_urlkraftzone_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogResult yesnoDialog = MessageBox.Show(linkLabel_urlkraftzone.Text, "Copy the link below to clipboard?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (yesnoDialog == DialogResult.Yes)
            {
                Clipboard.SetText(linkLabel_urlkraftzone.Text);
            }
            else
            {
                ProcessStartInfo sInfo = new ProcessStartInfo(linkLabel_urlkraftzone.Text);
                Process.Start(sInfo);
            }
        }

        private void modListChanges_linkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            DialogResult yesnoDialog = MessageBox.Show(modListChanges_linkLabel.Text, "Copy the link below to clipboard?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (yesnoDialog == DialogResult.Yes)
            {
                Clipboard.SetText(modListChanges_linkLabel.Text);
            }
            else
            {
                ProcessStartInfo sInfo = new ProcessStartInfo(modListChanges_linkLabel.Text);
                Process.Start(sInfo);
            }
        }

        private void modlistDl_button_Click(object sender, EventArgs e)
        {
            string modversion = comboBox_modsVersion.Text;

            int current = dataGridView_Modlists.Rows.Count;

            Dictionary<int, string[]> ModlistUpdate = new Dictionary<int, string[]>{
                           {0, new string[] { "http://modlist.mcf.li/api/v3/"+modversion+".json", @"\"+modversion+".json"}}
                            };

            Downloader MultifileDownload = new Downloader(ModlistUpdate, exePath, "", "", 2);
            MultifileDownload.ShowDialog();

            RefreshModList(comboBox_modsVersion.Text);
        }

        private void menuItem_version_Click(object sender, EventArgs e)
        {
            string myPath = exePath + "\\versions\\" + comboBox_MCVersion.Text;
            if (Directory.Exists(myPath))
            {
                System.Diagnostics.Process prc = new System.Diagnostics.Process();
                prc.StartInfo.FileName = myPath;
                prc.Start();
            }
        }

        private void menuItem_gameDir_Click(object sender, EventArgs e)
        {
            string myPath = exePath + "\\" + comboBox_MCVersion.Text;
            if (Directory.Exists(myPath))
            {
                System.Diagnostics.Process prc = new System.Diagnostics.Process();
                prc.StartInfo.FileName = myPath;
                prc.Start();
            }
        }

        private void menuItem_mods_Click(object sender, EventArgs e)
        {
            string myPath = exePath + "\\" + comboBox_MCVersion.Text+"\\mods";
            if (Directory.Exists(myPath))
            {
                System.Diagnostics.Process prc = new System.Diagnostics.Process();
                prc.StartInfo.FileName = myPath;
                prc.Start();
            }
        }

        private void menuItem_resources_Click(object sender, EventArgs e)
        {
            string myPath = exePath + "\\" + comboBox_MCVersion.Text + "\\resourcepacks";
            if (Directory.Exists(myPath))
            {
                System.Diagnostics.Process prc = new System.Diagnostics.Process();
                prc.StartInfo.FileName = myPath;
                prc.Start();
            }
        }

        private void menuItem_shaderpacks_Click(object sender, EventArgs e)
        {
            string myPath = exePath + "\\" + comboBox_MCVersion.Text + "\\shaderpacks";
            if (Directory.Exists(myPath))
            {
                System.Diagnostics.Process prc = new System.Diagnostics.Process();
                prc.StartInfo.FileName = myPath;
                prc.Start();
            }
        }

        private void About_Click(object sender, EventArgs e) {

        }


  

        //Same as above not required..
       /* private static void ExtractEmbeddedResource(string outputDir, string resourceLocation, string file)
        {
            using (System.IO.Stream stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceLocation + @"." + file))
                {
                    using (System.IO.FileStream fileStream = new System.IO.FileStream(System.IO.Path.Combine(outputDir, file), System.IO.FileMode.Create))
                    {
                        for (int i = 0; i < stream.Length; i++)
                        {
                            fileStream.WriteByte((byte)stream.ReadByte());
                        }
                        fileStream.Close();
                    }
                }
            
        }*/
    }
}