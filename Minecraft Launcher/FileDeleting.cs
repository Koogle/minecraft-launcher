﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.IO.Compression;
using System.Threading;

namespace Minecraft_Launcher
{
    class FileDeleting
    {
        public static void queueDelete(string pathFILE)
        {
            Thread delQuT = new Thread(() => deleteLoop(pathFILE, true));
            delQuT.Start();
        }


        public static string deleteLoop(string pathFILE, bool displayMessageBox = false, int attemtps = 10)
        {
            string status = "";
            int x = 0;
            while (true)
            {
                bool tempBool = tryToDelete(pathFILE);
                if (tempBool)
                {
                    break;
                }
                Thread.Sleep(500);
                if (x > attemtps)
                {
                    try
                    {
                        File.Delete(pathFILE);
                    }
                    catch (Exception ex)
                    {
                        status = ex.Message;
                        if (displayMessageBox) MessageBox.Show("File: " + pathFILE + "\n\nError: " + ex.Message + "\n\nMake sure Your have proper file permissons to delete these files, or, make sure a program isnt exicuting them.");
                    }
                    break;
                }
                x++;
            }
            return status;
        }
        public static bool tryToDelete(string pathFILE)
        {
            if (File.Exists(pathFILE))
            {
                try
                {
                    File.Delete(pathFILE);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        public static string deleteFolLoop(string pathFOLDER, bool recursive = true, bool displayMessageBox = false, int attemtps = 10)
        {
            string status = "";
            int x = 0;
            while (true)
            {
                bool tempBool = tryToDeleteFol(pathFOLDER, recursive);
                if (tempBool)
                {
                    break;
                }
                Thread.Sleep(500);
                if (x > attemtps)
                {
                    try
                    {
                        Directory.Delete(pathFOLDER, recursive);
                    }
                    catch (Exception ex)
                    {
                        status = ex.Message;
                        if (displayMessageBox) MessageBox.Show("File: " + pathFOLDER + "\n\nError: " + ex.Message + "\n\nMake sure Your have proper file permissons to delete these files, or, make sure a program isnt exicuting them.");
                    }
                    break;
                }
                x++;
            }
            return status;
        }
        public static bool tryToDeleteFol(string pathFOLDER, bool recursive)
        {
            if (Directory.Exists(pathFOLDER))
            {
                try
                {

                    string[] files = Directory.GetFiles(pathFOLDER);
                    string[] dirs = Directory.GetDirectories(pathFOLDER);

                    if (dirs.Length > 1) {
                        return true;
                    }
                    if (files.Length > 15) {
                        return true;
                    }
                    foreach (string file in files)
                    {
                        File.SetAttributes(file, FileAttributes.Normal);
                        File.Delete(file);
                    }

                    foreach (string dir in dirs)
                    {
                        tryToDeleteFol(dir, recursive);
                    }

                    Directory.Delete(pathFOLDER, recursive);

                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
    }
}
