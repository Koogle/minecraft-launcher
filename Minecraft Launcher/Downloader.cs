﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace Minecraft_Launcher
{
    
    public partial class Downloader : Form
    {
        WebClient webClient;               // Our WebClient that will be doing the downloading for us
        Stopwatch sw = new Stopwatch();    // The stopwatch which we will be using to calculate the download speed

        bool doDownload = true;
        int totalFilesCompleted = 0;
        bool allDownloadsComplete = false;
        bool downloadBusy = false;
        string urlAddress = "";
        string subFilePath = "";
        string locationFile = "";
        string MainFolder = "";
        long TotalBytesToBeReceived = 1;
        //long TotalBytesReceived = 0;
        int totalFiles = 0;
        int DownloadListKey = 0;
        string userAgentVer = "Kraftzone/1.0";
        int FormHeight = 150;
        string version = "";
        string extractFile = "";
        bool extractDone = false;
        int autostartclose = 0; //0 nothing, 2 autostart noautoclose ,4 autoeverything, 8 noautostart autoclose
        bool deleteTMP = false;
        bool autoextract = false;
       // Dictionary<int, string[]> DownloadList = new Dictionary<int, string[]>();
        

     
        public static Downloader atomLaunch;
        public Downloader(Dictionary<int, string[]> NewDownloadList, string MainFolder2, string version2, string extractFile2 = "", int autostartclose2 = 0, bool deleteTMP2 = false, bool autoextract2 = false)
        {
            autostartclose = autostartclose2;
            version = version2;
            deleteTMP = deleteTMP2;
            autoextract = autoextract2;
            //dirty cleanup for modpacks, cleansup <version>.json file and residing folder the directory never existed before.
            if (version != "" && !File.Exists(version) && !File.Exists(Path.GetFileNameWithoutExtension(version) + ".jar") && deleteTMP)
            {
                deleteTMP = true;
            }
            else { 
                deleteTMP = false; 
            }

            extractFile = extractFile2;
            //Downloader.ActiveForm.Height = 160;
          //  Downloader.ActiveForm.Width = 360;
            Main.DownloadList.Clear(); //wipe the main list

            Main.DownloadList = NewDownloadList;
            MainFolder = MainFolder2;
            InitializeComponent();
            totalFiles = Main.DownloadList.Count;
            Main.DownloadListTotalFiles = Main.DownloadList.Count;

            Downloader_LabelStatus.Text = "Start downloading?";
            Download_groupBox.Text = "Start Downloading?";
            Downloader_LabelDLFileName.Text = "";
            Downloader_LabelDLSpeed.Text = "Download Speed: 0.00 kb/s";
            Downloader_LabelDLSpeed.ForeColor = Color.Gray;
            Downloader_LabelFileMB.Text = "0.00MB's/0.00 MB's";
            Downloader_LabelFileMB.ForeColor = Color.Gray;
           // Downloader_LabelTotalMB.Text = "Total Downloaded: 0.00 MB's";
            Downloader_LabelTotalMB.Text = "Total Downloaded: ";
            Downloader_LabelTotalMB.ForeColor = Color.Gray;
            Downloader_LabelPercent.Text = "0%";
            Downloader_LabelPercent.ForeColor = Color.Gray;
            Downloader_LabelTotalFiles.Text = "";
            Downloader_LabelPercentTotal.Text = "0%";
            Downloader_LabelPercentTotal.ForeColor = Color.Gray;
            Downloader_LabelTotalFiles.Text = "Total Files: " + Main.DownloadListTotalFiles;

            if (autostartclose >= 2 && autostartclose <8)
            {
                MultiDownload(Main.DownloadList, MainFolder);
                Download_button.Text = "Cancel Download";
            }

        }
        public void formText(String name, String text) //Sets the text from other classes and threads.
        {
            this.Controls.Find(name, true)[0].Text = text;
           
        }
        
        private void Download_button_Click(object sender, EventArgs e)
        {
            //DownloadFile("http://kraftzone.net/downloads/LiteLoader1.7.2.jar", @"C:\Games\MC1.7.2\test\LiteLoader1.7.2.jar");
            //FileDownloader.Multi(demoDownloadDictonary, @"E:\ut2k4");
            if (downloadBusy)
            {
                doDownload = false;
                Main.DownloadList.Clear();
                webClient.CancelAsync();
                webClient.Dispose();
               // this.Dispose();
                this.Close();
            }

            if (allDownloadsComplete)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
                
            }
            else {
                    MultiDownload(Main.DownloadList, MainFolder);
                    Download_button.Text = "Cancel Download";
            }
            //todo sometimes it cancels the first download 
 
        }
        //public void DownloadFile(string urlAddress, string location)
        public void MultiDownload(Dictionary<int, string[]> DownloadList, string MainFolder)
        {



            totalFilesCompleted = 0;

            if (Main.DownloadListTotalFiles != 0 && doDownload)
            {
                if (DownloadList.ContainsKey(DownloadListKey))
                {
                    string[] URLAndFileName = DownloadList[DownloadListKey];


                    //Main.getFiles();

                     urlAddress = URLAndFileName[0];
                     subFilePath = URLAndFileName[1];
                     locationFile = MainFolder + subFilePath;

                     //if (extractFile == Path.GetFileName(locationFile) && extractDone != true)
                     //{
                     //    if (File.Exists(extractFile))
                     //    {
                     //        ExtractFile(extractFile, MainFolder);
                     //        Download_button.Text = "Cancel Extract";
                     //        doDownload = false;
                     //    }
                     //}


                     if (doDownload)
                     {
                         Downloader_LabelStatus.Text = "Downloading file:";
                         Download_groupBox.Text = "Downloading file:";
                         Downloader_LabelFileMB.ForeColor = Color.Black;
                         Downloader_LabelTotalMB.ForeColor = Color.Black;
                         Downloader_LabelPercent.ForeColor = Color.Black;
                         Downloader_LabelDLSpeed.ForeColor = Color.Black;

                         totalFilesCompleted += 1;
                         Downloader_LabelTotalFiles.Text = "Total Files: " + totalFilesCompleted + "/" + Main.DownloadListTotalFiles;
                         try
                         {
                             Directory.CreateDirectory(Path.GetDirectoryName(locationFile));
                         }
                         catch (Exception e)
                         {
                             MessageBox.Show("Could not create directory for " + Path.GetDirectoryName(locationFile) + "\n" + DownloadList + " " + e);
                             doDownload = false;
                         }

                         if (doDownload){
                         using (webClient = new WebClient())
                         {
                            
                             //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                             //webClient.Headers[HttpRequestHeader.UserAgent] = userAgentVer;
                             //webClient.Headers["User-Agent"] = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.15) Gecko/20110303 Firefox/3.6.15";
                             webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                             webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);

                             

                             // The variable that will be holding the url address (making sure it starts with http://)
                             //Uri URL = urlAddress.StartsWith("http://", StringComparison.OrdinalIgnoreCase) ? new Uri(urlAddress) : new Uri("http://" + urlAddress);
                             // Uri URL = urlAddress.StartsWith("https://", StringComparison.OrdinalIgnoreCase) ? new Uri(urlAddress) : new Uri("https://" + urlAddress);
                             Uri URL = new Uri(urlAddress);

                  
                             // Start the stopwatch which we will be using to calculate the download speed
                             sw.Start();
                             try
                             {
                                 // Start downloading the file

                                 Downloader_LabelDLFileName.Text = Path.GetFileName(urlAddress);

                                 Downloader_progressBarTotalFiles.Value = 0;
                                 Downloader_LabelPercentTotal.Text = "0%";

                                 
                                     doDownload = false;
                                     downloadBusy = true;
                                     webClient.DownloadFileAsync(URL, locationFile);
                                     
                                 
                             }
                             catch (Exception ex)
                             {
                                 MessageBox.Show(ex.Message);
                             }
                            }
                         }
                     }
                }
            }

            //foreach (KeyValuePair<int, string[]> entry in urlFilePath)
            //{

            //    //Downloader_LabelTotalFiles.Text = ((Convert.ToInt32(entry.Key) + 1)  / urlFilePath.Count).ToString();
            //    Downloader_LabelTotalFiles.Text = "Total Files "+ totalFiles + "/" + filesLeft;

            //     urlAddress = entry.Value[0];
            //     subFilePath = entry.Value[1];
            //     location = MainFolder + @"\" + subFilePath;
            //        try
            //        {
            //            Directory.CreateDirectory(Path.GetDirectoryName(location));
            //        }
            //        catch (Exception e)
            //        {
            //            MessageBox.Show("Could not create directory for " + urlFilePath + " " + e);
            //            cancelDownload = true;
            //        }
            //    using (webClient = new WebClient())
            //    {
            //        webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
            //        webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
            //        // The variable that will be holding the url address (making sure it starts with http://)
            //        Uri URL = urlAddress.StartsWith("http://", StringComparison.OrdinalIgnoreCase) ? new Uri(urlAddress) : new Uri("http://" + urlAddress);
            //        // Start the stopwatch which we will be using to calculate the download speed
            //        sw.Start();
            //        try
            //        {
            //            // Start downloading the file
            //            Downloader_LabelStatus.Text = "Downloading file:";
            //            Downloader_LabelDLFile.Text = Path.GetFileName(urlAddress);
            //            if (!cancelDownload)
            //            {
            //                webClient.DownloadFileAsync(URL, location);
            //                downloadBusy = true;
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            MessageBox.Show(ex.Message);
            //        }
            //    }
            //}
            //while (downloadBusy) // Wait for Complete File
            //{
            //    Thread.Sleep(100);
            //    if (cancelDownload)
            //    {
            //        break;
            //    }
            //}
        }

        public void Download() {


            Downloader_LabelDLFileName.Text = "";
            //Downloader_LabelDLSpeed.Text = "";
            //Downloader_LabelFileMB.Text = "";
            //Downloader_LabelPercent.Text = "";

            if (Main.DownloadListTotalFiles != 0 && doDownload)
            {
                Downloader_LabelStatus.Text = "Downloading file:";
                Download_groupBox.Text = "Downloading file:";

                if (Main.DownloadList.ContainsKey(DownloadListKey))
                {


                    string[] URLAndFileName = Main.DownloadList[DownloadListKey];

                    Downloader_progressBarTotalFiles.Value = (totalFilesCompleted / totalFiles) * 100;
                    Downloader_LabelPercentTotal.Text = ((totalFilesCompleted / totalFiles) * 100).ToString() + "%";

                    Downloader_LabelTotalFiles.Text = "Total Files: " + (totalFilesCompleted) + "/" + totalFiles;

                    urlAddress = URLAndFileName[0];
                    subFilePath = URLAndFileName[1];
                    locationFile = MainFolder + subFilePath;

 
                    
                    if (doDownload){
                    try
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(locationFile));
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Could not create directory for " + Main.DownloadList + " " + e);
                        doDownload = false;
                    }
                if (doDownload){
                            
                    using (webClient = new WebClient())
                    {
                        
                        webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                        webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
                        
                        // The variable that will be holding the url address (making sure it starts with http://)
                        //Uri URL = urlAddress.StartsWith("http://", StringComparison.OrdinalIgnoreCase) ? new Uri(urlAddress) : new Uri("http://" + urlAddress);
                        Uri URL = new Uri(urlAddress);
                        
                        // Start the stopwatch which we will be using to calculate the download speed
                        sw.Start();
                        try
                        {
                            // Start downloading the file
                            
                            Downloader_LabelDLFileName.Text = Path.GetFileName(urlAddress);
                            
                                doDownload = false;
                                downloadBusy = true;
                                webClient.DownloadFileAsync(URL, locationFile);
                                
                                
                            
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                      }
                    }
                    return;
                    }
                }
                
            }



            if (Main.DownloadListTotalFiles == 0 && doDownload) { 
                    versiondl();


                    legacycopy();

                    //delete tmp version file
                    if (deleteTMP && File.Exists(version) && doDownload)  
                    {
                       // FileDeleting.queueDelete(version);

                        //todo: clean this up and make more safe
                        FileDeleting.tryToDeleteFol(Path.GetDirectoryName(version), false);
                        //MessageBox.Show(Path.GetDirectoryName(version));
                    }

                    if (File.Exists(extractFile) && extractDone != true && doDownload)
                    {
                        ExtractFile(extractFile, MainFolder);
                        Download_button.Text = "Cancel Extract";
                
                    }
                    else
                    {
                        Downloader_LabelTotalFiles.Text = "Total Files: " + totalFilesCompleted + "/" + totalFiles;
                        Downloader_LabelStatus.Text = "Complete";
                        Download_groupBox.Text = "Complete";
                        allDownloadsComplete = true;
                        this.DialogResult = DialogResult.OK;
                        Download_button.Text = "Close";

                        if (autostartclose >= 4 && autostartclose <=8)
                        {
                            this.DialogResult = DialogResult.OK;
                            this.Close();
                        }
                    }
            } 
 

        }



        private void versiondl() {
            if (version != "" && doDownload)
            {

                // string versionJson = Main.exePath + "\\versions\\" + version + "\\" + version + ".json";
                if (File.Exists(version))
                {
                    FileCheck x = new FileCheck();

                    int check = x.getFiles(version);
                    Downloader_LabelStatus.Text = getFilesMessage(check);

                    if (check < 4)
                    {
                        DownloadListKey = 0;
                        totalFiles += Main.DownloadList.Count;
                        Main.DownloadListTotalFiles = Main.DownloadList.Count;
                        Download_button.Text = "Cancel Download";

                        //go back to download more
                        Download();
                        
                    }
                    
                }

            }
           // return;
        }
        private void legacycopy()
        {
            try
            {
                if (Main.versionData["assets"][0] == "legacy")
                {
                    int z = 0;
                    //formText("formLabelStatus", "Checking and creating legacy files...");
                    foreach (KeyValuePair<string, string[]> entry in Main.assetsList)
                    {
                        //formText("formLabelFileMB", (z + 1) + " / " + assetsList.Count());
                        //barValues((z + 1) * 100 / assetsList.Count(), 0);
                        if (!File.Exists(Main.exePath + @"\assets\virtual\legacy\" + entry.Key.Replace("/", @"\")))
                        {
                            Downloader_LabelStatus.Text = "Copying Legacy Files";
                            Download_groupBox.Text = "Copying Legacy Files";

                            Directory.CreateDirectory(Path.GetDirectoryName(Main.exePath + @"\assets\virtual\legacy\" + entry.Key.Replace("/", @"\")));
                            File.Copy(Main.exePath + @"\assets\objects\" + Main.Truncate(entry.Value[0], 2) + @"\" + entry.Value[0], Main.exePath + @"\assets\virtual\legacy\" + entry.Key.Replace("/", @"\"));
                            z++;
                        }
                        else
                        {
                            Random rnd = new Random();
                            z += rnd.Next(2, 26);
                            //if (z >= Main.assetsList.Count())
                            //{
                            //    break;
                            //}
                        }
                    }
                    //atomLauncher.atomLaunch.formText("formLabelFileMB", "");
                    // atomLauncher.atomLaunch.barValues(0, 0);
                }
            }catch (Exception nolegacy){
               // MessageBox.Show("Error 2153: "+nolegacy.ToString());
            }

        }

        // The event that will fire whenever the progress of the WebClient is changed
        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {

            // Calculate download speed and output it to labelSpeed.
            Downloader_LabelDLSpeed.Text = string.Format("Download Speed: {0} kb/s", (e.BytesReceived / 1024d / sw.Elapsed.TotalSeconds).ToString("0.00"));
    
            // Update the progressbar percentage only when the value is not the same.
           
            Downloader_progressBar.Value = e.ProgressPercentage;

            // Show the percentage on our label.
            Downloader_LabelPercent.Text = e.ProgressPercentage.ToString() + "%";
           
            //if (e.ProgressPercentage==0){
            //    MessageBox.Show(e.ProgressPercentage.ToString());
            //}

            // Downloader_progressBarTotalFiles.Value = ((temp / 100) / (totalFilesCompleted / totalFiles)) * 100; 
            

            // Downloader_LabelPercentTotal.Text = (((temp / 100) / (totalFilesCompleted / totalFiles)) * 100).ToString() + "%"; 
            
            // Update the label with how much data have been downloaded so far and the total size of the file we are currently downloading
            Downloader_LabelFileMB.Text = string.Format("{0} Mb's / {1} Mb's",
                (e.BytesReceived / 1024d / 1024d).ToString("0.00"),
                (e.TotalBytesToReceive / 1024d / 1024d).ToString("0.00"));
            if (e.BytesReceived > 0)
            {
                // + (TotalBytesToBeReceived / 1024d / 1024d)
                Downloader_LabelTotalMB.Text = string.Format("Total Downloaded: {0} MB's", ((TotalBytesToBeReceived+e.BytesReceived) / 1024d / 1024d).ToString("0.00"));
            }
            //Downloader_LabelTotalMB.Text = string.Format("Total Size: {0} MB's / {1} MB's",
            //Downloader_LabelTotalMB.Text = string.Format("Total Downloaded: {1} MB's",
            //((TotalBytesToBeReceived + e.BytesReceived) / 1024d / 1024d).ToString("0.00"),
            //((TotalBytesToBeReceived + e.TotalBytesToReceive) / 1024d / 1024d).ToString("0.00"));
            
        }
         
         

        // The event that will trigger when the WebClient is completed
        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            //this.BeginInvoke((MethodInvoker)delegate
            //{
            //    filesCompleted += 1;
            //    downloadBusy = false;
            //});
            if (e.Cancelled == true)
            {
                
                FileDeleting.queueDelete(locationFile);
                //MessageBox.Show("Download has been canceled. Deleted"+ location);
            }

            if (e.Error !=null)
            {
                MessageBox.Show(e.Error.Message +"\n Check your connection/firewall is not blocking, then try again", "Download Error (CTRL+C to copy this error and report it", MessageBoxButtons.OK, MessageBoxIcon.Error);
                FileDeleting.queueDelete(locationFile);
                //MessageBox.Show("Download has been canceled. Deleted"+ location);
            }
            //else if (e.Error != null)
            else  {
                // MessageBox.Show("Download completed!");

                downloadBusy = false;
                string timeinseconds = sw.Elapsed.TotalSeconds.ToString("0.00");

                FileInfo f = new FileInfo(locationFile);
                long filesize = f.Length;
                TotalBytesToBeReceived += filesize;

                Downloader_LabelTotalMB.Text = string.Format("Total Downloaded: {0} MB's", (TotalBytesToBeReceived / 1024d / 1024d).ToString("0.00"));


                //Downloader_TextBox.Text += location + " (Completed in: " + timeinseconds + "s, Size: " + ((filesize / 1024d / 1024d).ToString("0.00")) + "Mb)" + Environment.NewLine;
                Downloader_TextBox.AppendText(locationFile + " (Completed in: " + timeinseconds + "s, Size: " + ((filesize / 1024d / 1024d).ToString("0.00")) + "Mb)" + Environment.NewLine);
                // Console.WriteLine(Downloader_progressBarTotalFiles.Value.ToString());

                //never adds +1 on the last file downloaded so totalcomplete/totalfiles is the same at the end.
                if (totalFilesCompleted < totalFiles)
                {
                    totalFilesCompleted += 1;
                }
                Main.DownloadList.Remove(DownloadListKey);

                DownloadListKey += 1; //increment key
                Main.DownloadListTotalFiles = Main.DownloadList.Count;
                // Reset the stopwatch.
                sw.Reset();


                doDownload = true;
                    Download();
                

            }//else end

        }

        private void DoExtract(string extractFile)
        {
            throw new NotImplementedException();
        }
        public string getFilesMessage(int getFiles){
                        switch (getFiles){
                                //return "Downloading Library files:";
                            case 0:
                                return "Downloading Library files:";
                            case 1:
                                return "Downloading Asset index";
                            case 2:
                                return "Downloading Assets";
                            case 3:
                                return "Downloading Natives";
                            case 4:
                                return "Downloading Finished";
                            default:
                                return "Downloading Finished";
                            }

        }
        private void ShowHideLog_Click(object sender, EventArgs e)
        {
            if (FormHeight == 150)
            {

                Downloader.ActiveForm.Height = 370;
                Downloader.ActiveForm.SizeGripStyle = SizeGripStyle.Show;
                Downloader.ActiveForm.FormBorderStyle = FormBorderStyle.Sizable;
                FormHeight = 370;
                ShowHideLog.Text = "Hide Log";
            }
            else {
                Downloader.ActiveForm.Height = 150;
                Downloader.ActiveForm.Width = 380;
                Downloader.ActiveForm.SizeGripStyle = SizeGripStyle.Hide;
                Downloader.ActiveForm.FormBorderStyle = FormBorderStyle.FixedDialog;
                FormHeight = 150;
                ShowHideLog.Text = "Show Log";
            }
        }

        //public void ExtractFile2(string zipToUnpack, string unpackDirectory)
        //{
        //    string ZipToUnpack = "C:\\test.zip";
        //    string TargetDir = "C:\\";
        //    int totalEntriesToProcess = 0;
        //    using (ZipFile zip1 = ZipFile.Read(zipToUnpack)) { 

        //    totalEntriesToProcess = zip1.Entries.Count;
        //    SetProgressBarMax(zip1.Entries.Count);
        //    zip1.ExtractProgress += new EventHandler<ExtractProgressEventArgs>(zip_ExtractProgress);

        //    zip1.ExtractProgress += new EventHandler(this);
        //    new System.EventHandler(this.zip_ExtractProgress);
        //    zip1.ExtractAll(TargetDir, ExtractExistingFileAction.OverwriteSilently);
        //    }
            
        //}

        //private void SetProgressBarMax(int n)
        //{
        //    if (Downloader_progressBar.InvokeRequired)
        //    {
        //        Downloader_progressBar.Invoke(new Action(Of, Integer), new System.EventHandler(this.SetProgressBarMax));
        //        new object[] {n};
        //    }
        //    else
        //    {
        //        Downloader_progressBar.Maximum = n;
        //        Downloader_progressBar.Step = 1;
        //    }
        //}

        //private void zip_ExtractProgress(object sender, ExtractProgressEventArgs e)
        //{
        //    if ((e.EventType == Ionic.Zip.ZipProgressEventType.Extracting_AfterExtractEntry))
        //    {
        //        StepEntryProgress(e);
        //    }
        //    else if ((e.EventType == ZipProgressEventType.Extracting_BeforeExtractAll))
        //    {
        //        // StepArchiveProgress(e)
        //    }
        //}

        //private void StepEntryProgress(ZipProgressEventArgs e)
        //{
        //    if (Downloader_progressBar.InvokeRequired)
        //    {
        //        Downloader_progressBar.Invoke(new ZipProgress(new System.EventHandler(this.StepEntryProgress)), new object[] {
        //                e});
        //    }
        //    else if (!_operationCanceled)
        //    {
        //        ProgressBar1.PerformStep();
        //        System.Threading.Thread.Sleep(100);
        //        nFilesCompleted = (nFilesCompleted + 1);
        //        lblStatus.Text = string.Format("{0} of {1} files...({2})", nFilesCompleted, totalEntriesToProcess, e.CurrentEntry.FileName);
        //        this.Update();
        //    }
        //}

        public void ExtractFile(string zipToUnpack, string unpackDirectory)
        {
            if (doDownload)
            {
                //MessageBox.Show(zipToUnpack + " - " + unpackDirectory);

                Downloader_LabelTotalMB.ForeColor = Color.Gray;
                Downloader_LabelDLSpeed.ForeColor = Color.Gray;

                Downloader_LabelPercent.ForeColor = Color.Black;
                Downloader_LabelDLFileName.Text = zipToUnpack;
                Downloader_LabelStatus.Text = "Extract Progress:";

                Download_groupBox.Text = "Extract Progress:";

                Downloader_LabelStatus.Text = "Extracting files (will close when finished)";

                Download_button.Text = "Cancel";
                Download_button.Enabled = false;
                //button_Download.Visible = false;
                //progressBar_Update.Visible = true;
                int zipCount = 0;
                int numFiles = 0;
                int step = 0;
                doDownload = false;
                try
                {
                    BackgroundWorker worker = new BackgroundWorker();
                    worker.WorkerReportsProgress = true;
                    worker.WorkerSupportsCancellation = true;

                    worker.ProgressChanged += (o, e) =>
                    {
                        Downloader_progressBar.Value = e.ProgressPercentage;
                        Downloader_progressBar.Maximum = zipCount * step;
                        numFiles += 1;
                        double percent = (double)(numFiles * 100) / zipCount;
                        Downloader_LabelPercent.Text = Math.Ceiling(percent).ToString() + "%";
                        Downloader_LabelTotalFiles.Text = "Extracted Files: " + numFiles.ToString() + "/" + zipCount.ToString();
                        Download_groupBox.Text = "Extract Progress:";
                    };
                    worker.DoWork += (o, e) =>
                    {
                        using (ZipFile zip = ZipFile.Read(zipToUnpack))
                        {
                            zipCount = zip.Count;

                            step = (int)(zip.Count / 100.0);
                            int percentComplete = 0;



                            foreach (ZipEntry file in zip)
                            {
                                //dotnetzip error on .tmp files being left in the directory on errors....
                                var extractPath = Path.Combine(unpackDirectory, file.FileName);
                                var extractTmpPath = string.Concat(extractPath, ".tmp");

                                // Check if the temp file already exists
                                if (File.Exists(extractTmpPath))
                                {
                                    // Remove the temp file
                                    //_Log.Info("Deleting existing temp file ({0}) to prevent problems with extraction.", extractTmpPath);
                                    //Downloader_TextBox.AppendText("Deleting existing temp file ("+extractTmpPath+") to prevent problems with extraction." + Environment.NewLine);
                                    File.Delete(extractTmpPath);
                                }
                                ////////////////////////////////////////////

                                //Downloader_TextBox.AppendText(extractPath + " (Extracted, Size: " + ((file.CompressedSize / 1024d / 1024d).ToString("0.00")) + "Mb)" + Environment.NewLine);
                                file.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
                                percentComplete += step;
                                //Downloader_LabelTotalFiles.Text = "Total Files: " + (numFiles+=1).ToString() + "/" + zipCount.ToString();
                                worker.ReportProgress(percentComplete);

                            }
                        }
                    };

                    worker.RunWorkerAsync();

                    //button_Download.Text = "Ok";
                    //button_Download.Enabled = true;


                    //worker.RunWorkerCompleted += (sender, args) => Close();

                    worker.RunWorkerCompleted += (sender, args) =>
                    {
                        Downloader_LabelStatus.Text = "Done!";

                        //MessageBox.Show("Extraction complete, this will close after pressing 'OK'." + "\n" + "Run the launcher again.");
                        //Application.Exit();
                        extractDone = true;
                        doDownload = true;
                        Download_button.Enabled = true;
                        //never adds +1 on the last file downloaded so totalcomplete/totalfiles is the same at the end.
                        if (totalFilesCompleted < totalFiles)
                        {
                            totalFilesCompleted += 1;
                        }
                        Main.DownloadList.Remove(DownloadListKey);

                        Main.DownloadListTotalFiles = Main.DownloadList.Count;
                        DownloadListKey += 1; //increment key

                        if (doDownload)
                        {
                            Downloader_progressBar.Maximum = 100;
                            Download();
                        }
                        // Close();
                    };
                }
                catch (Exception derp)
                {
                    MessageBox.Show("Error 3434: " + derp.ToString());
                }
            }
        }
     
        private void button_debug_Click(object sender, EventArgs e)
        {
            ExtractFile("jre1.7.0_25_32bit.zip", MainFolder);
        }

        private void Downloader_Load(object sender, EventArgs e)
        {
            ////Automatic Extraction..
            if (File.Exists(extractFile) && extractDone != true && doDownload && autoextract)
            {
                ExtractFile(extractFile, MainFolder);
                Download_button.Text = "Cancel Extract";

            }
        }
    }
   
}
