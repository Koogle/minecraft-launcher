﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using Newtonsoft.Json;


//https://github.com/SirCmpwn/Craft.Net/blob/master/source/Craft.Net.Client/Session.cs

namespace Minecraft_Launcher
{
    public class Session
    {
        public static Session DoLogin(string username, string password)
        {
           /* ServicePointManager.ServerCertificateValidationCallback += (s, ce, ch, errors) =>
                {
                    // TODO: This is a really nasty hack. Why doesn't Mono recognize this cert anyway?
                    return ce.GetCertHashString() == "126B6351DC3039B2A244115F3766C038F4DBBC2A";
                };*/
            var serializer = new JsonSerializer();
            try
            {
                var request = (HttpWebRequest) WebRequest.Create("https://authserver.mojang.com/authenticate");
                request.ContentType = "application/json";
                request.Method = "POST";
                var token = Guid.NewGuid().ToString();
                var blob = new AuthenticationBlob(username, password, token);
                var json = JsonConvert.SerializeObject(blob);
                var stream = request.GetRequestStream();
                using (var writer = new StreamWriter(stream)) 
                    writer.Write(json);
                var response = request.GetResponse();
                stream = response.GetResponseStream();

               
             //   var json4 = new StreamReader(stream).ReadToEnd();
               // var json33 = JsonConvert.DeserializeObject(json4);
               // MessageBox.Show(json4.ToString());
                StreamReader stream4 = new StreamReader(stream);
                var session = serializer.Deserialize<Session>(new JsonTextReader(new StreamReader(stream)));
                session.UserName = username;

                return session;
            }
            catch (WebException e)
            {
                //var stream = e.Response.GetResponseStream();
                //var json = new StreamReader(stream).ReadToEnd();
                //stream.Close();

                MessageBox.Show(e.Message);

                
                //throw JsonConvert.DeserializeObject<MinecraftAuthenticationException>(json);
                throw e;

            }
        }

        private class AuthenticationBlob
        {
            public AuthenticationBlob(string username, string password, string token)
            {
                Username = username;
                Password = password;
                ClientToken = token;
                requestUser = "true"; //Gets twitch token
               // return "Agent{name='" + this.name + '\'' + ", version=" + this.version + '}';
                Agent = new AgentBlob();
            }

            public class AgentBlob
            {
                public AgentBlob()
                {
                    // TODO: Update if needed, per https://twitter.com/sircmpwn/status/365306166638690304
                    Name = "Minecraft";
                    Version = 1;
                    
                }

                [JsonProperty("name")]
                public string Name { get; set; }

                [JsonProperty("version")]
                public int Version { get; set; }
            }


            [JsonProperty("requestUser")]
            public string requestUser { get; set; }

            [JsonProperty("agent")]
            public AgentBlob Agent { get; set; }

            [JsonProperty("username")]
            public string Username { get; set; }

            [JsonProperty("password")]
            public string Password { get; set; }

            [JsonProperty("clientToken")]
            public string ClientToken { get; set; }
        }

        private class RefreshBlob
        {
            public RefreshBlob(Session session)
            {
                AccessToken = session.AccessToken;
                ClientToken = session.ClientToken;
                SelectedProfile = session.SelectedProfile;
            }

            [JsonProperty("accessToken")]
            public string AccessToken { get; set; }

            [JsonProperty("clientToken")]
            public string ClientToken { get; set; }

            [JsonProperty("selectedProfile")]
            public Profile SelectedProfile { get; set; }
        }

        public class MinecraftAuthenticationException : Exception
        {
            public MinecraftAuthenticationException()
            {
            }

            [JsonProperty("error")]
            public string Error { get; set; }

            [JsonProperty("errorMessage")]
            public string ErrorMessage { get; set; }

            [JsonProperty("cause")]
            public string Cause { get; set; }
        }

        public class Profile
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("properties")]
            public Property[] Properties { get; set; }
             


        }

        public Session(string userName)
        {
            AvailableProfiles = new Profile[]
                {
                    new Profile
                        {
                            Name = userName,
                            Id = Guid.NewGuid().ToString()
                        }
                };
            SelectedProfile = AvailableProfiles[0];
        }

        /// <summary>
        /// Refreshes this session, so it may be used again. You will need to re-save it if you've
        /// saved it to disk.
        /// </summary>
        public void Refresh()
        {
            if (!OnlineMode)
                throw new InvalidOperationException("This is an offline-mode session.");
            var serializer = new JsonSerializer();
            try
            {
                var request = (HttpWebRequest) WebRequest.Create("https://authserver.mojang.com/authenticate");
                request.ContentType = "application/json";
                request.Method = "POST";
                var blob = new RefreshBlob(this);
                var stream = request.GetRequestStream();
                serializer.Serialize(new StreamWriter(stream), blob);
                stream.Close();
                var response = request.GetResponse();
                stream = response.GetResponseStream();
                blob = serializer.Deserialize<RefreshBlob>(new JsonTextReader(new StreamReader(stream)));
                this.AccessToken = blob.AccessToken;
                this.ClientToken = blob.ClientToken;
                this.SelectedProfile = blob.SelectedProfile;
                // TODO: Add profile to available profiles if need be
            }
            catch (WebException e)
            {
                var stream = e.Response.GetResponseStream();
                throw serializer.Deserialize<WebException>(new JsonTextReader(new StreamReader(stream)));
            }
        }
        public class Property
        {

            [JsonProperty("name")]
            public string Name{ get; set; }

            [JsonProperty("value")]
            public string Value{ get; set; }
        }

        [JsonProperty("availableProfiles")]
        public Profile[] AvailableProfiles { get; set; }

        [JsonProperty("selectedProfile")]
        public Profile SelectedProfile { get; set; }

        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("clientToken")]
        public string ClientToken { get; set; }

        [JsonProperty("user")]
        public Profile User { get; set; }



        /// <summary>
        /// This is NOT the name of the player, which can be found in SelectedProfile. This is the username
        /// the user logged in with.
        /// </summary>
        [JsonIgnore]
        public string UserName { get; set; }

        [JsonIgnore]
        public string SessionId
        {
            get { return "token:" + AccessToken + ":" + SelectedProfile.Id; }
        }

        [JsonIgnore]
        public bool OnlineMode
        {
            get { return AccessToken != null; }
        }
    }
}