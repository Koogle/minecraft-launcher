﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Minecraft_Launcher
{
    class McVersions
    {
        public static string exePath = System.Windows.Forms.Application.StartupPath;
        public static DataTable CreateSource() {

            //DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(json2);


            //DataTable dataTable = dataSet.Tables["versions"];

            //Console.WriteLine(dataTable.Rows.Count);

            //foreach (DataRow row in dataTable.Rows)
            //{
            //    Console.WriteLine(row["id"] + " - " + row["releaseTime"]);
            //}

        //            {
        //    "id": "1.2.9",
        //    "time": "2014-05-02T11:38:17+00:00",
        //    "releaseTime": "2014-05-02T11:38:17+00:00",
        //    "type": "snapshot"
        //},
            

            

            if (File.Exists(exePath + "\\versions.json"))
            {
                return version();
            }
            else {
                DialogResult yesnoDialog = MessageBox.Show("Try download new version list update", "File not found: versions.json!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (yesnoDialog == DialogResult.Yes)
                {
                    Dictionary<int, string[]> LauncherDownload = new Dictionary<int, string[]>{
                           {0, new string[] { "http://s3.amazonaws.com/Minecraft.Download/versions/versions.json", @"\versions.json"}}
                            };

                    Downloader MultifileDownload = new Downloader(LauncherDownload, exePath, "", "");
                    MultifileDownload.ShowDialog();

                    
                }
                return version();
            }
            
        }
        public static DataTable version()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Installed", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Type", typeof(string));
            table.Columns.Add("Time", typeof(DateTime));
            table.Columns.Add("ReleaseTime", typeof(DateTime));
            

            try
            {
                var json = System.IO.File.ReadAllText(exePath + "\\versions.json");

                dynamic version = JsonConvert.DeserializeObject(json);

                if (!Directory.Exists(exePath + @"\versions")) {
                    Directory.CreateDirectory(exePath + @"\versions");
                }

                
                String[] VersionFolders = Directory.GetDirectories(exePath + @"\versions");


                foreach (var typeStr in version.versions)
                {
                    DataRow row = table.NewRow();
                    //check if versions folder has any subfolders..if not skip
                    if (Directory.GetDirectories(exePath + @"\versions").Length > 0)
                    {

                        foreach (string s in VersionFolders)
                        {
                            string removeBaseDir = s.Replace(exePath + @"\versions\", "");

                            //if ("1.7.9".Equals(typeStr.id.ToString()))  works

                            if (removeBaseDir.Equals(typeStr.id.ToString())) //does not work ???
                            {

                                row["Installed"] = "✓";
                                break;
                            }
                            else
                            {
                                row["Installed"] = " X";
                            }

                        }
                        
                        row["Name"] = typeStr.id;
                        row["Type"] = typeStr.type;
                        row["Time"] = typeStr.time;
                        row["ReleaseTime"] = typeStr.releaseTime;
                        
                        table.Rows.Add(row);
                    }
                    else
                    {
                        row["Installed"] = " X";
                        row["Name"] = typeStr.id;
                        row["Type"] = typeStr.type;
                        row["Time"] = typeStr.time;
                        row["ReleaseTime"] = typeStr.releaseTime;
                        
                        table.Rows.Add(row);
                    }
                }
                return table;

            }catch(Exception blah){
                
                MessageBox.Show(blah.ToString());
                return table;
            }
        }
        
        
   }
}
