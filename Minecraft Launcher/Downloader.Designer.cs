﻿namespace Minecraft_Launcher
{
    partial class Downloader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Downloader));
            this.Downloader_progressBar = new System.Windows.Forms.ProgressBar();
            this.Downloader_LabelStatus = new System.Windows.Forms.Label();
            this.Downloader_LabelDLFileName = new System.Windows.Forms.Label();
            this.Downloader_LabelDLSpeed = new System.Windows.Forms.Label();
            this.Downloader_LabelFileMB = new System.Windows.Forms.Label();
            this.Downloader_LabelTotalMB = new System.Windows.Forms.Label();
            this.Download_button = new System.Windows.Forms.Button();
            this.Downloader_LabelPercent = new System.Windows.Forms.Label();
            this.Downloader_LabelTotalFiles = new System.Windows.Forms.Label();
            this.Downloader_progressBarTotalFiles = new System.Windows.Forms.ProgressBar();
            this.Downloader_LabelPercentTotal = new System.Windows.Forms.Label();
            this.Download_groupBox = new System.Windows.Forms.GroupBox();
            this.button_debug = new System.Windows.Forms.Button();
            this.ShowHideLog = new System.Windows.Forms.Button();
            this.Downloader_TextBox = new System.Windows.Forms.TextBox();
            this.Download_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // Downloader_progressBar
            // 
            this.Downloader_progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Downloader_progressBar.Location = new System.Drawing.Point(9, 34);
            this.Downloader_progressBar.Name = "Downloader_progressBar";
            this.Downloader_progressBar.Size = new System.Drawing.Size(306, 16);
            this.Downloader_progressBar.TabIndex = 0;
            // 
            // Downloader_LabelStatus
            // 
            this.Downloader_LabelStatus.AutoSize = true;
            this.Downloader_LabelStatus.Location = new System.Drawing.Point(195, 16);
            this.Downloader_LabelStatus.Name = "Downloader_LabelStatus";
            this.Downloader_LabelStatus.Size = new System.Drawing.Size(85, 13);
            this.Downloader_LabelStatus.TabIndex = 1;
            this.Downloader_LabelStatus.Text = "Downloading file";
            this.Downloader_LabelStatus.Visible = false;
            // 
            // Downloader_LabelDLFileName
            // 
            this.Downloader_LabelDLFileName.AutoSize = true;
            this.Downloader_LabelDLFileName.Location = new System.Drawing.Point(6, 16);
            this.Downloader_LabelDLFileName.Name = "Downloader_LabelDLFileName";
            this.Downloader_LabelDLFileName.Size = new System.Drawing.Size(91, 13);
            this.Downloader_LabelDLFileName.TabIndex = 2;
            this.Downloader_LabelDLFileName.Text = "LabelDLFileName";
            // 
            // Downloader_LabelDLSpeed
            // 
            this.Downloader_LabelDLSpeed.AutoSize = true;
            this.Downloader_LabelDLSpeed.Location = new System.Drawing.Point(6, 54);
            this.Downloader_LabelDLSpeed.Name = "Downloader_LabelDLSpeed";
            this.Downloader_LabelDLSpeed.Size = new System.Drawing.Size(78, 13);
            this.Downloader_LabelDLSpeed.TabIndex = 3;
            this.Downloader_LabelDLSpeed.Text = "LabelDLSpeed";
            // 
            // Downloader_LabelFileMB
            // 
            this.Downloader_LabelFileMB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Downloader_LabelFileMB.AutoSize = true;
            this.Downloader_LabelFileMB.Location = new System.Drawing.Point(221, 54);
            this.Downloader_LabelFileMB.Name = "Downloader_LabelFileMB";
            this.Downloader_LabelFileMB.Size = new System.Drawing.Size(85, 13);
            this.Downloader_LabelFileMB.TabIndex = 4;
            this.Downloader_LabelFileMB.Text = "1MB\'s / 44 MB\'s";
            this.Downloader_LabelFileMB.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Downloader_LabelTotalMB
            // 
            this.Downloader_LabelTotalMB.AutoSize = true;
            this.Downloader_LabelTotalMB.Location = new System.Drawing.Point(6, 87);
            this.Downloader_LabelTotalMB.Name = "Downloader_LabelTotalMB";
            this.Downloader_LabelTotalMB.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Downloader_LabelTotalMB.Size = new System.Drawing.Size(73, 13);
            this.Downloader_LabelTotalMB.TabIndex = 5;
            this.Downloader_LabelTotalMB.Text = "LabelTotalMB";
            // 
            // Download_button
            // 
            this.Download_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Download_button.Location = new System.Drawing.Point(252, 75);
            this.Download_button.Name = "Download_button";
            this.Download_button.Size = new System.Drawing.Size(91, 23);
            this.Download_button.TabIndex = 6;
            this.Download_button.Text = "Start Download";
            this.Download_button.UseVisualStyleBackColor = true;
            this.Download_button.Click += new System.EventHandler(this.Download_button_Click);
            // 
            // Downloader_LabelPercent
            // 
            this.Downloader_LabelPercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Downloader_LabelPercent.AutoSize = true;
            this.Downloader_LabelPercent.BackColor = System.Drawing.Color.Transparent;
            this.Downloader_LabelPercent.Location = new System.Drawing.Point(313, 36);
            this.Downloader_LabelPercent.Name = "Downloader_LabelPercent";
            this.Downloader_LabelPercent.Size = new System.Drawing.Size(33, 13);
            this.Downloader_LabelPercent.TabIndex = 7;
            this.Downloader_LabelPercent.Text = "100%";
            this.Downloader_LabelPercent.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Downloader_LabelTotalFiles
            // 
            this.Downloader_LabelTotalFiles.AutoSize = true;
            this.Downloader_LabelTotalFiles.Location = new System.Drawing.Point(6, 70);
            this.Downloader_LabelTotalFiles.Name = "Downloader_LabelTotalFiles";
            this.Downloader_LabelTotalFiles.Size = new System.Drawing.Size(78, 13);
            this.Downloader_LabelTotalFiles.TabIndex = 8;
            this.Downloader_LabelTotalFiles.Text = "LabelTotalFiles";
            // 
            // Downloader_progressBarTotalFiles
            // 
            this.Downloader_progressBarTotalFiles.Location = new System.Drawing.Point(103, 53);
            this.Downloader_progressBarTotalFiles.Name = "Downloader_progressBarTotalFiles";
            this.Downloader_progressBarTotalFiles.Size = new System.Drawing.Size(65, 16);
            this.Downloader_progressBarTotalFiles.TabIndex = 9;
            this.Downloader_progressBarTotalFiles.Visible = false;
            // 
            // Downloader_LabelPercentTotal
            // 
            this.Downloader_LabelPercentTotal.AutoSize = true;
            this.Downloader_LabelPercentTotal.BackColor = System.Drawing.Color.Transparent;
            this.Downloader_LabelPercentTotal.Location = new System.Drawing.Point(90, 70);
            this.Downloader_LabelPercentTotal.Name = "Downloader_LabelPercentTotal";
            this.Downloader_LabelPercentTotal.Size = new System.Drawing.Size(94, 13);
            this.Downloader_LabelPercentTotal.TabIndex = 10;
            this.Downloader_LabelPercentTotal.Text = "LabelPercentTotal";
            this.Downloader_LabelPercentTotal.Visible = false;
            // 
            // Download_groupBox
            // 
            this.Download_groupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Download_groupBox.Controls.Add(this.button_debug);
            this.Download_groupBox.Controls.Add(this.ShowHideLog);
            this.Download_groupBox.Controls.Add(this.Downloader_LabelPercentTotal);
            this.Download_groupBox.Controls.Add(this.Downloader_LabelStatus);
            this.Download_groupBox.Controls.Add(this.Downloader_progressBarTotalFiles);
            this.Download_groupBox.Controls.Add(this.Downloader_LabelTotalFiles);
            this.Download_groupBox.Controls.Add(this.Downloader_progressBar);
            this.Download_groupBox.Controls.Add(this.Downloader_LabelDLFileName);
            this.Download_groupBox.Controls.Add(this.Downloader_LabelPercent);
            this.Download_groupBox.Controls.Add(this.Downloader_LabelDLSpeed);
            this.Download_groupBox.Controls.Add(this.Downloader_LabelTotalMB);
            this.Download_groupBox.Controls.Add(this.Download_button);
            this.Download_groupBox.Controls.Add(this.Downloader_LabelFileMB);
            this.Download_groupBox.Location = new System.Drawing.Point(7, 5);
            this.Download_groupBox.Name = "Download_groupBox";
            this.Download_groupBox.Size = new System.Drawing.Size(349, 105);
            this.Download_groupBox.TabIndex = 11;
            this.Download_groupBox.TabStop = false;
            this.Download_groupBox.Text = "Downloading File:";
            // 
            // button_debug
            // 
            this.button_debug.Location = new System.Drawing.Point(281, 0);
            this.button_debug.Name = "button_debug";
            this.button_debug.Size = new System.Drawing.Size(75, 23);
            this.button_debug.TabIndex = 12;
            this.button_debug.Text = "debug";
            this.button_debug.UseVisualStyleBackColor = true;
            this.button_debug.Visible = false;
            this.button_debug.Click += new System.EventHandler(this.button_debug_Click);
            // 
            // ShowHideLog
            // 
            this.ShowHideLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ShowHideLog.Location = new System.Drawing.Point(178, 75);
            this.ShowHideLog.Name = "ShowHideLog";
            this.ShowHideLog.Size = new System.Drawing.Size(64, 23);
            this.ShowHideLog.TabIndex = 11;
            this.ShowHideLog.Text = "Show Log";
            this.ShowHideLog.UseVisualStyleBackColor = true;
            this.ShowHideLog.Click += new System.EventHandler(this.ShowHideLog_Click);
            // 
            // Downloader_TextBox
            // 
            this.Downloader_TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Downloader_TextBox.Location = new System.Drawing.Point(7, 119);
            this.Downloader_TextBox.Multiline = true;
            this.Downloader_TextBox.Name = "Downloader_TextBox";
            this.Downloader_TextBox.ReadOnly = true;
            this.Downloader_TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.Downloader_TextBox.Size = new System.Drawing.Size(349, 0);
            this.Downloader_TextBox.TabIndex = 13;
            this.Downloader_TextBox.WordWrap = false;
            // 
            // Downloader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 112);
            this.Controls.Add(this.Downloader_TextBox);
            this.Controls.Add(this.Download_groupBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(380, 150);
            this.Name = "Downloader";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "File Progress";
            this.Load += new System.EventHandler(this.Downloader_Load);
            this.Download_groupBox.ResumeLayout(false);
            this.Download_groupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar Downloader_progressBar;
        private System.Windows.Forms.Label Downloader_LabelStatus;
        private System.Windows.Forms.Label Downloader_LabelDLFileName;
        private System.Windows.Forms.Label Downloader_LabelDLSpeed;
        private System.Windows.Forms.Label Downloader_LabelFileMB;
        private System.Windows.Forms.Label Downloader_LabelTotalMB;
        private System.Windows.Forms.Button Download_button;
        private System.Windows.Forms.Label Downloader_LabelPercent;
        private System.Windows.Forms.Label Downloader_LabelTotalFiles;
        private System.Windows.Forms.ProgressBar Downloader_progressBarTotalFiles;
        private System.Windows.Forms.Label Downloader_LabelPercentTotal;
        private System.Windows.Forms.GroupBox Download_groupBox;
        private System.Windows.Forms.Button ShowHideLog;
        private System.Windows.Forms.TextBox Downloader_TextBox;
        private System.Windows.Forms.Button button_debug;
    }
}