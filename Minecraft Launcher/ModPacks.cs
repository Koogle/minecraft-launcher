﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Minecraft_Launcher
{
    class ModPacks
    {
        public static string exePath = System.Windows.Forms.Application.StartupPath;
        public static DataTable CreateSource() {
            //DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(json2);
            //DataTable dataTable = dataSet.Tables["versions"];
            //Console.WriteLine(dataTable.Rows.Count);
            //foreach (DataRow row in dataTable.Rows)
            //{
            //    Console.WriteLine(row["id"] + " - " + row["releaseTime"]);
            //}
        //            {
        //    "id": "1.2.9",
        //    "time": "2014-05-02T11:38:17+00:00",
        //    "releaseTime": "2014-05-02T11:38:17+00:00",
        //    "type": "snapshot"
        //},
           
            if (File.Exists(exePath + "\\modpacks.json"))
            {
                return modpack();
            }
            else {
                DialogResult yesnoDialog = MessageBox.Show("Try download new modpack list update", "File not found: modpacks.json!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (yesnoDialog == DialogResult.Yes)
                {
                    Dictionary<int, string[]> LauncherDownload = new Dictionary<int, string[]>{
                           {0, new string[] { "http://kraftzone.net/KzLauncher/modpacks.json", @"\modpacks.json"}}
                            };

                    Downloader MultifileDownload = new Downloader(LauncherDownload, exePath, "", "", 4);
                    MultifileDownload.ShowDialog();

                    
                }
                return modpack();
            }
            
        }
        public static DataTable modpack()
        {
     //           {
     // "id": "1.7.10Forge",
     // "baseversion": "1.7.10",
     // "name": "1.7.10 Forge 142424",
    //  "description": "The best modpack out",
     // "modlist": "Forge1212",
    //  "time": "2014-06-26T15:05:03+00:00",
     // "releaseTime": "2014-06-26T15:05:03+00:00",
    //  "type": "snapshot"
   // },
            DataTable table = new DataTable();
            table.Columns.Add("Installed", typeof(string));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("BaseType", typeof(string)); //BaseVersion
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("Modlist", typeof(string));
            table.Columns.Add("Type", typeof(string));
            table.Columns.Add("Size", typeof(string));
            table.Columns.Add("Url", typeof(string));
            table.Columns.Add("ReleaseTime", typeof(DateTime));
            

            try
            {
                var json = System.IO.File.ReadAllText(exePath + "\\modpacks.json");

                dynamic mods = JsonConvert.DeserializeObject(json);

                if (!Directory.Exists(exePath + @"\versions")) {
                    Directory.CreateDirectory(exePath + @"\versions");
                }

                
                String[] VersionFolders = Directory.GetDirectories(exePath + @"\versions");


                foreach (var typeStr in mods.modpacks135)
                {
                    DataRow row = table.NewRow();
                    //check if versions folder has any subfolders..if not skip
                    if (Directory.GetDirectories(exePath + @"\versions").Length > 0)
                    {

                        foreach (string s in VersionFolders)
                        {
                            string removeBaseDir = s.Replace(exePath + @"\versions\", "");

                            //if ("1.7.9".Equals(typeStr.id.ToString()))  works

                            if (removeBaseDir.Equals(typeStr.id.ToString())) //does not work ???
                            {

                                row["Installed"] = "✓";
                                break;
                            }
                            else
                            {
                                foreach (string java in Main.JavaPortableFound) {
                                    if (java.Equals(typeStr.id.ToString()))
                                    {
                                        row["Installed"] = "✓";
                                        break;
                                    }
                                    else {
                                        row["Installed"] = " X"; 
                                    }
                                }
                               // row["Installed"] = " X";
                            }

                        }
                        
                        row["Name"] = typeStr.id;
                        row["BaseType"] = typeStr.baseversion;
                        row["Description"] = typeStr.description;
                        row["Modlist"] = typeStr.modlist;
                        row["Type"] = typeStr.type;
                        row["ReleaseTime"] = typeStr.releaseTime;
                        row["Size"] = typeStr.size; 
                        row["Url"] = typeStr.url;
                        table.Rows.Add(row);
                    }
                    else
                    {
                        row["Installed"] = " X";
                        row["Name"] = typeStr.id;
                        row["BaseType"] = typeStr.baseversion;
                        row["Description"] = typeStr.description;
                        row["Modlist"] = typeStr.modlist;
                        row["Type"] = typeStr.type;
                        row["ReleaseTime"] = typeStr.releaseTime;
                        row["Size"] = typeStr.size; 
                        row["Url"] = typeStr.url; 
                        table.Rows.Add(row);
                    }
                }
                return table;

            }catch(Exception blah){
                
                MessageBox.Show("Error 2640: "+blah.ToString());
                return table;
            }
        }
        
        
   }
}
