﻿namespace Minecraft_Launcher
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.label_Username = new System.Windows.Forms.Label();
            this.textBox_Username = new System.Windows.Forms.TextBox();
            this.label_Error = new System.Windows.Forms.Label();
            this.comboBox_MCVersion = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.openVersionFolder_Button = new System.Windows.Forms.Button();
            this.Auth_checkBox = new System.Windows.Forms.CheckBox();
            this.Premium_groupBox = new System.Windows.Forms.GroupBox();
            this.ShowPass_button = new System.Windows.Forms.Button();
            this.Premium_pass_textBox = new System.Windows.Forms.TextBox();
            this.Premium_user_textBox = new System.Windows.Forms.TextBox();
            this.group_JavaVersion = new System.Windows.Forms.GroupBox();
            this.button_JavaOptions = new System.Windows.Forms.Button();
            this.radioButton_JavaPortable = new System.Windows.Forms.RadioButton();
            this.radioButton_JavaDefault = new System.Windows.Forms.RadioButton();
            this.comboBox_JavaVersion = new System.Windows.Forms.ComboBox();
            this.groupBox_AutoConnect = new System.Windows.Forms.GroupBox();
            this.radioButton_CustomServer = new System.Windows.Forms.RadioButton();
            this.textBox_CustomServer = new System.Windows.Forms.TextBox();
            this.radioButton_Kraftzone = new System.Windows.Forms.RadioButton();
            this.radioButton_NoAuto = new System.Windows.Forms.RadioButton();
            this.groupBox_ScreenMode = new System.Windows.Forms.GroupBox();
            this.radioButton_Fullscreen = new System.Windows.Forms.RadioButton();
            this.radioButton_Windowed720p = new System.Windows.Forms.RadioButton();
            this.radioButton_ScreenDefault = new System.Windows.Forms.RadioButton();
            this.About = new System.Windows.Forms.Label();
            this.label_mcversionnumber = new System.Windows.Forms.Label();
            this.tabControl_Main = new System.Windows.Forms.TabControl();
            this.tabPage_Main = new System.Windows.Forms.TabPage();
            this.button_Addprofile = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_ProfileName = new System.Windows.Forms.TextBox();
            this.labelLaunch = new System.Windows.Forms.Label();
            this.button_Save = new System.Windows.Forms.Button();
            this.listView_Profiles = new System.Windows.Forms.ListView();
            this.tabPage_Settings = new System.Windows.Forms.TabPage();
            this.tabControl_Settings = new System.Windows.Forms.TabControl();
            this.LauncherOptions = new System.Windows.Forms.TabPage();
            this.button_FixVersion = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.debug_info = new System.Windows.Forms.Label();
            this.checkBox_showconsole = new System.Windows.Forms.CheckBox();
            this.launcherlocation = new System.Windows.Forms.Label();
            this.LauncherLocation_Checkbox = new System.Windows.Forms.CheckBox();
            this.checkBox_debug = new System.Windows.Forms.CheckBox();
            this.button_mcpatcher = new System.Windows.Forms.Button();
            this.groupBox_AfterLaunch = new System.Windows.Forms.GroupBox();
            this.radioButton_stayopen = new System.Windows.Forms.RadioButton();
            this.radioButton_MinimizeToTray = new System.Windows.Forms.RadioButton();
            this.radioButton_closelauncher = new System.Windows.Forms.RadioButton();
            this.radioButton_Minimize = new System.Windows.Forms.RadioButton();
            this.JavaOptions = new System.Windows.Forms.TabPage();
            this.groupBox_JavaSettings = new System.Windows.Forms.GroupBox();
            this.richTextBox_FinalJavaArgs = new System.Windows.Forms.RichTextBox();
            this.label_FinalJAVA = new System.Windows.Forms.Label();
            this.groupBox_Customjvm = new System.Windows.Forms.GroupBox();
            this.label_JavaCustom = new System.Windows.Forms.Label();
            this.textBox_JavacustomArgs = new System.Windows.Forms.TextBox();
            this.checkBoxCustomJAVA = new System.Windows.Forms.CheckBox();
            this.checkBoxEnableXMX = new System.Windows.Forms.CheckBox();
            this.groupBoxXMX = new System.Windows.Forms.GroupBox();
            this.label_MaximumSizeXMX = new System.Windows.Forms.Label();
            this.textBox_mem = new System.Windows.Forms.TextBox();
            this.numericUpDownXMX = new System.Windows.Forms.NumericUpDown();
            this.textBox_cpu = new System.Windows.Forms.TextBox();
            this.button_checkcpumem = new System.Windows.Forms.Button();
            this.label_freemem = new System.Windows.Forms.Label();
            this.label_xmx = new System.Windows.Forms.Label();
            this.McReleases = new System.Windows.Forms.TabPage();
            this.McReleaseUpdate_Button = new System.Windows.Forms.Button();
            this.ResizeHeaders = new System.Windows.Forms.Button();
            this.VersionDownloadbutton = new System.Windows.Forms.Button();
            this.VersionsGridView = new System.Windows.Forms.DataGridView();
            this.button_SaveSettings = new System.Windows.Forms.Button();
            this.tabPage_Updates = new System.Windows.Forms.TabPage();
            this.button_checkupdate = new System.Windows.Forms.Button();
            this.DownloadExtract_button = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label_tickjava64_java8 = new System.Windows.Forms.Label();
            this.label_tickjava32_java8 = new System.Windows.Forms.Label();
            this.radioButton_Java8_64 = new System.Windows.Forms.RadioButton();
            this.radioButton_Java8_32 = new System.Windows.Forms.RadioButton();
            this.label_tickjava64_java7 = new System.Windows.Forms.Label();
            this.label_tickjava32_java7 = new System.Windows.Forms.Label();
            this.radioButton_Java7_64 = new System.Windows.Forms.RadioButton();
            this.radioButton_Java7_32 = new System.Windows.Forms.RadioButton();
            this.Update_richTextBox = new System.Windows.Forms.RichTextBox();
            this.DownloadUpdate_button = new System.Windows.Forms.Button();
            this.radioButton_Minecraft172 = new System.Windows.Forms.RadioButton();
            this.label_tickmc172 = new System.Windows.Forms.Label();
            this.button_Update = new System.Windows.Forms.Button();
            this.tabPage_ModPacks = new System.Windows.Forms.TabPage();
            this.tabControl_Mods = new System.Windows.Forms.TabControl();
            this.tabPage__modpacks = new System.Windows.Forms.TabPage();
            this.button_ModPackExtract = new System.Windows.Forms.Button();
            this.ModpacksGridView = new System.Windows.Forms.DataGridView();
            this.button_ModpackDownload = new System.Windows.Forms.Button();
            this.label_ModInfo = new System.Windows.Forms.Label();
            this.button_updateMods = new System.Windows.Forms.Button();
            this.richTextBox_modpacks = new System.Windows.Forms.RichTextBox();
            this.tabPage_modlists = new System.Windows.Forms.TabPage();
            this.modlistDl_button = new System.Windows.Forms.Button();
            this.modListChanges_linkLabel = new System.Windows.Forms.LinkLabel();
            this.label_totalmods = new System.Windows.Forms.Label();
            this.richTextBox_Modlists = new System.Windows.Forms.RichTextBox();
            this.dataGridView_Modlists = new System.Windows.Forms.DataGridView();
            this.comboBox_modsVersion = new System.Windows.Forms.ComboBox();
            this.button_ModlistRefresh = new System.Windows.Forms.Button();
            this.tabPage_About = new System.Windows.Forms.TabPage();
            this.checkBoxAutoScroll = new System.Windows.Forms.CheckBox();
            this.Log_richTextBox = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button_CopyLog = new System.Windows.Forms.Button();
            this.dataGridView_modpacks = new System.Windows.Forms.DataGridView();
            this.DownloadFile = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.linkLabel_urlkraftzone = new System.Windows.Forms.LinkLabel();
            this.label_About = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBox_Profile = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PremiumToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.AutoConnectTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.Tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.notifyIcon_tray = new System.Windows.Forms.NotifyIcon(this.components);
            this.openVersionFolder_toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.openportablefolder_toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.openportablefolder_Button = new System.Windows.Forms.Button();
            this.button_Remove = new System.Windows.Forms.Button();
            this.button_Launch = new System.Windows.Forms.Button();
            this.Folder_contextMenu = new System.Windows.Forms.ContextMenu();
            this.menuItem_gameDir = new System.Windows.Forms.MenuItem();
            this.menuItem_mods = new System.Windows.Forms.MenuItem();
            this.menuItem_resources = new System.Windows.Forms.MenuItem();
            this.menuItem_shaderpacks = new System.Windows.Forms.MenuItem();
            this.menuItem_version = new System.Windows.Forms.MenuItem();
            this.groupBox1.SuspendLayout();
            this.Premium_groupBox.SuspendLayout();
            this.group_JavaVersion.SuspendLayout();
            this.groupBox_AutoConnect.SuspendLayout();
            this.groupBox_ScreenMode.SuspendLayout();
            this.tabControl_Main.SuspendLayout();
            this.tabPage_Main.SuspendLayout();
            this.tabPage_Settings.SuspendLayout();
            this.tabControl_Settings.SuspendLayout();
            this.LauncherOptions.SuspendLayout();
            this.groupBox_AfterLaunch.SuspendLayout();
            this.JavaOptions.SuspendLayout();
            this.groupBox_JavaSettings.SuspendLayout();
            this.groupBox_Customjvm.SuspendLayout();
            this.groupBoxXMX.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownXMX)).BeginInit();
            this.McReleases.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VersionsGridView)).BeginInit();
            this.tabPage_Updates.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage_ModPacks.SuspendLayout();
            this.tabControl_Mods.SuspendLayout();
            this.tabPage__modpacks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ModpacksGridView)).BeginInit();
            this.tabPage_modlists.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Modlists)).BeginInit();
            this.tabPage_About.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_modpacks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label_Username
            // 
            this.label_Username.AutoSize = true;
            this.label_Username.Location = new System.Drawing.Point(6, 26);
            this.label_Username.Name = "label_Username";
            this.label_Username.Size = new System.Drawing.Size(77, 13);
            this.label_Username.TabIndex = 1;
            this.label_Username.Text = "MC Username:";
            // 
            // textBox_Username
            // 
            this.textBox_Username.Location = new System.Drawing.Point(83, 22);
            this.textBox_Username.MaxLength = 16;
            this.textBox_Username.Name = "textBox_Username";
            this.textBox_Username.Size = new System.Drawing.Size(105, 20);
            this.textBox_Username.TabIndex = 2;
            this.textBox_Username.TextChanged += new System.EventHandler(this.textBox_Username_TextChanged);
            // 
            // label_Error
            // 
            this.label_Error.AutoSize = true;
            this.label_Error.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label_Error.Location = new System.Drawing.Point(10, 46);
            this.label_Error.Name = "label_Error";
            this.label_Error.Size = new System.Drawing.Size(127, 13);
            this.label_Error.TabIndex = 7;
            this.label_Error.Text = "Only accepts  a-zA-Z0-9_";
            this.label_Error.Visible = false;
            // 
            // comboBox_MCVersion
            // 
            this.comboBox_MCVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_MCVersion.FormattingEnabled = true;
            this.comboBox_MCVersion.Location = new System.Drawing.Point(255, 22);
            this.comboBox_MCVersion.Name = "comboBox_MCVersion";
            this.comboBox_MCVersion.Size = new System.Drawing.Size(121, 21);
            this.comboBox_MCVersion.TabIndex = 8;
            this.comboBox_MCVersion.SelectedIndexChanged += new System.EventHandler(this.comboBox_MCVersion_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(190, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "MC Version:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.openVersionFolder_Button);
            this.groupBox1.Controls.Add(this.Auth_checkBox);
            this.groupBox1.Controls.Add(this.Premium_groupBox);
            this.groupBox1.Controls.Add(this.group_JavaVersion);
            this.groupBox1.Controls.Add(this.groupBox_AutoConnect);
            this.groupBox1.Controls.Add(this.groupBox_ScreenMode);
            this.groupBox1.Controls.Add(this.label_Username);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox_Username);
            this.groupBox1.Controls.Add(this.comboBox_MCVersion);
            this.groupBox1.Controls.Add(this.label_Error);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(422, 285);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Profile";
            // 
            // openVersionFolder_Button
            // 
            this.openVersionFolder_Button.Image = global::Minecraft_Launcher.Properties.Resources.folder;
            this.openVersionFolder_Button.Location = new System.Drawing.Point(382, 21);
            this.openVersionFolder_Button.Name = "openVersionFolder_Button";
            this.openVersionFolder_Button.Size = new System.Drawing.Size(31, 23);
            this.openVersionFolder_Button.TabIndex = 21;
            this.openVersionFolder_Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.openVersionFolder_toolTip.SetToolTip(this.openVersionFolder_Button, "If a menu item is disabled, it might mean you need to.. \r\n\r\n\'Launch\' minecraft so" +
        " that it creates those folders.");
            this.openVersionFolder_Button.UseVisualStyleBackColor = true;
            this.openVersionFolder_Button.Click += new System.EventHandler(this.openVersionFolder_Button_Click);
            // 
            // Auth_checkBox
            // 
            this.Auth_checkBox.AutoSize = true;
            this.Auth_checkBox.Location = new System.Drawing.Point(14, 65);
            this.Auth_checkBox.Name = "Auth_checkBox";
            this.Auth_checkBox.Size = new System.Drawing.Size(130, 17);
            this.Auth_checkBox.TabIndex = 20;
            this.Auth_checkBox.Text = "Enable Authentication";
            this.PremiumToolTip.SetToolTip(this.Auth_checkBox, "This will replace you offline Username\r\nwith your Mojang account one.\r\n\r\nNeeded f" +
        "or premium authenticating servers.");
            this.Auth_checkBox.UseVisualStyleBackColor = true;
            this.Auth_checkBox.CheckedChanged += new System.EventHandler(this.Auth_checkBox_CheckedChanged);
            // 
            // Premium_groupBox
            // 
            this.Premium_groupBox.Controls.Add(this.ShowPass_button);
            this.Premium_groupBox.Controls.Add(this.Premium_pass_textBox);
            this.Premium_groupBox.Controls.Add(this.Premium_user_textBox);
            this.Premium_groupBox.Enabled = false;
            this.Premium_groupBox.Location = new System.Drawing.Point(146, 46);
            this.Premium_groupBox.Name = "Premium_groupBox";
            this.Premium_groupBox.Size = new System.Drawing.Size(267, 50);
            this.Premium_groupBox.TabIndex = 19;
            this.Premium_groupBox.TabStop = false;
            this.Premium_groupBox.Text = "Premium account login";
            this.PremiumToolTip.SetToolTip(this.Premium_groupBox, resources.GetString("Premium_groupBox.ToolTip"));
            // 
            // ShowPass_button
            // 
            this.ShowPass_button.Image = global::Minecraft_Launcher.Properties.Resources.change_password;
            this.ShowPass_button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ShowPass_button.Location = new System.Drawing.Point(202, 19);
            this.ShowPass_button.Name = "ShowPass_button";
            this.ShowPass_button.Size = new System.Drawing.Size(60, 23);
            this.ShowPass_button.TabIndex = 22;
            this.ShowPass_button.Text = "Show";
            this.ShowPass_button.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ShowPass_button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ShowPass_button.UseVisualStyleBackColor = true;
            this.ShowPass_button.Click += new System.EventHandler(this.ShowPass_button_Click);
            // 
            // Premium_pass_textBox
            // 
            this.Premium_pass_textBox.Location = new System.Drawing.Point(120, 20);
            this.Premium_pass_textBox.Name = "Premium_pass_textBox";
            this.Premium_pass_textBox.PasswordChar = '*';
            this.Premium_pass_textBox.Size = new System.Drawing.Size(79, 20);
            this.Premium_pass_textBox.TabIndex = 21;
            this.PremiumToolTip.SetToolTip(this.Premium_pass_textBox, "Password");
            this.Premium_pass_textBox.TextChanged += new System.EventHandler(this.Premium_pass_textBox_TextChanged);
            // 
            // Premium_user_textBox
            // 
            this.Premium_user_textBox.Location = new System.Drawing.Point(6, 20);
            this.Premium_user_textBox.Name = "Premium_user_textBox";
            this.Premium_user_textBox.Size = new System.Drawing.Size(108, 20);
            this.Premium_user_textBox.TabIndex = 20;
            this.PremiumToolTip.SetToolTip(this.Premium_user_textBox, "Email/username");
            this.Premium_user_textBox.TextChanged += new System.EventHandler(this.Premium_user_textBox_TextChanged);
            // 
            // group_JavaVersion
            // 
            this.group_JavaVersion.Controls.Add(this.button_JavaOptions);
            this.group_JavaVersion.Controls.Add(this.radioButton_JavaPortable);
            this.group_JavaVersion.Controls.Add(this.radioButton_JavaDefault);
            this.group_JavaVersion.Controls.Add(this.comboBox_JavaVersion);
            this.group_JavaVersion.Location = new System.Drawing.Point(9, 224);
            this.group_JavaVersion.Name = "group_JavaVersion";
            this.group_JavaVersion.Size = new System.Drawing.Size(404, 50);
            this.group_JavaVersion.TabIndex = 17;
            this.group_JavaVersion.TabStop = false;
            this.group_JavaVersion.Text = "Java Settings";
            // 
            // button_JavaOptions
            // 
            this.button_JavaOptions.Image = global::Minecraft_Launcher.Properties.Resources.hammer;
            this.button_JavaOptions.Location = new System.Drawing.Point(10, 16);
            this.button_JavaOptions.Name = "button_JavaOptions";
            this.button_JavaOptions.Size = new System.Drawing.Size(64, 23);
            this.button_JavaOptions.TabIndex = 19;
            this.button_JavaOptions.Text = "Extras";
            this.button_JavaOptions.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_JavaOptions.UseVisualStyleBackColor = true;
            this.button_JavaOptions.Click += new System.EventHandler(this.button_JavaOptions_Click);
            // 
            // radioButton_JavaPortable
            // 
            this.radioButton_JavaPortable.AutoSize = true;
            this.radioButton_JavaPortable.Location = new System.Drawing.Point(176, 19);
            this.radioButton_JavaPortable.Name = "radioButton_JavaPortable";
            this.radioButton_JavaPortable.Size = new System.Drawing.Size(123, 17);
            this.radioButton_JavaPortable.TabIndex = 18;
            this.radioButton_JavaPortable.Text = "JavaPortable (better)";
            this.radioButton_JavaPortable.UseVisualStyleBackColor = true;
            this.radioButton_JavaPortable.CheckedChanged += new System.EventHandler(this.radioButton_JavaPortable_CheckedChanged);
            // 
            // radioButton_JavaDefault
            // 
            this.radioButton_JavaDefault.AutoSize = true;
            this.radioButton_JavaDefault.Checked = true;
            this.radioButton_JavaDefault.Location = new System.Drawing.Point(78, 19);
            this.radioButton_JavaDefault.Name = "radioButton_JavaDefault";
            this.radioButton_JavaDefault.Size = new System.Drawing.Size(100, 17);
            this.radioButton_JavaDefault.TabIndex = 17;
            this.radioButton_JavaDefault.TabStop = true;
            this.radioButton_JavaDefault.Text = "System (default)";
            this.radioButton_JavaDefault.UseVisualStyleBackColor = true;
            this.radioButton_JavaDefault.CheckedChanged += new System.EventHandler(this.radioButton_JavaDefault_CheckedChanged);
            // 
            // comboBox_JavaVersion
            // 
            this.comboBox_JavaVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_JavaVersion.Enabled = false;
            this.comboBox_JavaVersion.FormattingEnabled = true;
            this.comboBox_JavaVersion.Location = new System.Drawing.Point(299, 18);
            this.comboBox_JavaVersion.Name = "comboBox_JavaVersion";
            this.comboBox_JavaVersion.Size = new System.Drawing.Size(98, 21);
            this.comboBox_JavaVersion.TabIndex = 15;
            this.comboBox_JavaVersion.SelectedIndexChanged += new System.EventHandler(this.comboBox_JavaVersion_SelectedIndexChanged);
            // 
            // groupBox_AutoConnect
            // 
            this.groupBox_AutoConnect.Controls.Add(this.radioButton_CustomServer);
            this.groupBox_AutoConnect.Controls.Add(this.textBox_CustomServer);
            this.groupBox_AutoConnect.Controls.Add(this.radioButton_Kraftzone);
            this.groupBox_AutoConnect.Controls.Add(this.radioButton_NoAuto);
            this.groupBox_AutoConnect.Location = new System.Drawing.Point(146, 102);
            this.groupBox_AutoConnect.Name = "groupBox_AutoConnect";
            this.groupBox_AutoConnect.Size = new System.Drawing.Size(267, 116);
            this.groupBox_AutoConnect.TabIndex = 13;
            this.groupBox_AutoConnect.TabStop = false;
            this.groupBox_AutoConnect.Text = "Auto server connect at startup";
            // 
            // radioButton_CustomServer
            // 
            this.radioButton_CustomServer.AutoSize = true;
            this.radioButton_CustomServer.Location = new System.Drawing.Point(17, 65);
            this.radioButton_CustomServer.Name = "radioButton_CustomServer";
            this.radioButton_CustomServer.Size = new System.Drawing.Size(103, 17);
            this.radioButton_CustomServer.TabIndex = 15;
            this.radioButton_CustomServer.Text = "Custom address:";
            this.radioButton_CustomServer.UseVisualStyleBackColor = true;
            this.radioButton_CustomServer.CheckedChanged += new System.EventHandler(this.radioButton_CustomServer_CheckedChanged);
            // 
            // textBox_CustomServer
            // 
            this.textBox_CustomServer.Location = new System.Drawing.Point(16, 87);
            this.textBox_CustomServer.Name = "textBox_CustomServer";
            this.textBox_CustomServer.ReadOnly = true;
            this.textBox_CustomServer.Size = new System.Drawing.Size(244, 20);
            this.textBox_CustomServer.TabIndex = 14;
            this.textBox_CustomServer.TextChanged += new System.EventHandler(this.textBox_CustomServer_TextChanged);
            // 
            // radioButton_Kraftzone
            // 
            this.radioButton_Kraftzone.AutoSize = true;
            this.radioButton_Kraftzone.Location = new System.Drawing.Point(17, 42);
            this.radioButton_Kraftzone.Name = "radioButton_Kraftzone";
            this.radioButton_Kraftzone.Size = new System.Drawing.Size(105, 17);
            this.radioButton_Kraftzone.TabIndex = 13;
            this.radioButton_Kraftzone.Text = "Mc.kraftzone.net";
            this.AutoConnectTooltip.SetToolTip(this.radioButton_Kraftzone, "If authentication is untick, this option \r\nwill use mc.kraftzone.net:25575\r\ninste" +
        "ad of the default 25565 port.");
            this.radioButton_Kraftzone.UseVisualStyleBackColor = true;
            this.radioButton_Kraftzone.CheckedChanged += new System.EventHandler(this.radioButton_Kraftzone_CheckedChanged);
            // 
            // radioButton_NoAuto
            // 
            this.radioButton_NoAuto.AutoSize = true;
            this.radioButton_NoAuto.Checked = true;
            this.radioButton_NoAuto.Location = new System.Drawing.Point(17, 19);
            this.radioButton_NoAuto.Name = "radioButton_NoAuto";
            this.radioButton_NoAuto.Size = new System.Drawing.Size(80, 17);
            this.radioButton_NoAuto.TabIndex = 12;
            this.radioButton_NoAuto.TabStop = true;
            this.radioButton_NoAuto.Text = "No (default)";
            this.radioButton_NoAuto.UseVisualStyleBackColor = true;
            this.radioButton_NoAuto.CheckedChanged += new System.EventHandler(this.radioButton_NoAuto_CheckedChanged);
            // 
            // groupBox_ScreenMode
            // 
            this.groupBox_ScreenMode.Controls.Add(this.radioButton_Fullscreen);
            this.groupBox_ScreenMode.Controls.Add(this.radioButton_Windowed720p);
            this.groupBox_ScreenMode.Controls.Add(this.radioButton_ScreenDefault);
            this.groupBox_ScreenMode.Location = new System.Drawing.Point(9, 102);
            this.groupBox_ScreenMode.Name = "groupBox_ScreenMode";
            this.groupBox_ScreenMode.Size = new System.Drawing.Size(130, 116);
            this.groupBox_ScreenMode.TabIndex = 12;
            this.groupBox_ScreenMode.TabStop = false;
            this.groupBox_ScreenMode.Text = "Screen Mode";
            // 
            // radioButton_Fullscreen
            // 
            this.radioButton_Fullscreen.AutoSize = true;
            this.radioButton_Fullscreen.Location = new System.Drawing.Point(15, 65);
            this.radioButton_Fullscreen.Name = "radioButton_Fullscreen";
            this.radioButton_Fullscreen.Size = new System.Drawing.Size(73, 17);
            this.radioButton_Fullscreen.TabIndex = 13;
            this.radioButton_Fullscreen.Text = "Fullscreen";
            this.radioButton_Fullscreen.UseVisualStyleBackColor = true;
            this.radioButton_Fullscreen.CheckedChanged += new System.EventHandler(this.radioButton_Fullscreen_CheckedChanged);
            // 
            // radioButton_Windowed720p
            // 
            this.radioButton_Windowed720p.AutoSize = true;
            this.radioButton_Windowed720p.Location = new System.Drawing.Point(15, 42);
            this.radioButton_Windowed720p.Name = "radioButton_Windowed720p";
            this.radioButton_Windowed720p.Size = new System.Drawing.Size(103, 17);
            this.radioButton_Windowed720p.TabIndex = 12;
            this.radioButton_Windowed720p.Text = "Windowed 720p";
            this.radioButton_Windowed720p.UseVisualStyleBackColor = true;
            this.radioButton_Windowed720p.CheckedChanged += new System.EventHandler(this.radioButton_Windowed720p_CheckedChanged);
            // 
            // radioButton_ScreenDefault
            // 
            this.radioButton_ScreenDefault.AutoSize = true;
            this.radioButton_ScreenDefault.Checked = true;
            this.radioButton_ScreenDefault.Location = new System.Drawing.Point(15, 19);
            this.radioButton_ScreenDefault.Name = "radioButton_ScreenDefault";
            this.radioButton_ScreenDefault.Size = new System.Drawing.Size(100, 17);
            this.radioButton_ScreenDefault.TabIndex = 11;
            this.radioButton_ScreenDefault.TabStop = true;
            this.radioButton_ScreenDefault.Text = "Default (default)";
            this.radioButton_ScreenDefault.UseVisualStyleBackColor = true;
            this.radioButton_ScreenDefault.CheckedChanged += new System.EventHandler(this.radioButton_ScreenDefault_CheckedChanged);
            // 
            // About
            // 
            this.About.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.About.AutoSize = true;
            this.About.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.About.Location = new System.Drawing.Point(271, 13);
            this.About.Name = "About";
            this.About.Size = new System.Drawing.Size(10, 13);
            this.About.TabIndex = 11;
            this.About.Text = " ";
            this.About.Click += new System.EventHandler(this.About_Click);
            // 
            // label_mcversionnumber
            // 
            this.label_mcversionnumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_mcversionnumber.AutoSize = true;
            this.label_mcversionnumber.BackColor = System.Drawing.SystemColors.Control;
            this.label_mcversionnumber.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label_mcversionnumber.Location = new System.Drawing.Point(410, 13);
            this.label_mcversionnumber.Name = "label_mcversionnumber";
            this.label_mcversionnumber.Size = new System.Drawing.Size(34, 13);
            this.label_mcversionnumber.TabIndex = 23;
            this.label_mcversionnumber.Text = "v1.10";
            // 
            // tabControl_Main
            // 
            this.tabControl_Main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl_Main.Controls.Add(this.tabPage_Main);
            this.tabControl_Main.Controls.Add(this.tabPage_Settings);
            this.tabControl_Main.Controls.Add(this.tabPage_Updates);
            this.tabControl_Main.Controls.Add(this.tabPage_ModPacks);
            this.tabControl_Main.Controls.Add(this.tabPage_About);
            this.tabControl_Main.Location = new System.Drawing.Point(6, 8);
            this.tabControl_Main.Name = "tabControl_Main";
            this.tabControl_Main.SelectedIndex = 0;
            this.tabControl_Main.Size = new System.Drawing.Size(439, 349);
            this.tabControl_Main.TabIndex = 24;
            this.tabControl_Main.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl_Main_Selected);
            // 
            // tabPage_Main
            // 
            this.tabPage_Main.Controls.Add(this.button_Addprofile);
            this.tabPage_Main.Controls.Add(this.label2);
            this.tabPage_Main.Controls.Add(this.textBox_ProfileName);
            this.tabPage_Main.Controls.Add(this.labelLaunch);
            this.tabPage_Main.Controls.Add(this.groupBox1);
            this.tabPage_Main.Controls.Add(this.button_Save);
            this.tabPage_Main.Controls.Add(this.listView_Profiles);
            this.tabPage_Main.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Main.Name = "tabPage_Main";
            this.tabPage_Main.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Main.Size = new System.Drawing.Size(431, 323);
            this.tabPage_Main.TabIndex = 1;
            this.tabPage_Main.Text = "Main";
            this.tabPage_Main.UseVisualStyleBackColor = true;
            // 
            // button_Addprofile
            // 
            this.button_Addprofile.Image = global::Minecraft_Launcher.Properties.Resources.index_cards;
            this.button_Addprofile.Location = new System.Drawing.Point(251, 297);
            this.button_Addprofile.Name = "button_Addprofile";
            this.button_Addprofile.Size = new System.Drawing.Size(92, 23);
            this.button_Addprofile.TabIndex = 27;
            this.button_Addprofile.Text = "Add Profile";
            this.button_Addprofile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_Addprofile.UseVisualStyleBackColor = true;
            this.button_Addprofile.Click += new System.EventHandler(this.button_Addprofile_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 301);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Profile Name:";
            // 
            // textBox_ProfileName
            // 
            this.textBox_ProfileName.Location = new System.Drawing.Point(145, 298);
            this.textBox_ProfileName.Name = "textBox_ProfileName";
            this.textBox_ProfileName.Size = new System.Drawing.Size(100, 20);
            this.textBox_ProfileName.TabIndex = 23;
            this.textBox_ProfileName.TextChanged += new System.EventHandler(this.textBox_ProfileName_TextChanged);
            // 
            // labelLaunch
            // 
            this.labelLaunch.AutoSize = true;
            this.labelLaunch.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.labelLaunch.Location = new System.Drawing.Point(269, 301);
            this.labelLaunch.Name = "labelLaunch";
            this.labelLaunch.Size = new System.Drawing.Size(13, 13);
            this.labelLaunch.TabIndex = 26;
            this.labelLaunch.Text = ">";
            // 
            // button_Save
            // 
            this.button_Save.Image = global::Minecraft_Launcher.Properties.Resources.page_save;
            this.button_Save.Location = new System.Drawing.Point(356, 297);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(72, 23);
            this.button_Save.TabIndex = 22;
            this.button_Save.Text = "Save";
            this.button_Save.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // listView_Profiles
            // 
            this.listView_Profiles.HideSelection = false;
            this.listView_Profiles.Location = new System.Drawing.Point(445, 13);
            this.listView_Profiles.MultiSelect = false;
            this.listView_Profiles.Name = "listView_Profiles";
            this.listView_Profiles.Size = new System.Drawing.Size(120, 192);
            this.listView_Profiles.TabIndex = 26;
            this.listView_Profiles.UseCompatibleStateImageBehavior = false;
            this.listView_Profiles.View = System.Windows.Forms.View.List;
            this.listView_Profiles.SelectedIndexChanged += new System.EventHandler(this.listView_Profiles_SelectedIndexChanged);
            this.listView_Profiles.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listView_Profiles_MouseDoubleClick);
            this.listView_Profiles.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listView_Profiles_MouseDown);
            this.listView_Profiles.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listView_Profiles_MouseUp);
            // 
            // tabPage_Settings
            // 
            this.tabPage_Settings.Controls.Add(this.tabControl_Settings);
            this.tabPage_Settings.Controls.Add(this.button_SaveSettings);
            this.tabPage_Settings.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Settings.Name = "tabPage_Settings";
            this.tabPage_Settings.Size = new System.Drawing.Size(431, 323);
            this.tabPage_Settings.TabIndex = 4;
            this.tabPage_Settings.Text = "Settings";
            this.tabPage_Settings.UseVisualStyleBackColor = true;
            this.tabPage_Settings.Click += new System.EventHandler(this.tabPage_Settings_Click);
            // 
            // tabControl_Settings
            // 
            this.tabControl_Settings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl_Settings.Controls.Add(this.LauncherOptions);
            this.tabControl_Settings.Controls.Add(this.JavaOptions);
            this.tabControl_Settings.Controls.Add(this.McReleases);
            this.tabControl_Settings.Location = new System.Drawing.Point(7, 7);
            this.tabControl_Settings.Name = "tabControl_Settings";
            this.tabControl_Settings.SelectedIndex = 0;
            this.tabControl_Settings.Size = new System.Drawing.Size(423, 285);
            this.tabControl_Settings.TabIndex = 7;
            this.tabControl_Settings.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl_Settings_Selected);
            // 
            // LauncherOptions
            // 
            this.LauncherOptions.Controls.Add(this.button_FixVersion);
            this.LauncherOptions.Controls.Add(this.label3);
            this.LauncherOptions.Controls.Add(this.debug_info);
            this.LauncherOptions.Controls.Add(this.checkBox_showconsole);
            this.LauncherOptions.Controls.Add(this.launcherlocation);
            this.LauncherOptions.Controls.Add(this.LauncherLocation_Checkbox);
            this.LauncherOptions.Controls.Add(this.checkBox_debug);
            this.LauncherOptions.Controls.Add(this.button_mcpatcher);
            this.LauncherOptions.Controls.Add(this.groupBox_AfterLaunch);
            this.LauncherOptions.Location = new System.Drawing.Point(4, 22);
            this.LauncherOptions.Name = "LauncherOptions";
            this.LauncherOptions.Padding = new System.Windows.Forms.Padding(3);
            this.LauncherOptions.Size = new System.Drawing.Size(415, 259);
            this.LauncherOptions.TabIndex = 0;
            this.LauncherOptions.Text = "Launcher Options";
            this.LauncherOptions.UseVisualStyleBackColor = true;
            // 
            // button_FixVersion
            // 
            this.button_FixVersion.Location = new System.Drawing.Point(174, 71);
            this.button_FixVersion.Name = "button_FixVersion";
            this.button_FixVersion.Size = new System.Drawing.Size(75, 23);
            this.button_FixVersion.TabIndex = 10;
            this.button_FixVersion.Text = "Attemp fix";
            this.button_FixVersion.UseVisualStyleBackColor = true;
            this.button_FixVersion.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(29, 189);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(201, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = " Will be shown in the \"About\" tab section";
            // 
            // debug_info
            // 
            this.debug_info.AutoSize = true;
            this.debug_info.ForeColor = System.Drawing.Color.Red;
            this.debug_info.Location = new System.Drawing.Point(31, 222);
            this.debug_info.Name = "debug_info";
            this.debug_info.Size = new System.Drawing.Size(277, 26);
            this.debug_info.TabIndex = 8;
            this.debug_info.Text = "This instance of Minecraft Launcher will be unable to run \r\n multiple minecraft s" +
    "esions while this option is ticked on.";
            this.debug_info.Visible = false;
            // 
            // checkBox_showconsole
            // 
            this.checkBox_showconsole.AutoSize = true;
            this.checkBox_showconsole.Location = new System.Drawing.Point(12, 170);
            this.checkBox_showconsole.Name = "checkBox_showconsole";
            this.checkBox_showconsole.Size = new System.Drawing.Size(132, 17);
            this.checkBox_showconsole.TabIndex = 7;
            this.checkBox_showconsole.Text = "Show all console info?";
            this.checkBox_showconsole.UseVisualStyleBackColor = true;
            // 
            // launcherlocation
            // 
            this.launcherlocation.AutoSize = true;
            this.launcherlocation.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.launcherlocation.Location = new System.Drawing.Point(29, 153);
            this.launcherlocation.Name = "launcherlocation";
            this.launcherlocation.Size = new System.Drawing.Size(267, 13);
            this.launcherlocation.TabIndex = 6;
            this.launcherlocation.Text = "Useful if running multiple launchers in different locations";
            this.launcherlocation.Click += new System.EventHandler(this.launcherlocation_Click);
            // 
            // LauncherLocation_Checkbox
            // 
            this.LauncherLocation_Checkbox.AutoSize = true;
            this.LauncherLocation_Checkbox.Location = new System.Drawing.Point(12, 134);
            this.LauncherLocation_Checkbox.Name = "LauncherLocation_Checkbox";
            this.LauncherLocation_Checkbox.Size = new System.Drawing.Size(188, 17);
            this.LauncherLocation_Checkbox.TabIndex = 5;
            this.LauncherLocation_Checkbox.Text = "Show location of launcher in title? ";
            this.LauncherLocation_Checkbox.UseVisualStyleBackColor = true;
            this.LauncherLocation_Checkbox.CheckedChanged += new System.EventHandler(this.LauncherLocation_Checkbox_CheckedChanged);
            // 
            // checkBox_debug
            // 
            this.checkBox_debug.AutoSize = true;
            this.checkBox_debug.ForeColor = System.Drawing.Color.Black;
            this.checkBox_debug.Location = new System.Drawing.Point(12, 205);
            this.checkBox_debug.Name = "checkBox_debug";
            this.checkBox_debug.Size = new System.Drawing.Size(189, 17);
            this.checkBox_debug.TabIndex = 5;
            this.checkBox_debug.Text = "Debug (Shows more error prompts)";
            this.checkBox_debug.UseVisualStyleBackColor = true;
            this.checkBox_debug.CheckedChanged += new System.EventHandler(this.checkBox_debug_CheckedChanged);
            // 
            // button_mcpatcher
            // 
            this.button_mcpatcher.Location = new System.Drawing.Point(174, 27);
            this.button_mcpatcher.Name = "button_mcpatcher";
            this.button_mcpatcher.Size = new System.Drawing.Size(75, 23);
            this.button_mcpatcher.TabIndex = 4;
            this.button_mcpatcher.Text = "mcpatcher";
            this.button_mcpatcher.UseVisualStyleBackColor = true;
            this.button_mcpatcher.Visible = false;
            // 
            // groupBox_AfterLaunch
            // 
            this.groupBox_AfterLaunch.Controls.Add(this.radioButton_stayopen);
            this.groupBox_AfterLaunch.Controls.Add(this.radioButton_MinimizeToTray);
            this.groupBox_AfterLaunch.Controls.Add(this.radioButton_closelauncher);
            this.groupBox_AfterLaunch.Controls.Add(this.radioButton_Minimize);
            this.groupBox_AfterLaunch.Location = new System.Drawing.Point(12, 13);
            this.groupBox_AfterLaunch.Name = "groupBox_AfterLaunch";
            this.groupBox_AfterLaunch.Size = new System.Drawing.Size(156, 115);
            this.groupBox_AfterLaunch.TabIndex = 3;
            this.groupBox_AfterLaunch.TabStop = false;
            this.groupBox_AfterLaunch.Text = "Do after pressing Launch?";
            // 
            // radioButton_stayopen
            // 
            this.radioButton_stayopen.AutoSize = true;
            this.radioButton_stayopen.Location = new System.Drawing.Point(12, 20);
            this.radioButton_stayopen.Name = "radioButton_stayopen";
            this.radioButton_stayopen.Size = new System.Drawing.Size(73, 17);
            this.radioButton_stayopen.TabIndex = 6;
            this.radioButton_stayopen.TabStop = true;
            this.radioButton_stayopen.Text = "Stay open";
            this.radioButton_stayopen.UseVisualStyleBackColor = true;
            this.radioButton_stayopen.CheckedChanged += new System.EventHandler(this.radioButton_stayopen_CheckedChanged);
            // 
            // radioButton_MinimizeToTray
            // 
            this.radioButton_MinimizeToTray.AutoSize = true;
            this.radioButton_MinimizeToTray.Location = new System.Drawing.Point(12, 64);
            this.radioButton_MinimizeToTray.Name = "radioButton_MinimizeToTray";
            this.radioButton_MinimizeToTray.Size = new System.Drawing.Size(120, 17);
            this.radioButton_MinimizeToTray.TabIndex = 5;
            this.radioButton_MinimizeToTray.TabStop = true;
            this.radioButton_MinimizeToTray.Text = "Minimize to icon tray";
            this.radioButton_MinimizeToTray.UseVisualStyleBackColor = true;
            this.radioButton_MinimizeToTray.CheckedChanged += new System.EventHandler(this.radioButton_MinimizeToTray_CheckedChanged);
            // 
            // radioButton_closelauncher
            // 
            this.radioButton_closelauncher.AutoSize = true;
            this.radioButton_closelauncher.Location = new System.Drawing.Point(12, 87);
            this.radioButton_closelauncher.Name = "radioButton_closelauncher";
            this.radioButton_closelauncher.Size = new System.Drawing.Size(95, 17);
            this.radioButton_closelauncher.TabIndex = 4;
            this.radioButton_closelauncher.TabStop = true;
            this.radioButton_closelauncher.Text = "Close launcher";
            this.radioButton_closelauncher.UseVisualStyleBackColor = true;
            this.radioButton_closelauncher.CheckedChanged += new System.EventHandler(this.radioButton_closelauncher_CheckedChanged);
            // 
            // radioButton_Minimize
            // 
            this.radioButton_Minimize.AutoSize = true;
            this.radioButton_Minimize.Location = new System.Drawing.Point(12, 41);
            this.radioButton_Minimize.Name = "radioButton_Minimize";
            this.radioButton_Minimize.Size = new System.Drawing.Size(65, 17);
            this.radioButton_Minimize.TabIndex = 3;
            this.radioButton_Minimize.TabStop = true;
            this.radioButton_Minimize.Text = "Minimize";
            this.radioButton_Minimize.UseVisualStyleBackColor = true;
            this.radioButton_Minimize.CheckedChanged += new System.EventHandler(this.radioButton_Minimize_CheckedChanged);
            // 
            // JavaOptions
            // 
            this.JavaOptions.Controls.Add(this.groupBox_JavaSettings);
            this.JavaOptions.Location = new System.Drawing.Point(4, 22);
            this.JavaOptions.Name = "JavaOptions";
            this.JavaOptions.Padding = new System.Windows.Forms.Padding(3);
            this.JavaOptions.Size = new System.Drawing.Size(415, 259);
            this.JavaOptions.TabIndex = 1;
            this.JavaOptions.Text = "Java Options";
            this.JavaOptions.UseVisualStyleBackColor = true;
            // 
            // groupBox_JavaSettings
            // 
            this.groupBox_JavaSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_JavaSettings.Controls.Add(this.richTextBox_FinalJavaArgs);
            this.groupBox_JavaSettings.Controls.Add(this.label_FinalJAVA);
            this.groupBox_JavaSettings.Controls.Add(this.groupBox_Customjvm);
            this.groupBox_JavaSettings.Controls.Add(this.checkBoxCustomJAVA);
            this.groupBox_JavaSettings.Controls.Add(this.checkBoxEnableXMX);
            this.groupBox_JavaSettings.Controls.Add(this.groupBoxXMX);
            this.groupBox_JavaSettings.Location = new System.Drawing.Point(6, 6);
            this.groupBox_JavaSettings.Name = "groupBox_JavaSettings";
            this.groupBox_JavaSettings.Size = new System.Drawing.Size(401, 246);
            this.groupBox_JavaSettings.TabIndex = 0;
            this.groupBox_JavaSettings.TabStop = false;
            this.groupBox_JavaSettings.Text = "Java Options";
            // 
            // richTextBox_FinalJavaArgs
            // 
            this.richTextBox_FinalJavaArgs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_FinalJavaArgs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richTextBox_FinalJavaArgs.Location = new System.Drawing.Point(27, 204);
            this.richTextBox_FinalJavaArgs.Name = "richTextBox_FinalJavaArgs";
            this.richTextBox_FinalJavaArgs.ReadOnly = true;
            this.richTextBox_FinalJavaArgs.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBox_FinalJavaArgs.Size = new System.Drawing.Size(360, 34);
            this.richTextBox_FinalJavaArgs.TabIndex = 14;
            this.richTextBox_FinalJavaArgs.Text = "";
            // 
            // label_FinalJAVA
            // 
            this.label_FinalJAVA.AutoSize = true;
            this.label_FinalJAVA.Location = new System.Drawing.Point(6, 186);
            this.label_FinalJAVA.Name = "label_FinalJAVA";
            this.label_FinalJAVA.Size = new System.Drawing.Size(115, 13);
            this.label_FinalJAVA.TabIndex = 5;
            this.label_FinalJAVA.Text = "Final JVM Arguements:";
            // 
            // groupBox_Customjvm
            // 
            this.groupBox_Customjvm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_Customjvm.Controls.Add(this.label_JavaCustom);
            this.groupBox_Customjvm.Controls.Add(this.textBox_JavacustomArgs);
            this.groupBox_Customjvm.Enabled = false;
            this.groupBox_Customjvm.Location = new System.Drawing.Point(27, 118);
            this.groupBox_Customjvm.Name = "groupBox_Customjvm";
            this.groupBox_Customjvm.Size = new System.Drawing.Size(360, 62);
            this.groupBox_Customjvm.TabIndex = 13;
            this.groupBox_Customjvm.TabStop = false;
            // 
            // label_JavaCustom
            // 
            this.label_JavaCustom.AutoSize = true;
            this.label_JavaCustom.Location = new System.Drawing.Point(6, 16);
            this.label_JavaCustom.Name = "label_JavaCustom";
            this.label_JavaCustom.Size = new System.Drawing.Size(164, 13);
            this.label_JavaCustom.TabIndex = 0;
            this.label_JavaCustom.Text = "Use to add additional java flags...";
            // 
            // textBox_JavacustomArgs
            // 
            this.textBox_JavacustomArgs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_JavacustomArgs.Location = new System.Drawing.Point(9, 34);
            this.textBox_JavacustomArgs.Name = "textBox_JavacustomArgs";
            this.textBox_JavacustomArgs.Size = new System.Drawing.Size(343, 20);
            this.textBox_JavacustomArgs.TabIndex = 0;
            this.textBox_JavacustomArgs.TextChanged += new System.EventHandler(this.textBox_JavacustomArgs_TextChanged);
            // 
            // checkBoxCustomJAVA
            // 
            this.checkBoxCustomJAVA.AutoSize = true;
            this.checkBoxCustomJAVA.Location = new System.Drawing.Point(9, 101);
            this.checkBoxCustomJAVA.Name = "checkBoxCustomJAVA";
            this.checkBoxCustomJAVA.Size = new System.Drawing.Size(177, 17);
            this.checkBoxCustomJAVA.TabIndex = 12;
            this.checkBoxCustomJAVA.Text = "Enable custom java arguements";
            this.checkBoxCustomJAVA.UseVisualStyleBackColor = true;
            this.checkBoxCustomJAVA.CheckedChanged += new System.EventHandler(this.checkBoxCustomJAVA_CheckedChanged);
            // 
            // checkBoxEnableXMX
            // 
            this.checkBoxEnableXMX.AutoSize = true;
            this.checkBoxEnableXMX.Location = new System.Drawing.Point(9, 19);
            this.checkBoxEnableXMX.Name = "checkBoxEnableXMX";
            this.checkBoxEnableXMX.Size = new System.Drawing.Size(122, 17);
            this.checkBoxEnableXMX.TabIndex = 11;
            this.checkBoxEnableXMX.Text = "Enable custom XMX";
            this.checkBoxEnableXMX.UseVisualStyleBackColor = true;
            this.checkBoxEnableXMX.CheckedChanged += new System.EventHandler(this.checkBoxEnableXMX_CheckedChanged);
            // 
            // groupBoxXMX
            // 
            this.groupBoxXMX.Controls.Add(this.label_MaximumSizeXMX);
            this.groupBoxXMX.Controls.Add(this.textBox_mem);
            this.groupBoxXMX.Controls.Add(this.numericUpDownXMX);
            this.groupBoxXMX.Controls.Add(this.textBox_cpu);
            this.groupBoxXMX.Controls.Add(this.button_checkcpumem);
            this.groupBoxXMX.Controls.Add(this.label_freemem);
            this.groupBoxXMX.Controls.Add(this.label_xmx);
            this.groupBoxXMX.Enabled = false;
            this.groupBoxXMX.Location = new System.Drawing.Point(27, 34);
            this.groupBoxXMX.Name = "groupBoxXMX";
            this.groupBoxXMX.Size = new System.Drawing.Size(360, 60);
            this.groupBoxXMX.TabIndex = 10;
            this.groupBoxXMX.TabStop = false;
            this.Tooltip.SetToolTip(this.groupBoxXMX, "Recommended:\r\n\r\n1024m -few  mods, default texture pack, no shaders\r\n2048m +many m" +
        "ods, higher resource packs, shaders\r\n\r\nBy default the launcher automatically set" +
        "s this value.");
            // 
            // label_MaximumSizeXMX
            // 
            this.label_MaximumSizeXMX.AutoSize = true;
            this.label_MaximumSizeXMX.Location = new System.Drawing.Point(6, 14);
            this.label_MaximumSizeXMX.Name = "label_MaximumSizeXMX";
            this.label_MaximumSizeXMX.Size = new System.Drawing.Size(213, 13);
            this.label_MaximumSizeXMX.TabIndex = 9;
            this.label_MaximumSizeXMX.Text = "Maximum size of the java memory allocated.";
            this.Tooltip.SetToolTip(this.label_MaximumSizeXMX, "Recommended:\r\n1024m -few  mods, default texture pack, no shaders\r\n2048m +many mod" +
        "s, higher resource packs, shaders\r\n\r\nBy default the launcher automatically sets " +
        "this value.");
            // 
            // textBox_mem
            // 
            this.textBox_mem.Location = new System.Drawing.Point(210, 32);
            this.textBox_mem.Name = "textBox_mem";
            this.textBox_mem.ReadOnly = true;
            this.textBox_mem.Size = new System.Drawing.Size(58, 20);
            this.textBox_mem.TabIndex = 1;
            this.textBox_mem.Text = "memory";
            // 
            // numericUpDownXMX
            // 
            this.numericUpDownXMX.Increment = new decimal(new int[] {
            64,
            0,
            0,
            0});
            this.numericUpDownXMX.Location = new System.Drawing.Point(42, 32);
            this.numericUpDownXMX.Maximum = new decimal(new int[] {
            8192,
            0,
            0,
            0});
            this.numericUpDownXMX.Minimum = new decimal(new int[] {
            512,
            0,
            0,
            0});
            this.numericUpDownXMX.Name = "numericUpDownXMX";
            this.numericUpDownXMX.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownXMX.TabIndex = 8;
            this.Tooltip.SetToolTip(this.numericUpDownXMX, "Recommended:\r\n1024m -few  mods, default texture pack, no shaders\r\n2048m +many mod" +
        "s, higher resource packs, shaders\r\n\r\nBy default the launcher automatically sets " +
        "this value.\r\n");
            this.numericUpDownXMX.Value = new decimal(new int[] {
            1024,
            0,
            0,
            0});
            this.numericUpDownXMX.ValueChanged += new System.EventHandler(this.numericUpDownXMX_ValueChanged);
            // 
            // textBox_cpu
            // 
            this.textBox_cpu.Location = new System.Drawing.Point(225, 11);
            this.textBox_cpu.Name = "textBox_cpu";
            this.textBox_cpu.Size = new System.Drawing.Size(42, 20);
            this.textBox_cpu.TabIndex = 0;
            this.textBox_cpu.Text = "cpu%";
            this.textBox_cpu.Visible = false;
            // 
            // button_checkcpumem
            // 
            this.button_checkcpumem.Image = global::Minecraft_Launcher.Properties.Resources.chart_up_color;
            this.button_checkcpumem.Location = new System.Drawing.Point(274, 30);
            this.button_checkcpumem.Name = "button_checkcpumem";
            this.button_checkcpumem.Size = new System.Drawing.Size(69, 23);
            this.button_checkcpumem.TabIndex = 2;
            this.button_checkcpumem.Text = "Update";
            this.button_checkcpumem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_checkcpumem.UseVisualStyleBackColor = true;
            this.button_checkcpumem.Click += new System.EventHandler(this.button_checkcpumem_Click);
            // 
            // label_freemem
            // 
            this.label_freemem.AutoSize = true;
            this.label_freemem.Location = new System.Drawing.Point(102, 35);
            this.label_freemem.Name = "label_freemem";
            this.label_freemem.Size = new System.Drawing.Size(104, 13);
            this.label_freemem.TabIndex = 3;
            this.label_freemem.Text = "Current free memory:";
            // 
            // label_xmx
            // 
            this.label_xmx.AutoSize = true;
            this.label_xmx.Location = new System.Drawing.Point(9, 35);
            this.label_xmx.Name = "label_xmx";
            this.label_xmx.Size = new System.Drawing.Size(33, 13);
            this.label_xmx.TabIndex = 4;
            this.label_xmx.Text = "-Xmx:";
            this.Tooltip.SetToolTip(this.label_xmx, "Recommended:\r\n1024m -few  mods, default texture pack, no shaders\r\n2048m +many mod" +
        "s, higher resource packs, shaders\r\n\r\nBy default the launcher automatically sets " +
        "this value.\r\n");
            // 
            // McReleases
            // 
            this.McReleases.Controls.Add(this.McReleaseUpdate_Button);
            this.McReleases.Controls.Add(this.ResizeHeaders);
            this.McReleases.Controls.Add(this.VersionDownloadbutton);
            this.McReleases.Controls.Add(this.VersionsGridView);
            this.McReleases.Location = new System.Drawing.Point(4, 22);
            this.McReleases.Name = "McReleases";
            this.McReleases.Size = new System.Drawing.Size(415, 259);
            this.McReleases.TabIndex = 2;
            this.McReleases.Text = "Minecraft Releases";
            this.McReleases.UseVisualStyleBackColor = true;
            // 
            // McReleaseUpdate_Button
            // 
            this.McReleaseUpdate_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.McReleaseUpdate_Button.Image = global::Minecraft_Launcher.Properties.Resources.world_add;
            this.McReleaseUpdate_Button.Location = new System.Drawing.Point(131, 231);
            this.McReleaseUpdate_Button.Name = "McReleaseUpdate_Button";
            this.McReleaseUpdate_Button.Size = new System.Drawing.Size(97, 23);
            this.McReleaseUpdate_Button.TabIndex = 16;
            this.McReleaseUpdate_Button.Text = "Update List?";
            this.McReleaseUpdate_Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.McReleaseUpdate_Button.UseVisualStyleBackColor = true;
            this.McReleaseUpdate_Button.Click += new System.EventHandler(this.McReleaseUpdate_Button_Click);
            // 
            // ResizeHeaders
            // 
            this.ResizeHeaders.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ResizeHeaders.Image = global::Minecraft_Launcher.Properties.Resources.tabbar;
            this.ResizeHeaders.Location = new System.Drawing.Point(8, 231);
            this.ResizeHeaders.Name = "ResizeHeaders";
            this.ResizeHeaders.Size = new System.Drawing.Size(117, 23);
            this.ResizeHeaders.TabIndex = 15;
            this.ResizeHeaders.Text = "Resize Headers";
            this.ResizeHeaders.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ResizeHeaders.UseVisualStyleBackColor = true;
            this.ResizeHeaders.Click += new System.EventHandler(this.ResizeHeaders_Click);
            // 
            // VersionDownloadbutton
            // 
            this.VersionDownloadbutton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.VersionDownloadbutton.Image = global::Minecraft_Launcher.Properties.Resources.inbox_download;
            this.VersionDownloadbutton.Location = new System.Drawing.Point(276, 231);
            this.VersionDownloadbutton.Name = "VersionDownloadbutton";
            this.VersionDownloadbutton.Size = new System.Drawing.Size(136, 23);
            this.VersionDownloadbutton.TabIndex = 14;
            this.VersionDownloadbutton.Text = "Download Selected";
            this.VersionDownloadbutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.VersionDownloadbutton.UseVisualStyleBackColor = true;
            this.VersionDownloadbutton.Click += new System.EventHandler(this.VersionDownloadbutton_Click);
            // 
            // VersionsGridView
            // 
            this.VersionsGridView.AllowUserToAddRows = false;
            this.VersionsGridView.AllowUserToDeleteRows = false;
            this.VersionsGridView.AllowUserToResizeRows = false;
            this.VersionsGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VersionsGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.VersionsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VersionsGridView.Location = new System.Drawing.Point(3, 5);
            this.VersionsGridView.MultiSelect = false;
            this.VersionsGridView.Name = "VersionsGridView";
            this.VersionsGridView.ReadOnly = true;
            this.VersionsGridView.RowHeadersVisible = false;
            this.VersionsGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.VersionsGridView.RowTemplate.Height = 21;
            this.VersionsGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.VersionsGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.VersionsGridView.ShowEditingIcon = false;
            this.VersionsGridView.Size = new System.Drawing.Size(409, 220);
            this.VersionsGridView.TabIndex = 13;
            this.VersionsGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.VersionsGridView_CellFormatting);
            this.VersionsGridView.SelectionChanged += new System.EventHandler(this.VersionsGridView_SelectionChanged);
            // 
            // button_SaveSettings
            // 
            this.button_SaveSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_SaveSettings.Image = global::Minecraft_Launcher.Properties.Resources.page_save;
            this.button_SaveSettings.Location = new System.Drawing.Point(356, 297);
            this.button_SaveSettings.Name = "button_SaveSettings";
            this.button_SaveSettings.Size = new System.Drawing.Size(72, 23);
            this.button_SaveSettings.TabIndex = 6;
            this.button_SaveSettings.Text = "Save";
            this.button_SaveSettings.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_SaveSettings.UseVisualStyleBackColor = true;
            this.button_SaveSettings.Click += new System.EventHandler(this.button_SettingsSave_Click);
            // 
            // tabPage_Updates
            // 
            this.tabPage_Updates.Controls.Add(this.button_checkupdate);
            this.tabPage_Updates.Controls.Add(this.DownloadExtract_button);
            this.tabPage_Updates.Controls.Add(this.groupBox2);
            this.tabPage_Updates.Controls.Add(this.Update_richTextBox);
            this.tabPage_Updates.Controls.Add(this.DownloadUpdate_button);
            this.tabPage_Updates.Controls.Add(this.radioButton_Minecraft172);
            this.tabPage_Updates.Controls.Add(this.label_tickmc172);
            this.tabPage_Updates.Controls.Add(this.button_Update);
            this.tabPage_Updates.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Updates.Name = "tabPage_Updates";
            this.tabPage_Updates.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Updates.Size = new System.Drawing.Size(431, 323);
            this.tabPage_Updates.TabIndex = 0;
            this.tabPage_Updates.Text = "Updates";
            this.tabPage_Updates.UseVisualStyleBackColor = true;
            // 
            // button_checkupdate
            // 
            this.button_checkupdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_checkupdate.Image = global::Minecraft_Launcher.Properties.Resources.world_add;
            this.button_checkupdate.Location = new System.Drawing.Point(23, 168);
            this.button_checkupdate.Name = "button_checkupdate";
            this.button_checkupdate.Size = new System.Drawing.Size(106, 23);
            this.button_checkupdate.TabIndex = 6;
            this.button_checkupdate.Text = "Check Updates";
            this.button_checkupdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_checkupdate.UseVisualStyleBackColor = true;
            this.button_checkupdate.Click += new System.EventHandler(this.button_checkupdate_Click);
            // 
            // DownloadExtract_button
            // 
            this.DownloadExtract_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DownloadExtract_button.Enabled = false;
            this.DownloadExtract_button.Location = new System.Drawing.Point(104, 292);
            this.DownloadExtract_button.Name = "DownloadExtract_button";
            this.DownloadExtract_button.Size = new System.Drawing.Size(75, 23);
            this.DownloadExtract_button.TabIndex = 5;
            this.DownloadExtract_button.Text = "Extract";
            this.DownloadExtract_button.UseVisualStyleBackColor = true;
            this.DownloadExtract_button.Click += new System.EventHandler(this.DownloadExtract_button_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label_tickjava64_java8);
            this.groupBox2.Controls.Add(this.label_tickjava32_java8);
            this.groupBox2.Controls.Add(this.radioButton_Java8_64);
            this.groupBox2.Controls.Add(this.radioButton_Java8_32);
            this.groupBox2.Controls.Add(this.label_tickjava64_java7);
            this.groupBox2.Controls.Add(this.label_tickjava32_java7);
            this.groupBox2.Controls.Add(this.radioButton_Java7_64);
            this.groupBox2.Controls.Add(this.radioButton_Java7_32);
            this.groupBox2.Location = new System.Drawing.Point(23, 197);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(390, 89);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Installed Packs";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(331, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "This section will be phased out next release, goto [Mod/Addons] tab.";
            // 
            // label_tickjava64_java8
            // 
            this.label_tickjava64_java8.AutoSize = true;
            this.label_tickjava64_java8.ForeColor = System.Drawing.Color.Red;
            this.label_tickjava64_java8.Location = new System.Drawing.Point(163, 69);
            this.label_tickjava64_java8.Name = "label_tickjava64_java8";
            this.label_tickjava64_java8.Size = new System.Drawing.Size(15, 13);
            this.label_tickjava64_java8.TabIndex = 11;
            this.label_tickjava64_java8.Text = " x";
            // 
            // label_tickjava32_java8
            // 
            this.label_tickjava32_java8.AutoSize = true;
            this.label_tickjava32_java8.ForeColor = System.Drawing.Color.Red;
            this.label_tickjava32_java8.Location = new System.Drawing.Point(163, 47);
            this.label_tickjava32_java8.Name = "label_tickjava32_java8";
            this.label_tickjava32_java8.Size = new System.Drawing.Size(15, 13);
            this.label_tickjava32_java8.TabIndex = 10;
            this.label_tickjava32_java8.Text = " x";
            // 
            // radioButton_Java8_64
            // 
            this.radioButton_Java8_64.AutoSize = true;
            this.radioButton_Java8_64.Enabled = false;
            this.radioButton_Java8_64.Location = new System.Drawing.Point(183, 67);
            this.radioButton_Java8_64.Name = "radioButton_Java8_64";
            this.radioButton_Java8_64.Size = new System.Drawing.Size(131, 17);
            this.radioButton_Java8_64.TabIndex = 9;
            this.radioButton_Java8_64.TabStop = true;
            this.radioButton_Java8_64.Text = "Portable Java 8 (64bit)";
            this.radioButton_Java8_64.UseVisualStyleBackColor = true;
            this.radioButton_Java8_64.CheckedChanged += new System.EventHandler(this.radioButton_Java8_64_CheckedChanged);
            // 
            // radioButton_Java8_32
            // 
            this.radioButton_Java8_32.AutoSize = true;
            this.radioButton_Java8_32.Enabled = false;
            this.radioButton_Java8_32.Location = new System.Drawing.Point(183, 44);
            this.radioButton_Java8_32.Name = "radioButton_Java8_32";
            this.radioButton_Java8_32.Size = new System.Drawing.Size(131, 17);
            this.radioButton_Java8_32.TabIndex = 8;
            this.radioButton_Java8_32.TabStop = true;
            this.radioButton_Java8_32.Text = "Portable Java 8 (32bit)";
            this.radioButton_Java8_32.UseVisualStyleBackColor = true;
            this.radioButton_Java8_32.CheckedChanged += new System.EventHandler(this.radioButton_Java8_32_CheckedChanged);
            // 
            // label_tickjava64_java7
            // 
            this.label_tickjava64_java7.AutoSize = true;
            this.label_tickjava64_java7.ForeColor = System.Drawing.Color.Red;
            this.label_tickjava64_java7.Location = new System.Drawing.Point(5, 68);
            this.label_tickjava64_java7.Name = "label_tickjava64_java7";
            this.label_tickjava64_java7.Size = new System.Drawing.Size(15, 13);
            this.label_tickjava64_java7.TabIndex = 5;
            this.label_tickjava64_java7.Text = " x";
            // 
            // label_tickjava32_java7
            // 
            this.label_tickjava32_java7.AutoSize = true;
            this.label_tickjava32_java7.ForeColor = System.Drawing.Color.Red;
            this.label_tickjava32_java7.Location = new System.Drawing.Point(5, 46);
            this.label_tickjava32_java7.Name = "label_tickjava32_java7";
            this.label_tickjava32_java7.Size = new System.Drawing.Size(15, 13);
            this.label_tickjava32_java7.TabIndex = 4;
            this.label_tickjava32_java7.Text = " x";
            // 
            // radioButton_Java7_64
            // 
            this.radioButton_Java7_64.AutoSize = true;
            this.radioButton_Java7_64.Enabled = false;
            this.radioButton_Java7_64.Location = new System.Drawing.Point(25, 66);
            this.radioButton_Java7_64.Name = "radioButton_Java7_64";
            this.radioButton_Java7_64.Size = new System.Drawing.Size(131, 17);
            this.radioButton_Java7_64.TabIndex = 2;
            this.radioButton_Java7_64.TabStop = true;
            this.radioButton_Java7_64.Text = "Portable Java 7 (64bit)";
            this.radioButton_Java7_64.UseVisualStyleBackColor = true;
            this.radioButton_Java7_64.CheckedChanged += new System.EventHandler(this.radioButton_Java64_CheckedChanged);
            // 
            // radioButton_Java7_32
            // 
            this.radioButton_Java7_32.AutoSize = true;
            this.radioButton_Java7_32.Enabled = false;
            this.radioButton_Java7_32.Location = new System.Drawing.Point(25, 43);
            this.radioButton_Java7_32.Name = "radioButton_Java7_32";
            this.radioButton_Java7_32.Size = new System.Drawing.Size(131, 17);
            this.radioButton_Java7_32.TabIndex = 1;
            this.radioButton_Java7_32.TabStop = true;
            this.radioButton_Java7_32.Text = "Portable Java 7 (32bit)";
            this.radioButton_Java7_32.UseVisualStyleBackColor = true;
            this.radioButton_Java7_32.CheckedChanged += new System.EventHandler(this.radioButton_Java32_CheckedChanged);
            // 
            // Update_richTextBox
            // 
            this.Update_richTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Update_richTextBox.Location = new System.Drawing.Point(23, 17);
            this.Update_richTextBox.Name = "Update_richTextBox";
            this.Update_richTextBox.ReadOnly = true;
            this.Update_richTextBox.Size = new System.Drawing.Size(390, 145);
            this.Update_richTextBox.TabIndex = 1;
            this.Update_richTextBox.Text = resources.GetString("Update_richTextBox.Text");
            // 
            // DownloadUpdate_button
            // 
            this.DownloadUpdate_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DownloadUpdate_button.Enabled = false;
            this.DownloadUpdate_button.Location = new System.Drawing.Point(23, 292);
            this.DownloadUpdate_button.Name = "DownloadUpdate_button";
            this.DownloadUpdate_button.Size = new System.Drawing.Size(75, 23);
            this.DownloadUpdate_button.TabIndex = 3;
            this.DownloadUpdate_button.Text = "Download";
            this.DownloadUpdate_button.UseVisualStyleBackColor = true;
            this.DownloadUpdate_button.Click += new System.EventHandler(this.DownloadUpdate_button_Click);
            // 
            // radioButton_Minecraft172
            // 
            this.radioButton_Minecraft172.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_Minecraft172.AutoSize = true;
            this.radioButton_Minecraft172.Checked = true;
            this.radioButton_Minecraft172.Location = new System.Drawing.Point(206, 292);
            this.radioButton_Minecraft172.Name = "radioButton_Minecraft172";
            this.radioButton_Minecraft172.Size = new System.Drawing.Size(96, 17);
            this.radioButton_Minecraft172.TabIndex = 0;
            this.radioButton_Minecraft172.TabStop = true;
            this.radioButton_Minecraft172.Text = "Minecraft 1.7.2";
            this.radioButton_Minecraft172.UseVisualStyleBackColor = true;
            this.radioButton_Minecraft172.Visible = false;
            this.radioButton_Minecraft172.CheckedChanged += new System.EventHandler(this.radioButton_Minecraft172_CheckedChanged);
            // 
            // label_tickmc172
            // 
            this.label_tickmc172.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_tickmc172.AutoSize = true;
            this.label_tickmc172.ForeColor = System.Drawing.Color.LimeGreen;
            this.label_tickmc172.Location = new System.Drawing.Point(186, 295);
            this.label_tickmc172.Name = "label_tickmc172";
            this.label_tickmc172.Size = new System.Drawing.Size(19, 13);
            this.label_tickmc172.TabIndex = 3;
            this.label_tickmc172.Text = "✓";
            this.label_tickmc172.Visible = false;
            // 
            // button_Update
            // 
            this.button_Update.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_Update.Image = global::Minecraft_Launcher.Properties.Resources.world_go;
            this.button_Update.Location = new System.Drawing.Point(338, 168);
            this.button_Update.Name = "button_Update";
            this.button_Update.Size = new System.Drawing.Size(75, 23);
            this.button_Update.TabIndex = 2;
            this.button_Update.Text = "Update";
            this.button_Update.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_Update.UseVisualStyleBackColor = true;
            this.button_Update.Visible = false;
            this.button_Update.Click += new System.EventHandler(this.button_Update_Click);
            // 
            // tabPage_ModPacks
            // 
            this.tabPage_ModPacks.Controls.Add(this.tabControl_Mods);
            this.tabPage_ModPacks.Location = new System.Drawing.Point(4, 22);
            this.tabPage_ModPacks.Name = "tabPage_ModPacks";
            this.tabPage_ModPacks.Size = new System.Drawing.Size(431, 323);
            this.tabPage_ModPacks.TabIndex = 2;
            this.tabPage_ModPacks.Text = "Mod/Addons";
            this.tabPage_ModPacks.UseVisualStyleBackColor = true;
            // 
            // tabControl_Mods
            // 
            this.tabControl_Mods.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl_Mods.Controls.Add(this.tabPage__modpacks);
            this.tabControl_Mods.Controls.Add(this.tabPage_modlists);
            this.tabControl_Mods.Location = new System.Drawing.Point(7, 7);
            this.tabControl_Mods.Name = "tabControl_Mods";
            this.tabControl_Mods.SelectedIndex = 0;
            this.tabControl_Mods.Size = new System.Drawing.Size(423, 317);
            this.tabControl_Mods.TabIndex = 21;
            this.tabControl_Mods.SelectedIndexChanged += new System.EventHandler(this.tabControl_Mods_SelectedIndexChanged);
            // 
            // tabPage__modpacks
            // 
            this.tabPage__modpacks.Controls.Add(this.button_ModPackExtract);
            this.tabPage__modpacks.Controls.Add(this.ModpacksGridView);
            this.tabPage__modpacks.Controls.Add(this.button_ModpackDownload);
            this.tabPage__modpacks.Controls.Add(this.label_ModInfo);
            this.tabPage__modpacks.Controls.Add(this.button_updateMods);
            this.tabPage__modpacks.Controls.Add(this.richTextBox_modpacks);
            this.tabPage__modpacks.Location = new System.Drawing.Point(4, 22);
            this.tabPage__modpacks.Name = "tabPage__modpacks";
            this.tabPage__modpacks.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage__modpacks.Size = new System.Drawing.Size(415, 291);
            this.tabPage__modpacks.TabIndex = 0;
            this.tabPage__modpacks.Text = "Mod Packs";
            this.tabPage__modpacks.UseVisualStyleBackColor = true;
            // 
            // button_ModPackExtract
            // 
            this.button_ModPackExtract.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_ModPackExtract.Enabled = false;
            this.button_ModPackExtract.Image = global::Minecraft_Launcher.Properties.Resources.installer_box;
            this.button_ModPackExtract.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_ModPackExtract.Location = new System.Drawing.Point(189, 262);
            this.button_ModPackExtract.Name = "button_ModPackExtract";
            this.button_ModPackExtract.Size = new System.Drawing.Size(99, 23);
            this.button_ModPackExtract.TabIndex = 21;
            this.button_ModPackExtract.Text = "Install Again?";
            this.button_ModPackExtract.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button_ModPackExtract.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_ModPackExtract.UseVisualStyleBackColor = true;
            this.button_ModPackExtract.Click += new System.EventHandler(this.button_ModPackExtract_Click);
            // 
            // ModpacksGridView
            // 
            this.ModpacksGridView.AllowUserToAddRows = false;
            this.ModpacksGridView.AllowUserToDeleteRows = false;
            this.ModpacksGridView.AllowUserToResizeRows = false;
            this.ModpacksGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ModpacksGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.ModpacksGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ModpacksGridView.Location = new System.Drawing.Point(6, 6);
            this.ModpacksGridView.MultiSelect = false;
            this.ModpacksGridView.Name = "ModpacksGridView";
            this.ModpacksGridView.ReadOnly = true;
            this.ModpacksGridView.RowHeadersVisible = false;
            this.ModpacksGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.ModpacksGridView.RowTemplate.Height = 21;
            this.ModpacksGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ModpacksGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ModpacksGridView.ShowEditingIcon = false;
            this.ModpacksGridView.Size = new System.Drawing.Size(403, 108);
            this.ModpacksGridView.TabIndex = 10;
            this.ModpacksGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ModpacksGridView_CellFormatting);
            this.ModpacksGridView.SelectionChanged += new System.EventHandler(this.ModpacksGridView_SelectionChanged);
            this.ModpacksGridView.Sorted += new System.EventHandler(this.ModpacksGridView_Sorted);
            this.ModpacksGridView.MouseEnter += new System.EventHandler(this.ModpacksGridView_MouseEnter);
            // 
            // button_ModpackDownload
            // 
            this.button_ModpackDownload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_ModpackDownload.Image = global::Minecraft_Launcher.Properties.Resources.inbox_download;
            this.button_ModpackDownload.Location = new System.Drawing.Point(294, 262);
            this.button_ModpackDownload.Name = "button_ModpackDownload";
            this.button_ModpackDownload.Size = new System.Drawing.Size(115, 23);
            this.button_ModpackDownload.TabIndex = 17;
            this.button_ModpackDownload.Text = "Download Again?";
            this.button_ModpackDownload.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_ModpackDownload.UseVisualStyleBackColor = true;
            this.button_ModpackDownload.Click += new System.EventHandler(this.button_ModpackDownload_Click);
            // 
            // label_ModInfo
            // 
            this.label_ModInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_ModInfo.AutoSize = true;
            this.label_ModInfo.Location = new System.Drawing.Point(6, 117);
            this.label_ModInfo.Name = "label_ModInfo";
            this.label_ModInfo.Size = new System.Drawing.Size(119, 13);
            this.label_ModInfo.TabIndex = 20;
            this.label_ModInfo.Text = "Selected modpack info:";
            // 
            // button_updateMods
            // 
            this.button_updateMods.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_updateMods.Image = global::Minecraft_Launcher.Properties.Resources.world_add;
            this.button_updateMods.Location = new System.Drawing.Point(6, 262);
            this.button_updateMods.Name = "button_updateMods";
            this.button_updateMods.Size = new System.Drawing.Size(130, 23);
            this.button_updateMods.TabIndex = 18;
            this.button_updateMods.Text = "Check for updates?";
            this.button_updateMods.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_updateMods.UseVisualStyleBackColor = true;
            this.button_updateMods.Click += new System.EventHandler(this.button_updateMods_Click);
            // 
            // richTextBox_modpacks
            // 
            this.richTextBox_modpacks.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_modpacks.Location = new System.Drawing.Point(6, 133);
            this.richTextBox_modpacks.Name = "richTextBox_modpacks";
            this.richTextBox_modpacks.Size = new System.Drawing.Size(403, 126);
            this.richTextBox_modpacks.TabIndex = 19;
            this.richTextBox_modpacks.Text = "";
            // 
            // tabPage_modlists
            // 
            this.tabPage_modlists.Controls.Add(this.modlistDl_button);
            this.tabPage_modlists.Controls.Add(this.modListChanges_linkLabel);
            this.tabPage_modlists.Controls.Add(this.label_totalmods);
            this.tabPage_modlists.Controls.Add(this.richTextBox_Modlists);
            this.tabPage_modlists.Controls.Add(this.dataGridView_Modlists);
            this.tabPage_modlists.Controls.Add(this.comboBox_modsVersion);
            this.tabPage_modlists.Controls.Add(this.button_ModlistRefresh);
            this.tabPage_modlists.Location = new System.Drawing.Point(4, 22);
            this.tabPage_modlists.Name = "tabPage_modlists";
            this.tabPage_modlists.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_modlists.Size = new System.Drawing.Size(415, 291);
            this.tabPage_modlists.TabIndex = 1;
            this.tabPage_modlists.Text = "List of mods";
            this.tabPage_modlists.UseVisualStyleBackColor = true;
            // 
            // modlistDl_button
            // 
            this.modlistDl_button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.modlistDl_button.Enabled = false;
            this.modlistDl_button.Image = global::Minecraft_Launcher.Properties.Resources.world_go;
            this.modlistDl_button.Location = new System.Drawing.Point(334, 262);
            this.modlistDl_button.Name = "modlistDl_button";
            this.modlistDl_button.Size = new System.Drawing.Size(75, 23);
            this.modlistDl_button.TabIndex = 7;
            this.modlistDl_button.Text = "Get List";
            this.modlistDl_button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.modlistDl_button.UseVisualStyleBackColor = true;
            this.modlistDl_button.Click += new System.EventHandler(this.modlistDl_button_Click);
            // 
            // modListChanges_linkLabel
            // 
            this.modListChanges_linkLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.modListChanges_linkLabel.AutoSize = true;
            this.modListChanges_linkLabel.Location = new System.Drawing.Point(252, 117);
            this.modListChanges_linkLabel.Name = "modListChanges_linkLabel";
            this.modListChanges_linkLabel.Size = new System.Drawing.Size(157, 13);
            this.modListChanges_linkLabel.TabIndex = 6;
            this.modListChanges_linkLabel.TabStop = true;
            this.modListChanges_linkLabel.Text = "http://modlist.mcf.li/changelog/";
            this.modListChanges_linkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.modListChanges_linkLabel_LinkClicked);
            // 
            // label_totalmods
            // 
            this.label_totalmods.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_totalmods.AutoSize = true;
            this.label_totalmods.Location = new System.Drawing.Point(6, 117);
            this.label_totalmods.Name = "label_totalmods";
            this.label_totalmods.Size = new System.Drawing.Size(89, 13);
            this.label_totalmods.TabIndex = 5;
            this.label_totalmods.Text = "Total mods listed:";
            // 
            // richTextBox_Modlists
            // 
            this.richTextBox_Modlists.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_Modlists.Location = new System.Drawing.Point(6, 133);
            this.richTextBox_Modlists.Name = "richTextBox_Modlists";
            this.richTextBox_Modlists.Size = new System.Drawing.Size(403, 127);
            this.richTextBox_Modlists.TabIndex = 3;
            this.richTextBox_Modlists.Text = "";
            this.richTextBox_Modlists.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.richTextBox_Modlists_LinkClicked);
            // 
            // dataGridView_Modlists
            // 
            this.dataGridView_Modlists.AllowUserToAddRows = false;
            this.dataGridView_Modlists.AllowUserToDeleteRows = false;
            this.dataGridView_Modlists.AllowUserToResizeRows = false;
            this.dataGridView_Modlists.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_Modlists.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Modlists.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_Modlists.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Modlists.Location = new System.Drawing.Point(6, 6);
            this.dataGridView_Modlists.MultiSelect = false;
            this.dataGridView_Modlists.Name = "dataGridView_Modlists";
            this.dataGridView_Modlists.ReadOnly = true;
            this.dataGridView_Modlists.RowHeadersVisible = false;
            this.dataGridView_Modlists.RowTemplate.Height = 21;
            this.dataGridView_Modlists.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView_Modlists.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_Modlists.ShowEditingIcon = false;
            this.dataGridView_Modlists.Size = new System.Drawing.Size(403, 108);
            this.dataGridView_Modlists.TabIndex = 2;
            this.dataGridView_Modlists.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_Modlists_CellFormatting);
            this.dataGridView_Modlists.SelectionChanged += new System.EventHandler(this.dataGridView_Modlists_SelectionChanged);
            // 
            // comboBox_modsVersion
            // 
            this.comboBox_modsVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_modsVersion.FormattingEnabled = true;
            this.comboBox_modsVersion.Location = new System.Drawing.Point(224, 262);
            this.comboBox_modsVersion.Name = "comboBox_modsVersion";
            this.comboBox_modsVersion.Size = new System.Drawing.Size(104, 21);
            this.comboBox_modsVersion.TabIndex = 1;
            this.comboBox_modsVersion.SelectedIndexChanged += new System.EventHandler(this.comboBox_modsVersion_SelectedIndexChanged);
            // 
            // button_ModlistRefresh
            // 
            this.button_ModlistRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_ModlistRefresh.Image = global::Minecraft_Launcher.Properties.Resources.world_add;
            this.button_ModlistRefresh.Location = new System.Drawing.Point(6, 262);
            this.button_ModlistRefresh.Name = "button_ModlistRefresh";
            this.button_ModlistRefresh.Size = new System.Drawing.Size(130, 23);
            this.button_ModlistRefresh.TabIndex = 4;
            this.button_ModlistRefresh.Text = "Check for updates?";
            this.button_ModlistRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_ModlistRefresh.UseVisualStyleBackColor = true;
            this.button_ModlistRefresh.Click += new System.EventHandler(this.button_ModlistRefresh_Click);
            // 
            // tabPage_About
            // 
            this.tabPage_About.Controls.Add(this.checkBoxAutoScroll);
            this.tabPage_About.Controls.Add(this.Log_richTextBox);
            this.tabPage_About.Controls.Add(this.button2);
            this.tabPage_About.Controls.Add(this.button_CopyLog);
            this.tabPage_About.Controls.Add(this.dataGridView_modpacks);
            this.tabPage_About.Controls.Add(this.linkLabel_urlkraftzone);
            this.tabPage_About.Controls.Add(this.label_About);
            this.tabPage_About.Controls.Add(this.pictureBox1);
            this.tabPage_About.Location = new System.Drawing.Point(4, 22);
            this.tabPage_About.Name = "tabPage_About";
            this.tabPage_About.Size = new System.Drawing.Size(431, 323);
            this.tabPage_About.TabIndex = 3;
            this.tabPage_About.Text = "About";
            this.tabPage_About.UseVisualStyleBackColor = true;
            // 
            // checkBoxAutoScroll
            // 
            this.checkBoxAutoScroll.AutoSize = true;
            this.checkBoxAutoScroll.Checked = true;
            this.checkBoxAutoScroll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAutoScroll.Location = new System.Drawing.Point(351, 127);
            this.checkBoxAutoScroll.Name = "checkBoxAutoScroll";
            this.checkBoxAutoScroll.Size = new System.Drawing.Size(74, 17);
            this.checkBoxAutoScroll.TabIndex = 11;
            this.checkBoxAutoScroll.Text = "AutoScroll";
            this.checkBoxAutoScroll.UseVisualStyleBackColor = true;
            // 
            // Log_richTextBox
            // 
            this.Log_richTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Log_richTextBox.Location = new System.Drawing.Point(8, 179);
            this.Log_richTextBox.Name = "Log_richTextBox";
            this.Log_richTextBox.ReadOnly = true;
            this.Log_richTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.Log_richTextBox.Size = new System.Drawing.Size(417, 136);
            this.Log_richTextBox.TabIndex = 10;
            this.Log_richTextBox.Text = "";
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(269, 150);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Detach log";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button_CopyLog
            // 
            this.button_CopyLog.Location = new System.Drawing.Point(350, 150);
            this.button_CopyLog.Name = "button_CopyLog";
            this.button_CopyLog.Size = new System.Drawing.Size(75, 23);
            this.button_CopyLog.TabIndex = 4;
            this.button_CopyLog.Text = "Copy All";
            this.button_CopyLog.UseVisualStyleBackColor = true;
            this.button_CopyLog.Click += new System.EventHandler(this.button_CopyLog_Click);
            // 
            // dataGridView_modpacks
            // 
            this.dataGridView_modpacks.AllowUserToAddRows = false;
            this.dataGridView_modpacks.AllowUserToDeleteRows = false;
            this.dataGridView_modpacks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_modpacks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DownloadFile});
            this.dataGridView_modpacks.Location = new System.Drawing.Point(480, 45);
            this.dataGridView_modpacks.Name = "dataGridView_modpacks";
            this.dataGridView_modpacks.ReadOnly = true;
            this.dataGridView_modpacks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_modpacks.ShowEditingIcon = false;
            this.dataGridView_modpacks.Size = new System.Drawing.Size(152, 49);
            this.dataGridView_modpacks.TabIndex = 9;
            this.dataGridView_modpacks.Visible = false;
            // 
            // DownloadFile
            // 
            this.DownloadFile.HeaderText = "Get?";
            this.DownloadFile.Name = "DownloadFile";
            this.DownloadFile.ReadOnly = true;
            this.DownloadFile.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DownloadFile.Width = 50;
            // 
            // linkLabel_urlkraftzone
            // 
            this.linkLabel_urlkraftzone.AutoSize = true;
            this.linkLabel_urlkraftzone.Location = new System.Drawing.Point(44, 155);
            this.linkLabel_urlkraftzone.Name = "linkLabel_urlkraftzone";
            this.linkLabel_urlkraftzone.Size = new System.Drawing.Size(160, 13);
            this.linkLabel_urlkraftzone.TabIndex = 2;
            this.linkLabel_urlkraftzone.TabStop = true;
            this.linkLabel_urlkraftzone.Text = "http://kraftzone.net/mclauncher";
            this.linkLabel_urlkraftzone.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_urlkraftzone_LinkClicked);
            // 
            // label_About
            // 
            this.label_About.AutoSize = true;
            this.label_About.Location = new System.Drawing.Point(16, 103);
            this.label_About.Name = "label_About";
            this.label_About.Size = new System.Drawing.Size(319, 65);
            this.label_About.TabIndex = 0;
            this.label_About.Text = "If you have any questions, feature requests, modpack suggestions\r\n\r\nOr just bug r" +
    "eports and feedback in general...\r\n\r\nVisit:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Minecraft_Launcher.Properties.Resources.logonew;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(-24, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(487, 91);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // comboBox_Profile
            // 
            this.comboBox_Profile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_Profile.FormattingEnabled = true;
            this.comboBox_Profile.Location = new System.Drawing.Point(237, 365);
            this.comboBox_Profile.Name = "comboBox_Profile";
            this.comboBox_Profile.Size = new System.Drawing.Size(104, 21);
            this.comboBox_Profile.TabIndex = 25;
            this.comboBox_Profile.SelectedIndexChanged += new System.EventHandler(this.comboBox_Profile_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label4.Location = new System.Drawing.Point(218, 368);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "<";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label5.Location = new System.Drawing.Point(346, 369);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = ">";
            // 
            // PremiumToolTip
            // 
            this.PremiumToolTip.AutomaticDelay = 1;
            this.PremiumToolTip.AutoPopDelay = 500000;
            this.PremiumToolTip.InitialDelay = 1;
            this.PremiumToolTip.ReshowDelay = 1;
            this.PremiumToolTip.ShowAlways = true;
            this.PremiumToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.PremiumToolTip.ToolTipTitle = "Mojang Account";
            // 
            // AutoConnectTooltip
            // 
            this.AutoConnectTooltip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.AutoConnectTooltip.ToolTipTitle = "Auto Connect";
            // 
            // Tooltip
            // 
            this.Tooltip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // notifyIcon_tray
            // 
            this.notifyIcon_tray.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon_tray.BalloonTipText = "Click to open again.";
            this.notifyIcon_tray.BalloonTipTitle = "KZ Minecraft Launcher";
            this.notifyIcon_tray.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon_tray.Icon")));
            this.notifyIcon_tray.Text = "KZ Minecraft Launcher";
            this.notifyIcon_tray.Visible = true;
            this.notifyIcon_tray.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_tray_MouseDoubleClick);
            // 
            // openVersionFolder_toolTip
            // 
            this.openVersionFolder_toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.openVersionFolder_toolTip.ToolTipTitle = "Version Folder";
            // 
            // openportablefolder_toolTip
            // 
            this.openportablefolder_toolTip.AutomaticDelay = 200;
            this.openportablefolder_toolTip.AutoPopDelay = 5000;
            this.openportablefolder_toolTip.InitialDelay = 100;
            this.openportablefolder_toolTip.ReshowDelay = 40;
            this.openportablefolder_toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.openportablefolder_toolTip.ToolTipTitle = "KzLauncher/Minecraft directory";
            // 
            // openportablefolder_Button
            // 
            this.openportablefolder_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.openportablefolder_Button.Image = global::Minecraft_Launcher.Properties.Resources.folder_wrench;
            this.openportablefolder_Button.Location = new System.Drawing.Point(5, 364);
            this.openportablefolder_Button.Name = "openportablefolder_Button";
            this.openportablefolder_Button.Size = new System.Drawing.Size(95, 23);
            this.openportablefolder_Button.TabIndex = 3;
            this.openportablefolder_Button.Text = "Open Folder";
            this.openportablefolder_Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.openportablefolder_toolTip.SetToolTip(this.openportablefolder_Button, "This opens the base directory from where Kz Launcher is installed.");
            this.openportablefolder_Button.UseVisualStyleBackColor = true;
            this.openportablefolder_Button.Click += new System.EventHandler(this.button_openportablefolder_Click);
            // 
            // button_Remove
            // 
            this.button_Remove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Remove.Enabled = false;
            this.button_Remove.Image = global::Minecraft_Launcher.Properties.Resources.application_form_delete;
            this.button_Remove.Location = new System.Drawing.Point(109, 364);
            this.button_Remove.Name = "button_Remove";
            this.button_Remove.Size = new System.Drawing.Size(104, 23);
            this.button_Remove.TabIndex = 28;
            this.button_Remove.Text = "Remove Profile";
            this.button_Remove.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_Remove.UseVisualStyleBackColor = true;
            this.button_Remove.Click += new System.EventHandler(this.button_Remove_Click);
            // 
            // button_Launch
            // 
            this.button_Launch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Launch.Image = global::Minecraft_Launcher.Properties.Resources.arrow_right;
            this.button_Launch.Location = new System.Drawing.Point(364, 364);
            this.button_Launch.Name = "button_Launch";
            this.button_Launch.Size = new System.Drawing.Size(75, 23);
            this.button_Launch.TabIndex = 3;
            this.button_Launch.Text = "Launch";
            this.button_Launch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_Launch.UseVisualStyleBackColor = true;
            this.button_Launch.Click += new System.EventHandler(this.button_Launch_Click);
            // 
            // Folder_contextMenu
            // 
            this.Folder_contextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem_gameDir,
            this.menuItem_mods,
            this.menuItem_resources,
            this.menuItem_shaderpacks,
            this.menuItem_version});
            // 
            // menuItem_gameDir
            // 
            this.menuItem_gameDir.Enabled = false;
            this.menuItem_gameDir.Index = 0;
            this.menuItem_gameDir.Text = "GameDir";
            this.menuItem_gameDir.Click += new System.EventHandler(this.menuItem_gameDir_Click);
            // 
            // menuItem_mods
            // 
            this.menuItem_mods.Enabled = false;
            this.menuItem_mods.Index = 1;
            this.menuItem_mods.Text = " -Mods";
            this.menuItem_mods.Click += new System.EventHandler(this.menuItem_mods_Click);
            // 
            // menuItem_resources
            // 
            this.menuItem_resources.Enabled = false;
            this.menuItem_resources.Index = 2;
            this.menuItem_resources.Text = " -ResourcePacks";
            this.menuItem_resources.Click += new System.EventHandler(this.menuItem_resources_Click);
            // 
            // menuItem_shaderpacks
            // 
            this.menuItem_shaderpacks.Enabled = false;
            this.menuItem_shaderpacks.Index = 3;
            this.menuItem_shaderpacks.Text = " -ShaderPacks";
            this.menuItem_shaderpacks.Click += new System.EventHandler(this.menuItem_shaderpacks_Click);
            // 
            // menuItem_version
            // 
            this.menuItem_version.Enabled = false;
            this.menuItem_version.Index = 4;
            this.menuItem_version.Text = "Version";
            this.menuItem_version.Click += new System.EventHandler(this.menuItem_version_Click);
            // 
            // Main
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 394);
            this.Controls.Add(this.About);
            this.Controls.Add(this.openportablefolder_Button);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label_mcversionnumber);
            this.Controls.Add(this.button_Remove);
            this.Controls.Add(this.comboBox_Profile);
            this.Controls.Add(this.tabControl_Main);
            this.Controls.Add(this.button_Launch);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(464, 432);
            this.Name = "Main";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Minecraft Launcher";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Main_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Main_DragEnter);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Premium_groupBox.ResumeLayout(false);
            this.Premium_groupBox.PerformLayout();
            this.group_JavaVersion.ResumeLayout(false);
            this.group_JavaVersion.PerformLayout();
            this.groupBox_AutoConnect.ResumeLayout(false);
            this.groupBox_AutoConnect.PerformLayout();
            this.groupBox_ScreenMode.ResumeLayout(false);
            this.groupBox_ScreenMode.PerformLayout();
            this.tabControl_Main.ResumeLayout(false);
            this.tabPage_Main.ResumeLayout(false);
            this.tabPage_Main.PerformLayout();
            this.tabPage_Settings.ResumeLayout(false);
            this.tabControl_Settings.ResumeLayout(false);
            this.LauncherOptions.ResumeLayout(false);
            this.LauncherOptions.PerformLayout();
            this.groupBox_AfterLaunch.ResumeLayout(false);
            this.groupBox_AfterLaunch.PerformLayout();
            this.JavaOptions.ResumeLayout(false);
            this.groupBox_JavaSettings.ResumeLayout(false);
            this.groupBox_JavaSettings.PerformLayout();
            this.groupBox_Customjvm.ResumeLayout(false);
            this.groupBox_Customjvm.PerformLayout();
            this.groupBoxXMX.ResumeLayout(false);
            this.groupBoxXMX.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownXMX)).EndInit();
            this.McReleases.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VersionsGridView)).EndInit();
            this.tabPage_Updates.ResumeLayout(false);
            this.tabPage_Updates.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage_ModPacks.ResumeLayout(false);
            this.tabControl_Mods.ResumeLayout(false);
            this.tabPage__modpacks.ResumeLayout(false);
            this.tabPage__modpacks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ModpacksGridView)).EndInit();
            this.tabPage_modlists.ResumeLayout(false);
            this.tabPage_modlists.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Modlists)).EndInit();
            this.tabPage_About.ResumeLayout(false);
            this.tabPage_About.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_modpacks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Username;
        private System.Windows.Forms.TextBox textBox_Username;
        private System.Windows.Forms.Button button_Launch;
        private System.Windows.Forms.Label label_Error;
        private System.Windows.Forms.ComboBox comboBox_MCVersion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton_ScreenDefault;
        private System.Windows.Forms.GroupBox groupBox_AutoConnect;
        private System.Windows.Forms.TextBox textBox_CustomServer;
        private System.Windows.Forms.RadioButton radioButton_Kraftzone;
        private System.Windows.Forms.RadioButton radioButton_NoAuto;
        private System.Windows.Forms.GroupBox groupBox_ScreenMode;
        private System.Windows.Forms.RadioButton radioButton_Fullscreen;
        private System.Windows.Forms.RadioButton radioButton_Windowed720p;
        private System.Windows.Forms.Label About;
        private System.Windows.Forms.RadioButton radioButton_CustomServer;
        private System.Windows.Forms.ComboBox comboBox_JavaVersion;
        private System.Windows.Forms.GroupBox group_JavaVersion;
        private System.Windows.Forms.RadioButton radioButton_JavaPortable;
        private System.Windows.Forms.RadioButton radioButton_JavaDefault;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.Label label_mcversionnumber;
        private System.Windows.Forms.TabControl tabControl_Main;
        private System.Windows.Forms.TabPage tabPage_Main;
        private System.Windows.Forms.TabPage tabPage_ModPacks;
        private System.Windows.Forms.TabPage tabPage_About;
        private System.Windows.Forms.Button button_Update;
        private System.Windows.Forms.RichTextBox Update_richTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_ProfileName;
        private System.Windows.Forms.ComboBox comboBox_Profile;
        private System.Windows.Forms.Label labelLaunch;
        private System.Windows.Forms.Button button_Remove;
        private System.Windows.Forms.Button button_Addprofile;
        private System.Windows.Forms.ListView listView_Profiles;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label_About;
        private System.Windows.Forms.LinkLabel linkLabel_urlkraftzone;
        private System.Windows.Forms.TabPage tabPage_Updates;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox Premium_groupBox;
        private System.Windows.Forms.TextBox Premium_pass_textBox;
        private System.Windows.Forms.TextBox Premium_user_textBox;
        private System.Windows.Forms.ToolTip PremiumToolTip;
        private System.Windows.Forms.CheckBox Auth_checkBox;
        private System.Windows.Forms.Button ShowPass_button;
        private System.Windows.Forms.Button DownloadExtract_button;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton_Java7_64;
        private System.Windows.Forms.RadioButton radioButton_Java7_32;
        private System.Windows.Forms.RadioButton radioButton_Minecraft172;
        private System.Windows.Forms.Button DownloadUpdate_button;
        private System.Windows.Forms.Button button_checkupdate;
        private System.Windows.Forms.Label label_tickjava64_java7;
        private System.Windows.Forms.Label label_tickjava32_java7;
        private System.Windows.Forms.Label label_tickmc172;
        private System.Windows.Forms.Button openportablefolder_Button;
        private System.Windows.Forms.ToolTip AutoConnectTooltip;
        private System.Windows.Forms.TabPage tabPage_Settings;
        private System.Windows.Forms.GroupBox groupBox_JavaSettings;
        private System.Windows.Forms.Button button_checkcpumem;
        private System.Windows.Forms.TextBox textBox_mem;
        private System.Windows.Forms.TextBox textBox_cpu;
        private System.Windows.Forms.CheckBox checkBox_debug;
        private System.Windows.Forms.Label label_xmx;
        private System.Windows.Forms.Label label_freemem;
        private System.Windows.Forms.ToolTip Tooltip;
        private System.Windows.Forms.Label label_FinalJAVA;
        private System.Windows.Forms.NumericUpDown numericUpDownXMX;
        private System.Windows.Forms.Label label_MaximumSizeXMX;
        private System.Windows.Forms.GroupBox groupBoxXMX;
        private System.Windows.Forms.CheckBox checkBoxEnableXMX;
        private System.Windows.Forms.CheckBox checkBoxCustomJAVA;
        private System.Windows.Forms.GroupBox groupBox_Customjvm;
        private System.Windows.Forms.TextBox textBox_JavacustomArgs;
        private System.Windows.Forms.Label label_JavaCustom;
        private System.Windows.Forms.RichTextBox richTextBox_FinalJavaArgs;
        private System.Windows.Forms.Button button_SaveSettings;
        private System.Windows.Forms.Button button_JavaOptions;
        private System.Windows.Forms.TabControl tabControl_Settings;
        private System.Windows.Forms.TabPage LauncherOptions;
        private System.Windows.Forms.TabPage JavaOptions;
        private System.Windows.Forms.GroupBox groupBox_AfterLaunch;
        private System.Windows.Forms.RadioButton radioButton_MinimizeToTray;
        private System.Windows.Forms.RadioButton radioButton_closelauncher;
        private System.Windows.Forms.RadioButton radioButton_Minimize;
        private System.Windows.Forms.Button button_mcpatcher;
        private System.Windows.Forms.NotifyIcon notifyIcon_tray;
        private System.Windows.Forms.RadioButton radioButton_stayopen;
        private System.Windows.Forms.DataGridView dataGridView_modpacks;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DownloadFile;
        private System.Windows.Forms.TabPage McReleases;
        private System.Windows.Forms.Button VersionDownloadbutton;
        private System.Windows.Forms.DataGridView VersionsGridView;
        private System.Windows.Forms.Button ResizeHeaders;
        private System.Windows.Forms.Button McReleaseUpdate_Button;
        private System.Windows.Forms.CheckBox LauncherLocation_Checkbox;
        private System.Windows.Forms.Label launcherlocation;
        private System.Windows.Forms.Label label_tickjava64_java8;
        private System.Windows.Forms.Label label_tickjava32_java8;
        private System.Windows.Forms.RadioButton radioButton_Java8_64;
        private System.Windows.Forms.RadioButton radioButton_Java8_32;
        private System.Windows.Forms.Label debug_info;
        private System.Windows.Forms.CheckBox checkBox_showconsole;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button_CopyLog;
        private System.Windows.Forms.DataGridView ModpacksGridView;
        private System.Windows.Forms.Button button_ModpackDownload;
        private System.Windows.Forms.Button button_updateMods;
        private System.Windows.Forms.RichTextBox richTextBox_modpacks;
        private System.Windows.Forms.Label label_ModInfo;
        private System.Windows.Forms.TabControl tabControl_Mods;
        private System.Windows.Forms.TabPage tabPage__modpacks;
        private System.Windows.Forms.TabPage tabPage_modlists;
        private System.Windows.Forms.Button button_ModPackExtract;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox richTextBox_Modlists;
        private System.Windows.Forms.DataGridView dataGridView_Modlists;
        private System.Windows.Forms.ComboBox comboBox_modsVersion;
        private System.Windows.Forms.Button button_ModlistRefresh;
        private System.Windows.Forms.RichTextBox Log_richTextBox;
        private System.Windows.Forms.CheckBox checkBoxAutoScroll;
        private System.Windows.Forms.Button button_FixVersion;
        private System.Windows.Forms.Label label_totalmods;
        private System.Windows.Forms.Button openVersionFolder_Button;
        private System.Windows.Forms.ToolTip openVersionFolder_toolTip;
        private System.Windows.Forms.ToolTip openportablefolder_toolTip;
        private System.Windows.Forms.LinkLabel modListChanges_linkLabel;
        private System.Windows.Forms.Button modlistDl_button;
        private System.Windows.Forms.ContextMenu Folder_contextMenu;
        private System.Windows.Forms.MenuItem menuItem_gameDir;
        private System.Windows.Forms.MenuItem menuItem_version;
        private System.Windows.Forms.MenuItem menuItem_mods;
        private System.Windows.Forms.MenuItem menuItem_resources;
        private System.Windows.Forms.MenuItem menuItem_shaderpacks;
    }
}

