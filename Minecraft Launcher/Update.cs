﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//used for stopwatch
using System.Diagnostics;
//webclient
using System.Net;
using Ionic.Zip;


namespace Minecraft_Launcher
{
    public partial class Update : Form
    {
        private string downloadurl = "";
        private string filename = "";
        private int filesize = 0;

        private string exePath = Application.StartupPath;

        public Update(string url, string reason, int filesize0, bool extract)
        {
            InitializeComponent();
            if (extract)
            {
                downloadurl = url;
                filename = Path.GetFileName(downloadurl);
                reason_label.Text = reason;
                groupBox_Progress.Text = "Extract Progress";
                ExtractFile(filename, exePath);
                filesize = filesize0;
            }
            else
            {
                reason_label.Text = reason;
                downloadurl = url;
                groupBox_Progress.Text = "Download Progress";
                filename = Path.GetFileName(downloadurl);
                filesize = filesize0;
            }
        }


        private void button_Update_Click(object sender, EventArgs e)
        {
            bool downloadit = true;
            if (File.Exists(filename))
            {
                FileInfo f = new FileInfo(filename);
                long s1 = f.Length;

                if (s1 == filesize)
                {
                    ExtractFile(filename, exePath);
                    downloadit = false;
                }
                else
                {
                    try
                    {
                        var req = HttpWebRequest.Create(downloadurl);
                        req.Method = "HEAD";


                        using (WebResponse resp = req.GetResponse())
                        {
                            int ContentLength;
                            if (int.TryParse(resp.Headers.Get("Content-Length"), out ContentLength))
                            {
                                if (s1 == ContentLength)
                                {
                                    // MessageBox.Show(s1 + "bytes" + ContentLength + "bytes ");
                                    ExtractFile(filename, exePath);
                                    downloadit = false;
                                }
                                else
                                {
                                    downloadit = false;
                                    download();
                                }


                                //Do something useful with ContentLength here 
                            }
                            resp.Close();
                        }
                    }
                    catch
                    {
                        //MessageBox.Show("Couldn't connect to server server");
                        downloadit = false;
                        download();
                    }
                }
            }

            if (downloadit)
            {
                download();
            }
        }


        private void download()
        {
            reason_label.Visible = false;
            button_Download.Enabled = false;

            labelSpeed.Visible = true;
            labelDownloaded.Visible = true;

            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                groupBox_Progress.Visible = true;
                progressBar_Update.Visible = true;

                WebClient webClient = new WebClient();
                webClient.DownloadProgressChanged += (s, et) =>
                    {
                        progressBar_Update.Value = et.ProgressPercentage;
                        if (labelPerc.Text != et.ProgressPercentage.ToString() + "%")
                        {
                            labelPerc.Text = et.ProgressPercentage.ToString() + "%";
                        }
                        if (labelPerc.Text !=
                            (Convert.ToDouble(et.BytesReceived)/1024/sw.Elapsed.TotalSeconds).ToString("0"))
                        {
                            labelSpeed.Text =
                                (Convert.ToDouble(et.BytesReceived)/1024/sw.Elapsed.TotalSeconds).ToString("0.00") +
                                " kb/s";
                        }

                        // Update the label with how much data have been downloaded so far and the total size of the file we are currently downloading
                        labelDownloaded.Text = (Convert.ToDouble(et.BytesReceived)/1024/1024).ToString("0.00") + " Mb's" +
                                               "  /  " +
                                               (Convert.ToDouble(et.TotalBytesToReceive)/1024/1024).ToString("0.00") +
                                               " Mb's";
                    };
                webClient.DownloadFileCompleted += (s, et) => { 
                        progressBar_Update.Visible = false;
                        // any other code to process the file
                    };


                //  MessageBox.Show(downloadurl + " "+ filename);
                //  webClient.DownloadFileAsync(new Uri("http://kraftzone.tk/downloads/jre1.7.0_25_64bit.zip"), exePath + "\\jre1.7.0_25_64bit222.zip");
                webClient.DownloadFileAsync(new Uri(downloadurl), exePath + "\\" + filename);
                webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
            }
            catch(WebException ex)
            {
                MessageBox.Show(ex.ToString()+ " Couldn't connect to server");
            }
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            // MessageBox.Show("Download completed!");

            //MessageBox.Show("Extracting files (will close when finished)", "Download Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //using (ZipFile zip = ZipFile.Read(exePath + "\\" + filename))
            //{
            //    foreach (ZipEntry e3 in zip)
            //    {
            //        // e3.Extract(exePath, true); // overwrite == true
            //        e3.Extract(exePath, ExtractExistingFileAction.OverwriteSilently);
            //    }
            //}

            if (e.Cancelled == true)
            {
                tryToDelete(filename);
                Close();
            }

            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message + "\n Check your connection/firewall is not blocking, then try again", "Download Error (CTRL+C to copy this error and report it", MessageBoxButtons.OK, MessageBoxIcon.Error);
                  tryToDelete(filename);
                  Close();
            }
            if (File.Exists(filename))
            {
                FileInfo f = new FileInfo(filename);
                long s1 = f.Length;

                if (s1 == filesize)
                {
                    ExtractFile(filename, exePath);

                }
                else
                {
                    MessageBox.Show(filename + " (size:" + s1 + " does not match expected size : " + filesize + " try download again.", "Download Error (CTRL+C to copy this error and report it", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    tryToDelete(filename);
                    Close();
                }
            }

           // ExtractFile(filename, exePath);

            //Close();
        }

        public static bool tryToDelete(string pathFILE)
        {
            if (File.Exists(pathFILE))
            {
                try
                {
                    File.Delete(pathFILE);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        public void ExtractFile(string zipToUnpack, string unpackDirectory)
        {
            //MessageBox.Show(zipToUnpack + " - " + unpackDirectory);


            labelSpeed.Visible = false;
            labelDownloaded.Visible = false;

            groupBox_Progress.Text = "Extract Progress";
            reason_label.Text = "Extracting files (will close when finished)";
            button_Download.Text = "Cancel";
            button_Download.Visible = false;
            progressBar_Update.Visible = true;
            int zipCount = 0;
            int numFiles = 0;
            int step = 0;

            try
            {
                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;
                worker.ProgressChanged += (o, e) =>
                    {
                        
                        progressBar_Update.Value = e.ProgressPercentage;
                        progressBar_Update.Maximum = zipCount * step;
                        numFiles += 1;
                        double percent = (double)(numFiles * 100) / zipCount;
                        labelPerc.Text = Math.Ceiling(percent).ToString() + "%";
                        //Downloader_LabelTotalFiles.Text = "Extracted Files: " + numFiles.ToString() + "/" + zipCount.ToString();

                    };
                worker.DoWork += (o, e) =>
                    {
                        using (ZipFile zip = ZipFile.Read(zipToUnpack))
                        {
                            zipCount = zip.Count;
                            step = (int)(zipCount/ 100.0);
                            int percentComplete = 0;
                            foreach (ZipEntry file in zip)
                            {
                                file.Extract(unpackDirectory, ExtractExistingFileAction.OverwriteSilently);
                                percentComplete += step;
                                worker.ReportProgress(percentComplete);
                            }
                        }
                    };

                worker.RunWorkerAsync();

                button_Download.Text = "Ok";
                button_Download.Enabled = true;


                //worker.RunWorkerCompleted += (sender, args) => Close();

                worker.RunWorkerCompleted += (sender, args) =>
                    {
                        reason_label.Text = "Done!";

                        //MessageBox.Show("Extraction complete, this will close after pressing 'OK'." + "\n" + "Run the launcher again.");
                        //Application.Exit();


                         Close();
                    };
            }
            catch (Exception derp)
            {
                MessageBox.Show("Error 3434: "+derp.ToString());
            }
        }
    }
}
