﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Minecraft_Launcher
{
    
    class FileCheck
    {
        //internal static Dictionary<string, string[]> versionData3 = new Dictionary<string, string[]>;



        public int getFiles(string versionJson)
        {
           // string versionJson = Main.exePath + "\\versions\\" + version + "\\" + version + ".json";
            string subString = "";
            try
            {
                if (File.Exists(versionJson))
                {
                    Main.versionData = JsonLoader.getVersionData(versionJson);
                }

            }
            catch (Exception ex)
            {
                subString = ex.Message;
            }

            bool returntoDownload = false;
            string versionFile = Main.exePath + @"\assets\indexes\" + Main.versionData["assets"][0] + ".json";

            int counter = 0;
            try
            {
                
                int x = 0;
                int i = 0;
                foreach (string entry in Main.versionData["libraries"])
                {
                    string libraryfile = Main.exePath + @"\libraries\" + entry;
                    if (!File.Exists(libraryfile))
                    {
                        returntoDownload = true;
                        if (Main.versionData["urlLibraries"][i] == "")
                        {
                            
                            Main.DownloadList.Add(counter, new string[] { "https://libraries.minecraft.net/" + entry.Replace(@"\", "/"), @"\libraries\" + entry });
                            counter++;
                            x++;
                            //fileInput.Add(x, new string[] { "https://libraries.minecraft.net/" + entry.Replace(@"\", "/"), @"libraries\" + entry }); 
                            //x++;
                            
                            Main.DownloadList.Add(counter, new string[] { "https://libraries.minecraft.net/" + entry.Replace(@"\", "/") + ".sha1", @"\libraries\" + entry + ".sha" });
                            counter++;
                            x++;

                            //fileInput.Add(x, new string[] { "https://libraries.minecraft.net/" + entry.Replace(@"\", "/") + ".sha1", @"libraries\" + entry + ".sha" }); 
                            //x++;
                        }
                        else
                        {
                            //modInput.Add(x, new string[] { "URL" + entry.Replace(@"\", "/"), @"libraries\" + entry }); x++;
                            //Dev// rewrite bad, Possible idea, check to see if download is possbile, if not, ... error?
                        }
                        i++;
                    }

                }

                if (returntoDownload)
                {
                    //return "Downloading Library files:";
                    return 0;
                }

                //native files
                foreach (string entry in Main.versionData["natives"])
                {
                    string libraryfile = Main.exePath + @"\libraries\" + entry;
                    if (!File.Exists(libraryfile))
                    {
                        returntoDownload = true;
                        if (Main.versionData["urlLibraries"][i] == "")
                        {

                            Main.DownloadList.Add(counter, new string[] { "https://libraries.minecraft.net/" + entry.Replace(@"\", "/"), @"\libraries\" + entry });
                            counter++;
                            x++;
                            //fileInput.Add(x, new string[] { "https://libraries.minecraft.net/" + entry.Replace(@"\", "/"), @"libraries\" + entry }); 
                            //x++;

                            Main.DownloadList.Add(counter, new string[] { "https://libraries.minecraft.net/" + entry.Replace(@"\", "/") + ".sha1", @"\libraries\" + entry + ".sha" });
                            counter++;
                            x++;

                            //fileInput.Add(x, new string[] { "https://libraries.minecraft.net/" + entry.Replace(@"\", "/") + ".sha1", @"libraries\" + entry + ".sha" }); 
                            //x++;
                        }
                        else
                        {
                            //modInput.Add(x, new string[] { "URL" + entry.Replace(@"\", "/"), @"libraries\" + entry }); x++;
                            //Dev// rewrite bad, Possible idea, check to see if download is possbile, if not, ... error?
                        }
                        i++;
                    }

                }

                if (returntoDownload)
                {
                    //return "Downloading native files:";
                    return 3;
                }
                //Dev//
                // Create a method to check for multiple xml files.
               
                if ((DateTime.Now - File.GetLastWriteTime(versionFile)).TotalHours > 8)
                {
                    //Dev//
                    //In the future, possibly check ETAG instead with all the files.
                    try
                    {
                      
                        //Dictionary<int, string[]> demoDownloadDictonary = new Dictionary<int, string[]>{
                        //{0, new string[] {"https://s3.amazonaws.com/Minecraft.Download/indexes/" + Main.versionData["assets"][0] + ".json", @"\assets\indexes\" + Main.versionData["assets"][0] + ".json" }}
                        // };

                        //Downloader MultifileDownload = new Downloader(demoDownloadDictonary, exePath);
                        //MultifileDownload.ShowDialog();


                        //int newint = Main.DownloadList.Count + 1;

                        
                        Main.DownloadList.Add(counter, new string[] { "https://s3.amazonaws.com/Minecraft.Download/indexes/" + Main.versionData["assets"][0] + ".json", @"\assets\indexes\" + Main.versionData["assets"][0] + ".json" });
                        counter++;
                        //return "Downloading asset index";
                        return 1;

                    }
                    catch (Exception ex)
                    {
                        subString = ex.Message;
                    }
                }


               // assetsList = JsonLoader.getVersionData(fileName);

                try
                {
                    if (File.Exists(versionFile))
                    {
                        Main.assetsList = JsonLoader.getAssetsList(versionFile);
                        foreach (KeyValuePair<string, string[]> entry in Main.assetsList)
                        {
                            if (!File.Exists(Main.exePath + @"\assets\objects\" + Main.Truncate(entry.Value[0], 2) + @"\" + entry.Value[0])) {
                                returntoDownload = true;
                                //fileInput.Add(x, new string[] { "http://resources.download.minecraft.net/" + Truncate(entry.Value[0], 2) + "/" + entry.Value[0], @"\assets\objects\" + Truncate(entry.Value[0], 2) + @"\" + entry.Value[0] }); 
                                //x++;
                                
                                Main.DownloadList.Add(counter, new string[] { "http://resources.download.minecraft.net/" + Main.Truncate(entry.Value[0], 2) + "/" + entry.Value[0], @"\assets\objects\" + Main.Truncate(entry.Value[0], 2) + @"\" + entry.Value[0] });
                                counter++; 
                                x++;
                            }
                        }
                        if (returntoDownload)
                        {
                            //return "Downloading assets";
                            return 2;
                        }
                  //works      
                      
                        //Downloader MultifileDownload = new Downloader(fileInput, exePath);
                    //    MultifileDownload.ShowDialog();

                    }
                    else
                    {
                        MessageBox.Show("Assets index missing.");
                    }
                }
                catch
                {
                   
                }
                        //if (status == "Successful")
                        //{
                        //    fileInput.Add(x, new string[] { "http://s3.amazonaws.com/Minecraft.Download/versions/" + versionData["id"][0] + "/" + versionData["id"][0] + ".jar", @"versions\" + versionData["id"][0] + @"\" + versionData["id"][0] + ".jar" }); x++;
                        //    Dictionary<int, string[]> downloadInput = atomFileData.fileCheck(fileInput, mcLocation);
                        //    Dictionary<int, string[]> modCheckInput = atomFileData.fileCheck(modInput, mcLocation);
                        //    if (modCheckInput.Count > 0)
                        //    {
                        //        status = "Error: Modifcation Files Missing, Please reinstall mod files.";
                        //    }
                        //    else
                        //    {
                        //        //if (mcOnlineMode)
                        //        // {
                        //        atomDownloading.Multi(downloadInput, mcLocation);
                        //        // }
                        //        //     else
                        //        //   {
                        //        if (downloadInput.Count > 0)
                        //        {
                        //            status = "Offline Mode, Files Missing. You need to login and download first, before offline mode can be used.";
                        //        }
                        //        //  }
                        //    }


      


                }
            
            catch (Exception ex)
            {
                //if (atomLauncher.cancelPressed)
                //{
                //    status = "Canceled: " + ex.Message;
                //}
                //else
                //{
                   // status = "Error: Checking Minecraft Files: " + ex.Message;
                //}
            }
            //return done;
            return 4;
        }

 
    }
  
}       

